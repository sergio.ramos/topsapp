<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('telefones');
            $table->string('suporte')->nullable();
            $table->string('email');
            $table->string('endereco');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('youtube');
            $table->mediumText('receber_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_sites');
    }
}
