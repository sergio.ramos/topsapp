<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecursosSistemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursos_sistemas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('filtro_id');
            $table->string('titulo');
            $table->longText('descricao');
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('filtro_id')
                ->references('id')->on('filtro_recursos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recursos_sistemas');
    }
}
