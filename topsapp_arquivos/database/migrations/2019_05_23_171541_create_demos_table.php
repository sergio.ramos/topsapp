<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('telefone_comercial');
            $table->string('telefone_celular');
            $table->string('email');
            $table->string('estado');
            $table->string('cidade');
            $table->string('endereco');
            $table->string('numero');
            $table->string('cep');
            $table->string('site');
            $table->string('responsavel');
            $table->string('conheceu');
            $table->longText('mensagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demos');
    }
}
