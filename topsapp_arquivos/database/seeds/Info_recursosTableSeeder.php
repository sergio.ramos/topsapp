<?php

use Illuminate\Database\Seeder;
use App\Models\Info_recurso;

class Info_recursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Info_recurso::create([
            'texto' => 'O Sistema TopSapp é o melhor e mais completo Sistema para gerenciamento de provedores de internet.'
        ]);
    }
}
