<?php

use Illuminate\Database\Seeder;
use App\Models\Demo;

class DemosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Demo::create([
            'nome' => 'maicon',
            'telefone_comercial' => '(43) 3461-2499',
            'telefone_celular' => '(41) 9671-5131',
            'email' => 'suporterma@outlook.com.br',
            'estado' => 'parana',
            'cidade' => 'faxinal',
            'endereco' => 'rua ana neri',
            'numero' => '31',
            'cep' => '86840-000',
            'site' => 'www.rmatelecom.com.br',
            'responsavel' => 'maicon',
            'conheceu' => 'Internet',
            'mensagem' => 'bom dia queria dar uma olhda na demo sua '
        ]);
    }
}
