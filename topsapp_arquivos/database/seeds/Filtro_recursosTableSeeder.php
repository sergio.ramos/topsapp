<?php

use Illuminate\Database\Seeder;
use App\Models\Filtro_recurso;

class Filtro_recursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Filtro_recurso::create([
            'nome' => 'Filtro A'
        ]);
    }
}
