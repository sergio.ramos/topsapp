<?php

use Illuminate\Database\Seeder;
use App\Models\Sobre;

class SobresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sobre::create([
            'titulo' => 'O TOPSAPP',
            'subtitulo' => 'Nossa tecnologia encurta distâncias e proporciona soluções que te levam a tranquilidade de seu negócio',
            'descricao' => 'A família TOPSAPP vem crescendo a cada dia, tornando sua marca cada vez mais forte e sólida. '
        ]);
    }
}
