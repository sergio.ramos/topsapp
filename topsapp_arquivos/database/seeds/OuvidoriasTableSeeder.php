<?php

use Illuminate\Database\Seeder;
use App\Models\Ouvidoria;

class OuvidoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ouvidoria::create([
            'titulo' => 'OUVIDORIA',
            'descricao' => 'A Ouvidoria TopSapp é um serviço de atendimento à sociedade com atribuições de ouvir, encaminhar e acompanhar críticas, opiniões, sugestões e denúncias sobre nossas atividades e serviços.

            É um órgão, de natureza mediadora, com caráter administrativo, executivo, que exerce suas funções diretamente junto à instituição, para que possa atingir seus fins. 
            
            Nota: Em caso de denúncia, necessitamos de um relato detalhado do fato, com datas, local, nomes dos envolvidos, endereços, entre outros dados, além de documentos que comprovem o ato e possibilitem a apuração dos fatos. Caso contrário, a denúncia poderá ser arquivada por falta de evidências. O prazo para resposta é de 15 dias corridos. Central de Ouvidoria TopSapp: (66) 3531-9741.'
        ]);
    }
}
