<?php

use Illuminate\Database\Seeder;
use App\Models\Mensagens;

class MensagensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mensagens::create([
            'nome_mensagem' => 'Alcivan Nunes',
            'email_mensagem' => 'alcivan@infotechtelecom.com.br',
            'site_mensagem' => 'www.infotechtelecom.com.br',
            'telefone_mensagem' => '(87) 99611-7566',
            'departamento_mensagem' => 'Administrativo',
            'mensagem_mensagem' => 'Desejo migrar para o topsapp'
        ]);
    }
}
