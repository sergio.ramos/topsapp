<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([   UsersTableSeeder::class, 
                        Titulos_sitesTableSeeder::class,
                        SobresTableSeeder::class,
                        Info_recursosTableSeeder::class,
                        Filtro_recursosTableSeeder::class,
                        Dados_siteTableSeeder::class,
                        OuvidoriasTableSeeder::class,
                        Ouvidoria_mensagensTableSeeder::class,
                        DemosTableSeeder::class,
                        MensagensTableSeeder::class,
                        NoticiasTableSeeder::class
        ]);
    }
}
