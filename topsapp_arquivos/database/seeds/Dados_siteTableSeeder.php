<?php

use Illuminate\Database\Seeder;
use App\Models\Dados_site;

class Dados_siteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dados_site::create([
            'telefones' => '(66) 3211-0010',
            'email' => 'comercial@topsapp.com.br',
            'endereco' => 'Rua das Cerejeiras, Nº 1987, Jardim Paraiso, Sinop, Mato Grosso, Cep 78556-106',
            'facebook' => 'https://www.facebook.com/multiwaretecnologia',
            'twitter' => 'https://twitter.com/topsapp',
            'youtube' => 'https://www.youtube.com/channel/UC3pxwqowZFKOXc0OQtqLNbg',
            'receber_info' => 'noc@topsapp.com.br;comercial@topsapp.com.br'
        ]);
    }
}
