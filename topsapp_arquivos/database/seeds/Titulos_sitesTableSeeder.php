<?php

use Illuminate\Database\Seeder;
use App\Models\Titulos_site;

class Titulos_sitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Titulos_site::create([
            'titulo_site' => 'Topsapp Gestão de Provedores de Internet'
        ]);
    }
}
