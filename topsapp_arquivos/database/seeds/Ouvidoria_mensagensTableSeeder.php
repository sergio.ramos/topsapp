<?php

use Illuminate\Database\Seeder;
use App\Models\Ouvidoria_mensagens;

class Ouvidoria_mensagensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ouvidoria_mensagens::create([
            'nome' => 'Allan Cassio',
            'email' => 'allangonssalves@live.com',
            'estado' => 'Pernambuco',
            'cidade' => 'Cabo de Santo Agostinho',
            'cep' => '54580-230',
            'telefone' => '(81) 8444-8932',
            'prioridade' => 'Urgente',
            'tipo_pessoa' => 'Pessoa Física',
            'mensagem' => 'Preciso de um sistema para gerenciar meus cliente boletos e etc do meu provedor, Gostaria de saber os valores de compra, valores mensais'
        ]);
    }
}
