<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name'  =>  'Sergio Ramos',
                'email' =>  'sergio.ramos@unemat.br',
                'password'  =>  bcrypt('99611661'),
                'visita'    =>  date("Y-m-d H:i:s"),
                'pusuarios' => true,
                'psobre'    => true,
                'pouvidoria'    => true,
                'pnoticias' => true,
                'pmensagens'    => true,
                'ptv'   => true,
                'precursos' => true,
                'pslide'    => true
            ],
            [
                'name'  =>  'Teste Sistema',
                'email' =>  'teste.sistema@topsapp.com',
                'password'  =>  '$2y$10$hCSPstphOXRyTrXE.KJYeOQ4jAhIfdI6lbv3mqSsXcMi66YH2B/qm',
                'visita'    =>  date("Y-m-d H:i:s"),
                'pusuarios' => true,
                'psobre'    => true,
                'pouvidoria'    => true,
                'pnoticias' => true,
                'pmensagens'    => true,
                'ptv'   => true,
                'precursos' => true,
                'pslide'    => true
            ],
            [
                'name'  =>  'Diego Rocha',
                'email' =>  'diego@topsinop.com.br',
                'password'  =>  '$2y$10$bbqE1j0oGxbCc.zbBu37HOUkMxnllBnV20kPAdxFAYlRI9pol892.',
                'visita'    =>  date("Y-m-d H:i:s"),
                'pusuarios' => true,
                'psobre'    => true,
                'pouvidoria'    => true,
                'pnoticias' => true,
                'pmensagens'    => true,
                'ptv'   => true,
                'precursos' => true,
                'pslide'    => true
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
        
    }
}
