<?php

use Illuminate\Database\Seeder;
use App\Models\Noticia;

class NoticiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Noticia::create([
            'titulo_noticia' => 'TopSapp expõe produtos e serviços na Future ISP Cuiabá',
            'descricao_noticia' => 'teste',
            'link' => 'topsapp-expoe-produtos-e-servicos-na-future-isp-cuiaba-11-06-2019'
        ]);
    }
}
