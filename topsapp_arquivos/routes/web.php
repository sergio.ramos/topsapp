<?php
use Illuminate\Support\Facades\Route;

// Rotas do topsapp
Route::group(['prefix' => '/', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Site\SiteController@index')->name('site.home');

    // quem somos
    Route::get('/quem-somos', 'Site\SiteController@quemSomos')->name('site.quemSomos');

    // clientes
    Route::get('/clientes', 'Site\SiteController@clientes')->name('site.clientes');

    // recursos
    Route::get('/recursos', 'Site\SiteController@recursos')->name('site.recursos');

    // tv
    Route::get('/tv', 'Site\SiteController@tv')->name('site.tv');

    // ouvidoria
    Route::get('/ouvidoria', 'Site\SiteController@ouvidoria')->name('site.ouvidoria');

    Route::post('/ouvidoria/enviar-email', 'Site\SiteController@sendOuvidoriaEmail')->name('site.ouvidoria.email');

    // contato
    Route::get('/contato', 'Site\SiteController@contato')->name('site.contato');

    Route::post('/contato/enviar-email', 'Site\SiteController@sendContatoEmail')->name('site.contato.email');

    // demo
    Route::get('/demo', 'Site\SiteController@demo')->name('site.demo');

    Route::post('/demo/enviar-email', 'Site\SiteController@sendDemoEmail')->name('site.demo.email');

    // noticias
    Route::get('/noticias', 'Site\SiteController@listarNoticias')->name('site.noticias');

    Route::get('/noticias/{link?}/{id?}', 'Site\SiteController@verNoticia')->name('site.abrir.noticia');
});

// Rotas do topvoz
Route::group(['prefix' => '/topvoz', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topvoz\TopvozController@index')->name('topvoz.home');

    Route::post('/solicitar', 'Topvoz\TopvozController@solicitar')->name('topvoz.solicitar');
});

// Rotas do modulocobranca
Route::group(['prefix' => '/topwifi', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topwifi\TopWifiController@index')->name('topwifi.home');

    // Route::post('/solicitar', 'Topvoz\TopvozController@solicitar')->name('topvoz.solicitar');
});

// Rotas do topvoz
Route::group(['prefix' => '/topmaps', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topmaps\TopmapsController@index')->name('topmaps.home');

    // Route::get('/info', 'Topmaps\info.php')->name('topmaps.info');
});

// Rotas do topvoz
Route::group(['prefix' => '/topapp', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topapp\TopappController@index')->name('topapp.home');

    // Route::get('/info', 'Topmaps\info.php')->name('topmaps.info');
});

// Rotas do modulocobranca
Route::group(['prefix' => '/modulocobranca', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Modulocobranca\ModulocobrancaController@index')->name('modulocobranca.home');

    // Route::post('/solicitar', 'Topvoz\TopvozController@solicitar')->name('topvoz.solicitar');
});

// Rotas do topvoz
Route::group(['prefix' => '/topsms', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topsms\TopsmsController@index')->name('topsms.home');

    // Route::get('/info', 'Topmaps\info.php')->name('topmaps.info');
});

// Rotas do topvoz
Route::group(['prefix' => '/centralapp', 'middleware' => 'dados.site'], function () {
    // home
    Route::get('/', 'Topapp\TopappController@indexCentral')->name('topappCentral.home');

    // Route::get('/info', 'Topmaps\info.php')->name('topmaps.info');
});

// Rotas do tip
Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'registrar.session'], 'namespace' => 'Admin'], function(){
    // inicio rotas página principal
    Route::get('', 'PrincipalController@index')->name('admin.home');

    Route::post('atualizar-titulo', 'PrincipalController@atualizarTitulo')->name('atualizar.titulo');

    Route::get('alterar-senha', 'PrincipalController@viewAlterarSenha')->name('alterar.senha');
    
    Route::post('alterar-senha', 'PrincipalController@alterarSenha')->name('alterar.senha');
    // fim rotas página pricipal

    // inicio rota pagina sobre
    Route::get('pagina/sobre','Pagina\SobreController@index')->name('pagina.sobre');

    Route::post('pagina/sobre/atualizar-dados', 'Pagina\SobreController@atualizarDados')->name('atualizar.dados.sobre');
    // fim rota pagina sobre

    // inicio rota pagina recursos
    Route::get('pagina/recursos/descricao','Pagina\RecursoController@descricao')->name('pagina.recurso.descricao');
    
    Route::post('pagina/recursos/descricao/atualizar-dados','Pagina\RecursoController@atualizarDados')->name('atualizar.dados.recurso.descricao');

    Route::get('pagina/recursos/sistema','Pagina\RecursoController@sistema')->name('pagina.recurso.sistema');
    
    Route::post('pagina/recursos/sistema/cadastrar-filtro','Pagina\RecursoController@cadastrarFiltro')->name('cadastrar.filtro.recurso.sistema');

    Route::delete('pagina/recursos/sistema/deletar-filtro/{id?}','Pagina\RecursoController@deletarFiltro')->name('deletar.filtro.recurso.sistema');

    Route::get('pagina/recursos/sistema/cadastrar-recurso','Pagina\RecursoController@viewCadastrarRecurso')->name('cadastrar.recurso.sistema');

    Route::post('pagina/recursos/sistema/cadastrar-recurso','Pagina\RecursoController@cadastrarRecurso')->name('cadastrar.recurso.sistema');

    Route::get('pagina/recursos/sistema/editar-recurso/{id?}','Pagina\RecursoController@viewEditarRecurso')->name('editar.recurso.sistema');

    Route::post('pagina/recursos/sistema/editar-recurso/{id?}','Pagina\RecursoController@editarRecurso')->name('editar.recurso.sistema');

    Route::delete('pagina/recursos/sistema/deletar-recurso/{id?}','Pagina\RecursoController@deletarRecurso')->name('deletar.recurso.sistema');
    // fim rota pagina recursos

    // inicio rota pagina noticias
    Route::get('pagina/noticias','Pagina\NoticiaController@noticias')->name('pagina.noticia');

    Route::get('pagina/noticias/cadastrar-noticia','Pagina\NoticiaController@viewCadastrarNoticia')->name('cadastrar.noticia');

    Route::post('pagina/noticias/cadastrar-noticia/upload-imagens', 'Pagina\NoticiaController@uploadImagens');

    Route::post('pagina/noticias/cadastrar-noticia/delete-imagens', 'Pagina\NoticiaController@deleteImagens');

    Route::post('pagina/noticias/cadastrar-noticia','Pagina\NoticiaController@cadastrarNoticia')->name('cadastrar.noticia');

    Route::get('pagina/noticias/editar-noticia/{id?}','Pagina\NoticiaController@viewEditarNoticia')->name('editar.noticia');

    Route::get('pagina/noticias/editar-noticia/view-imagens/{id?}', 'Pagina\NoticiaController@viewFilesNoticia');

    Route::post('pagina/noticias/editar-noticia/upload-imagens/{id?}', 'Pagina\NoticiaController@editarUploadImagens');

    Route::post('pagina/noticias/editar-noticia/delete-imagens/{id?}', 'Pagina\NoticiaController@editarDeleteImagens');

    Route::post('pagina/noticias/editar-noticia/{id?}','Pagina\NoticiaController@editarNoticia')->name('editar.noticia');

    Route::delete('pagina/noticias/deletar-noticia/{id?}','Pagina\NoticiaController@deletarNoticia')->name('deletar.noticia');
    // fim rota pagina noticias

    // inicio rota pagina ouvidoria
    Route::get('pagina/ouvidoria','Pagina\OuvidoriaController@ouvidoria')->name('pagina.ouvidoria');

    Route::post('pagina/ouvidoria','Pagina\OuvidoriaController@atualizarDados')->name('atualizar.dados.ouvidoria');
    // fim rota pagina ouvidoria

    // inicio rota pagina tv
    Route::get('pagina/tv','Pagina\TvController@tv')->name('pagina.tv');

    Route::get('pagina/tv/cadastrar-video','Pagina\TvController@viewCadastrarVideo')->name('cadastrar.video');

    Route::post('pagina/tv/cadastrar-video','Pagina\TvController@cadastrarVideo')->name('cadastrar.video');

    Route::get('pagina/tv/editar-video/{id?}','Pagina\TvController@viewEditarVideo')->name('editar.video');

    Route::post('pagina/tv/editar-video/{id?}','Pagina\TvController@editarVideo')->name('editar.video');

    Route::delete('pagina/tv/deletar-video/{id?}','Pagina\TvController@deletarVideo')->name('deletar.video');
    // fim rota pagina tv

    // inicio rota pagina slides
    Route::get('pagina/slides','Pagina\SlideController@slide')->name('pagina.slide');

    Route::get('pagina/slides/cadastrar-slide','Pagina\SlideController@viewCadastrarSlide')->name('cadastrar.slide');

    Route::post('pagina/slides/cadastrar-slide','Pagina\SlideController@cadastrarSlide')->name('cadastrar.slide');

    Route::get('pagina/slides/editar-slide/{id?}','Pagina\SlideController@viewEditarSlide')->name('editar.slide');

    Route::post('pagina/slides/editar-slide/{id?}','Pagina\SlideController@editarSlide')->name('editar.slide');

    Route::delete('pagina/slides/deletar-slide/{id?}','Pagina\SlideController@deletarSlide')->name('deletar.slide');
    // fim rota pagina slide

    // inicio rota contato
    Route::get('contato','ContatoController@contato')->name('contato');

    Route::get('contato/visualizar-mensagem/{id?}','ContatoController@visualizarMensagem')->name('visualizar.mensagem');
    // fim rota contato

    // inicio rota demo
    Route::get('demos','DemoController@demo')->name('demo');

    Route::get('demos/visualizar-demo/{id?}', 'DemoController@visualizarDemo')->name('visualizar.demo');
    // fim rota demo

    // inicio rota ouvidoria
    Route::get('ouvidorias','OuvidoriaMensagensController@ouvidorias')->name('ouvidorias');

    Route::get('ouvidorias/visualizar-mensagem/{id?}', 'OuvidoriaMensagensController@visualizarMensagem')->name('visualizar.ouvidoria');
    // fim rota ouvidoria

    // inicio rota usuarios
    Route::get('usuarios','UsuarioController@usuarios')->name('usuarios');

    Route::get('usuarios/visualizar-usuario/{id?}', 'UsuarioController@visualizarUsuario')->name('visualizar.usuario');

    Route::get('usuario/cadastrar-usuario', 'UsuarioController@viewCadastrarUsuario')->name('cadatrar.usuario');

    Route::post('usuario/cadastrar-usuario', 'UsuarioController@cadastrarUsuario')->name('cadastrar.usuario');

    Route::get('usuario/editar-usuario/{id?}', 'UsuarioController@viewEditarUsuario')->name('editar.usuario');

    Route::post('usuario/editar-usuario/{id?}', 'UsuarioController@editarUsuario')->name('editar.usuario');

    Route::delete('usuarios/deletar-usuario/{id?}', 'UsuarioController@deletarUsuario')->name('deletar.usuario');
    // fim rota usuarios

    // inicio rota dados
    Route::get('dados','DadosController@dados')->name('dados.site');

    Route::post('dados', 'DadosController@atualizarDados')->name('atualizar.dados.site');
    // fim rota dados
});
