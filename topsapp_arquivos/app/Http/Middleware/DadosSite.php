<?php

namespace App\Http\Middleware;
use App\Models\Titulos_site;
use App\Models\Dados_site;

use Closure;

class DadosSite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset($_SESSION['site'])) {
            
            $this->dadosSite();
        }

        return $next($request);
    }

    private function dadosSite() {
        
        $titulo_site = Titulos_site::find(1);
        $dados_site = Dados_site::find(1);

        $_SESSION['site'] = array(  'titulo_site' => $titulo_site->titulo_site,
                                    'dados_site' =>  serialize($dados_site)
        );
    }
}
