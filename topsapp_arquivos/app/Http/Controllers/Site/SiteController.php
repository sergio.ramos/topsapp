<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use App\Models\Recursos_sistema;
use App\Models\Noticia;
use App\Models\Tv;
use App\Models\Sobre;
use App\Models\Info_recurso;
use App\Models\Filtro_recurso;
use App\Models\Ouvidoria;
// emails models
use App\Models\Ouvidoria_mensagens;
use App\Models\Mensagens;
use App\Models\Demo;
use function Opis\Closure\unserialize;

// config email use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OuvidoriaMail;
use App\Mail\ContatoMail;
use App\Mail\DemoMail;
use function GuzzleHttp\json_decode;

// config phpmailer
// require 'mailer/PHPMailerAutoload.php';

class SiteController extends Controller
{
    public function index()
    {
        $slides = Slide::all();
        $recursos = Recursos_sistema::all();
        $noticias = Noticia::all();
        $videos = Tv::all();

        foreach ($recursos as $key => $value) {
            $value->descricao = strip_tags($value->descricao);
        }

        foreach ($noticias as $key => $value) {
            $value->descricao_noticia = strip_tags($value->descricao_noticia);
            $value->descricao_noticia = substr($value->descricao_noticia, 0, 100) . " . . .";
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        foreach ($videos as $key => $value) {
            if (strpos($value->video, "watch?v=") !== false) {
                
                $value->video = str_replace("watch?v=", "embed/", $value->video);
            }
        }

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'slides' => $slides,
                        'recursos' => $recursos,
                        'noticias' => $noticias,
                        'video' => $videos[$videos->count()-1],
                        'pageAtual' => [
                            'nome' => 'Home',
                            'rota' => 'site.home'
                        ]
        );

        return view('site.index', $data);
    }

    public function quemSomos()
    {
        $sobre = Sobre::find(1);

        // $sobre->descricao = strip_tags($value->descricao);

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'sobre' => $sobre,
                        'pageAtual' => [
                            'nome' => 'Quem somos',
                            'rota' => 'site.quemSomos'
                        ]
        );
        
        return view('site.quem_somos', $data);
    }

    public function clientes()
    {

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'pageAtual' => [
                            'nome' => 'Clientes',
                            'rota' => 'site.clientes'
                        ]
        );
        
        return view('site.clientes', $data);
    }

    public function recursos()
    {
        $info_recurso = Info_recurso::find(1);
        $filtros_recurso = Filtro_recurso::all();
        $recursos = Recursos_sistema::all();

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'info_recurso' => $info_recurso,
                        'filtros_recurso' => $filtros_recurso,
                        'recursos' => $recursos,
                        'pageAtual' => [
                            'nome' => 'Recursos',
                            'rota' => 'site.recursos'
                        ]
        );
        
        return view('site.recursos', $data);
    }

    public function tv() 
    {
        $videos = Tv::all();

        foreach ($videos as $key => $value) {
            if (strpos($value->video, "watch?v=") !== false) {
                
                $value->video = str_replace("watch?v=", "embed/", $value->video);
            }
        }

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'videos' => $videos,
                        'pageAtual' => [
                            'nome' => 'Tv topsapp',
                            'rota' => 'site.tv'
                        ]
        );

        return view('site.tv', $data);
    }

    public function ouvidoria() 
    {
        $ouvidoria = Ouvidoria::find(1);
        
        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'ouvidoria' => $ouvidoria,
                        'pageAtual' => [
                            'nome' => 'Ouvidoria',
                            'rota' => 'site.ouvidoria'
                        ]
        );

        return view('site.ouvidoria', $data);
    }

    public function sendOuvidoriaEmail(Request $request)
    {
        // autentication recaptcha
        $siteKey = "6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr";
        $secretKey = "6LeMm7EUAAAAAI8v6mUD2rOiox1uwMNccNI1qv4N";

        if($request->input("g-recaptcha-response") != null && !empty($request->input("g-recaptcha-response"))) {
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $request->input("g-recaptcha-response"));
            $responseData = json_decode($verifyResponse);

            if($responseData->success) {

                $ouvidoria_mensagem = new Ouvidoria_mensagens;
                $nome = $request->input('nome');
                $email = $request->input('email');
                $mensagem = $request->input('descricao');

                // Enviando o e-mail noc@topsapp.com.br  comercial@topsapp.com.br
                Mail::to('noc@topsapp.com.br')->cc('comercial@topsapp.com.br')->send(new OuvidoriaMail($nome, $email, $request->input('estados_brasil'), $request->input('cidade'), $request->input('cep'), $request->input('telefone'), $request->input('prioridade'), $request->input('tipo_pessoa'), $mensagem));

                // salvar no banco de dados
                $ouvidoria_mensagem->nome = $nome;
                $ouvidoria_mensagem->email = $email;
                $ouvidoria_mensagem->estado = $request->input('estados_brasil');
                $ouvidoria_mensagem->cidade = $request->input('cidade');
                $ouvidoria_mensagem->cep = $request->input('cep');
                $ouvidoria_mensagem->telefone = $request->input('telefone');
                $ouvidoria_mensagem->prioridade = $request->input('prioridade');
                $ouvidoria_mensagem->tipo_pessoa = $request->input('tipo_pessoa');
                $ouvidoria_mensagem->mensagem = $mensagem;

                $ouvidoria_mensagem->save();

                return redirect()->route('site.ouvidoria')->with('mensagem', 'O e-mail foi enviado com sucesso! Retornaremos sua solicitação.');
            } else {
                return redirect()->route('site.ouvidoria')->with('invalido', 'A verificação do robô falhou, tente novamente.');
            }

        } else {
            return redirect()->route('site.ouvidoria')->with('invalido', 'Por favor, verifique a caixa reCAPTCHA.');
        }
    }

    public function contato() 
    {
        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'pageAtual' => [
                            'nome' => 'Contato',
                            'rota' => 'site.contato'
                        ]
        );

        return view('site.contato', $data);
    }

    public function sendContatoEmail(Request $request)
    {
        // autentication recaptcha
        $siteKey = "6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr";
        $secretKey = "6LeMm7EUAAAAAI8v6mUD2rOiox1uwMNccNI1qv4N";

        if($request->input("g-recaptcha-response") != null && !empty($request->input("g-recaptcha-response"))) {
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $request->input("g-recaptcha-response"));
            $responseData = json_decode($verifyResponse);

            if($responseData->success) {

                $contato_mensagem = new Mensagens;
                $nome = $request->input('nome');
                $email = $request->input('email');
                $descricao = $request->input('descricao');

                // Enviando o e-mail comercial@topsapp.com.br
                Mail::to('comercial@topsapp.com.br')->send(new ContatoMail($nome, $email, $request->input('site'), $request->input('telefone'), $request->input('departamento'), $descricao));

                $contato_mensagem->nome_mensagem = $nome;
                $contato_mensagem->email_mensagem = $email;
                $contato_mensagem->site_mensagem = $request->input('site');
                $contato_mensagem->telefone_mensagem = $request->input('telefone');
                $contato_mensagem->departamento_mensagem = $request->input('departamento');
                $contato_mensagem->mensagem_mensagem = $descricao;

                $contato_mensagem->save();

                return redirect()->route('site.contato')->with('mensagem', 'O e-mail foi enviado com sucesso! Retornaremos sua solicitação.');
            } else {
                return redirect()->route('site.contato')->with('invalido', 'A verificação do robô falhou, tente novamente.');
            }
        } else {
            return redirect()->route('site.contato')->with('invalido', 'Por favor, verifique a caixa reCAPTCHA.');
        }
    }

    public function demo() 
    {
        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'pageAtual' => [
                            'nome' => 'Demo',
                            'rota' => 'site.demo'
                        ]
        );

        return view('site.demo', $data);
    }

    public function sendDemoEmail(Request $request)
    {
        // autentication recaptcha
        $siteKey = "6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr";
        $secretKey = "6LeMm7EUAAAAAI8v6mUD2rOiox1uwMNccNI1qv4N";

        if($request->input("g-recaptcha-response") != null && !empty($request->input("g-recaptcha-response"))) {
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $request->input("g-recaptcha-response"));
            $responseData = json_decode($verifyResponse);

            if($responseData->success) {

                $demo_mensagem = new Demo;
                $nome = $request->input('nome');
                $email = $request->input('email');
                $descricao = $request->input('descricao');

                // Enviando o e-mail comercial@topsapp.com.br
                Mail::to('comercial1@topsapp.com.br')->send(new DemoMail($nome, $request->input('telefone_comercial'), $request->input('telefone_celular'), $email, $request->input('estados_brasil'), $request->input('cidade'), $request->input('endereco'), $request->input('numero'), $request->input('cep'), $request->input('site'), $request->input('responsavel'), $request->input('conheceu'), $descricao));

                $demo_mensagem->nome = $nome;
                $demo_mensagem->telefone_comercial = (empty($request->input('telefone_comercial'))) ? "" : $request->input('telefone_comercial');
                $demo_mensagem->telefone_celular = $request->input('telefone_celular');
                $demo_mensagem->email = $email;
                $demo_mensagem->estado = $request->input('estados_brasil');
                $demo_mensagem->cidade = $request->input('cidade');
                $demo_mensagem->endereco = $request->input('endereco');
                $demo_mensagem->numero = $request->input('numero');
                $demo_mensagem->cep = $request->input('cep');
                $demo_mensagem->site = $request->input('site');
                $demo_mensagem->responsavel = $request->input('responsavel');
                $demo_mensagem->conheceu = $request->input('conheceu');
                $demo_mensagem->mensagem = $descricao;

                $demo_mensagem->save();

                return redirect()->route('site.demo')->with('mensagem', 'Solicitação enviada com sucesso');
            } else {
                return redirect()->route('site.demo')->with('invalido', 'A verificação do robô falhou, tente novamente.');
            }
        } else {
            return redirect()->route('site.demo')->with('invalido', 'Por favor, verifique a caixa reCAPTCHA.');
        }
    }

    public function listarNoticias(Request $request)
    {
        $noticias = Noticia::all();

        foreach ($noticias as $key => $value) {

            $value->data_cadastro = strftime('%d de %B de %Y', strtotime($value->created_at));
        }

        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'noticias' => $noticias,
                        'pageAtual' => [
                            'nome' => 'Notícias',
                            'rota' => 'site.noticias'
                        ]
        );

        return view('site.listarNoticias', $data);
    }

    public function verNoticia(Request $request) 
    {
        $noticia = Noticia::find($request->id);

        $noticia->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i', strtotime($noticia->created_at)));

        if(!empty($noticia->imagens)) {
            $imagens = explode(';', $noticia->imagens);
            array_pop($imagens);
            $noticia->imagens = $imagens;
        }
        
        $data = array(  'titulo_site' => $_SESSION['site']['titulo_site'],
                        'dados_site' => unserialize($_SESSION['site']['dados_site']),
                        'noticia' => $noticia,
                        'pageAtual' => [
                            'nome' => 'Notícias',
                            'rota' => 'site.noticias'
                        ]
        );

        return view('site.noticias', $data);
    }
}
