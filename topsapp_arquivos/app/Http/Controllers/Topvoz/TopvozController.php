<?php

namespace App\Http\Controllers\topvoz;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// config email use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Mail;
use App\Mail\TopvozMail;

class TopvozController extends Controller
{
    public function index()
    {
        $data = array(  
        );

        return view('topvoz.index', $data);
    }

    public function solicitar(Request $request) {

        $nome = $request->input('nome');
        $email = $request->input('email');
        $telefone = $request->input('telefone');

        // Enviando o e-mail comercial@topsapp.com.br
        Mail::to('comercial@topsapp.com.br')->send(new TopvozMail($nome, $email, $telefone));

        return redirect()->route('topvoz.home')->with('mensagem', 'Solicitação enviada com sucesso');
    }
}
