<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;

class SlideController extends Controller
{
    public function slide()
    {
        $slides = Slide::all();

        foreach ($slides as $key => $value) {

            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'slides' => $slides
        );

        return view('admin.paginas.slides.index', $data);
    }
    
    public function viewCadastrarSlide()
    {
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.paginas.slides.cadastrar_slide', $data);
    }

    public function cadastrarSlide(Request $request)
    {
        if ($request->hasFile('file')) {
            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.slide')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.slide')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/slide', $file_name);

                $slide = new Slide;
                $slide->titulo = $request->input('titulo');
                $slide->link = $request->input('link');
                $slide->imagem = $file_name;
                $slide->save();

                return redirect()->route('pagina.slide')->with('mensagem', 'O slide foi cadastrado com sucesso!');
            }
        }
    }

    public function viewEditarSlide(Request $request)
    {
        $slide = Slide::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'slide' => $slide
        );

        return view('admin.paginas.slides.editar_slide', $data);
    }

    public function editarSlide(Request $request)
    {
        $slide = Slide::find($request->input('id'));

        if( $slide->titulo === $request->input('titulo') &&
            $slide->link === $request->input('link') &&
            $slide->imagem === $request->input('file-name')
        ) {
            
            return redirect()->route('pagina.slide')->with('mensagem', 'O slide já está atualizado!');
        } else if($slide->imagem === $request->input('file-name')) {

            $slide->titulo = $request->input('titulo');
            $slide->link = $request->input('link');
            $slide->save();

            return redirect()->route('pagina.slide')->with('mensagem', 'O slide foi atualizado com sucesso!');
        } else {

            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.slide')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.slide')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/slide', $file_name);

                $slide->titulo = $request->input('titulo');
                $slide->link = $request->input('link');
                $slide->imagem = $file_name;
                $slide->save();

                return redirect()->route('pagina.slide')->with('mensagem', 'O slide foi atualizado com sucesso!');
            }
        }
    }

    public function deletarSlide(Request $request)
    {
        $slide = Slide::find($request->id);
        $slide->delete();

        return redirect()->route('pagina.slide')->with('mensagem', 'O slide foi deletado com sucesso!');
    }
}
