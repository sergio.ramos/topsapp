<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        $dadosSobre = Sobre::find(1);

        $dadosSobre->atualizacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($dadosSobre->updated_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'dados' => $dadosSobre
        );

        return view('admin.paginas.sobre', $data);
    }

    public function atualizarDados(Request $request)
    {
        $dadosSobre = Sobre::find(1);

        if(     $dadosSobre->titulo === $request->input('titulo') &&
                $dadosSobre->subtitulo === $request->input('subtitulo') &&
                $dadosSobre->descricao === $request->input('descricao')
        ) {

            return redirect()->route('pagina.sobre')->with('mensagem', 'Os dados da página já estão atualizados!');
        } else {

            $dadosSobre->titulo = $request->input('titulo');
            $dadosSobre->subtitulo = $request->input('subtitulo');
            $dadosSobre->descricao = $request->input('descricao');
            $dadosSobre->save();

            return redirect()->route('pagina.sobre')->with('mensagem', 'Os dados da página foram atualizados com sucesso!');
        }
    }
}
