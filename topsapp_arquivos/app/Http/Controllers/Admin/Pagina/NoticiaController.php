<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Noticia;
use URLify;

class NoticiaController extends Controller
{
    public function noticias()
    {
        $noticias = Noticia::all();

        foreach ($noticias as $key => $value) {
            
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'noticias' => $noticias
        );

        return view('admin.paginas.noticias.index', $data);
    }

    public function viewCadastrarNoticia() 
    {
        
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.paginas.noticias.cadastrar_noticia', $data);
    }

    public function uploadImagens(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move('uploads/noticias/', $imageName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $imageName]);
    }

    public function deleteImagens(Request $request)
    {
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/noticias/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function cadastrarNoticia(Request $request) 
    {
        $noticia = new Noticia;
        $noticia->titulo_noticia = $request->input('titulo_noticia');
        $noticia->descricao_noticia = $request->input('descricao_noticia');
        $noticia->link = URLify::filter($request->input('titulo_noticia')) . '-' . date('d-m-Y');

        if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link)) {

            mkdir(public_path() . "/uploads/noticias/" . $noticia->link);
        }

        if(!empty($request->file('foto_destaque'))) {

            $noticia->foto_destaque = $this->noticiaUploadFile($request->file('foto_destaque'), $noticia->link);
        }

        if(!empty($request->file('foto_capa'))) {

            $noticia->foto_capa = $this->noticiaUploadFile($request->file('foto_capa'), $noticia->link);
        }

        if(!empty($request->file('foto_interna'))) {

            $noticia->foto_interna = $this->noticiaUploadFile($request->file('foto_interna'), $noticia->link);
        }

        if(!empty($request->input('imagens'))) {

            if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link . "/fotos")) {

                mkdir(public_path() . "/uploads/noticias/" . $noticia->link . "/fotos");
            }

            $imagens = $request->input('imagens');
            $noticia->imagens = $imagens;
            $imagens = explode(";", $imagens);
            array_pop($imagens);

            foreach ($imagens as $key => $value) {
                rename(public_path() . "/uploads/noticias/" . $value, public_path() . "/uploads/noticias/" . $noticia->link . "/fotos" . "/" . $value);
            }
        }

        $noticia->save();

        return redirect()->route('pagina.noticia')->with('mensagem', 'A notícia foi cadastrada com sucesso!');
    }

    public function viewEditarNoticia(Request $request) 
    {
        $noticia = Noticia::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'noticia' => $noticia
        );

        return view('admin.paginas.noticias.editar_noticia', $data);
    }

    public function viewFilesNoticia(Request $request)
    {
        $noticia = Noticia::find($request->id);
        $ds = DIRECTORY_SEPARATOR;
        $diretorio = public_path() . "/uploads/noticias/" . $noticia->link . "/fotos" . "/";
        $result  = array();
 
        $files = scandir($diretorio);                 
        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if ( '.'!=$file && '..'!=$file) {
                    $obj['name'] = $file;
                    $obj['size'] = filesize($diretorio.$ds.$file);
                    $result[] = $obj;
                }
            }
        }
        
        header('Content-type: text/json');
        header('Content-type: application/json');
        return $result;
    }

    public function editarUploadImagens(Request $request)
    {
        $noticia = Noticia::find($request->id);
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move('uploads/noticias/' . $noticia->link . "/fotos" . "/", $imageName);

        return response()->json(['success' => 'arquivo enviado com sucesso', 'arquivo' => $imageName]);
    }

    public function editarDeleteImagens(Request $request)
    {
        $noticia = Noticia::find($request->id);
        $filename = $request->get('filename');
        $path = public_path() . '/uploads/noticias/' . $noticia->link . "/fotos" . "/" . $filename;
        $noticia->imagens = str_replace($filename . ";", "", $noticia->imagens);
        $noticia->save();
        if (file_exists($path)) {
            unlink($path);
        }
        
        return $filename;
    }

    public function editarNoticia(Request $request) 
    {
        $noticia = Noticia::find($request->input('id'));

        if( $noticia->titulo_noticia === $request->input('titulo_noticia') && 
            $noticia->descricao_noticia === $request->input('descricao_noticia') &&
            $noticia->imagens === $request->input('imagens') &&
            ($noticia->foto_destaque === $request->input('foto_destaque_name') || $request->input('foto_destaque_name') == "Selecione um arquivo") && 
            ($noticia->foto_capa === $request->input('foto_capa_name') || $request->input('foto_capa_name') == "Selecione um arquivo") && 
            ($noticia->foto_interna === $request->input('foto_interna_name') || $request->input('foto_interna_name') == "Selecione um arquivo")
        ) {

            return redirect()->route('pagina.noticia')->with('mensagem', 'A noticia já está atualizada!');
        } else {

            $noticia->descricao_noticia = $request->input('descricao_noticia');

            // se não houver imagens cadastrar elas
            if(!is_dir(public_path() . "/uploads/noticias/" . $noticia->link)) {
		        // dd(public_path());
                mkdir(public_path() . "/uploads/noticias/" . $noticia->link);
            }

            if(!empty($request->file('foto_destaque'))) {

                $noticia->foto_destaque = $this->noticiaUploadFile($request->file('foto_destaque'), $noticia->link);
            }

            if(!empty($request->file('foto_capa'))) {

                $noticia->foto_capa = $this->noticiaUploadFile($request->file('foto_capa'), $noticia->link);
            }

            if(!empty($request->file('foto_interna'))) {

                $noticia->foto_interna = $this->noticiaUploadFile($request->file('foto_interna'), $noticia->link);
            }
            
            // caso tenha imagens, alterar elas
            if($noticia->titulo_noticia != $request->input('titulo_noticia')) {

                $noticia->titulo_noticia = $request->input('titulo_noticia');
                $oldLink = $noticia->link;
                $noticia->link = URLify::filter($request->input('titulo_noticia')) . '-' . date('d-m-Y', strtotime($noticia->created_at));
                rename(public_path() . "\\uploads\\noticias\\" . $oldLink, public_path() . "\\uploads\\noticias\\" . $noticia->link);
            }
            
            $noticia->imagens = $request->input('imagens');

            if(!empty($request->file('foto_destaque')) && $noticia->foto_destaque != $request->input('foto_destaque_name')) {
                unlink(public_path() . '/uploads/noticias/' . $noticia->link . '/' . $noticia->foto_destaque);
                $noticia->foto_destaque = $this->noticiaUploadFile($request->file('foto_destaque'), $noticia->link);
            }

            if(!empty($request->file('foto_capa')) && $noticia->foto_capa != $request->input('foto_capa_name')) {

                unlink(public_path() . '/uploads/noticias/' . $noticia->link . '/' . $noticia->foto_capa);
                $noticia->foto_capa = $this->noticiaUploadFile($request->file('foto_capa'), $noticia->link);
            }

            if(!empty($request->file('foto_interna')) && $noticia->foto_interna != $request->input('foto_interna_name')) {

                unlink(public_path() . '/uploads/noticias/' . $noticia->link . '/' . $noticia->foto_interna);
                $noticia->foto_interna = $this->noticiaUploadFile($request->file('foto_interna'), $noticia->link);
            }

            $noticia->save();

            return redirect()->route('pagina.noticia')->with('mensagem', 'A noticia foi atualizada com sucesso!');
        }
    }

    public function deletarNoticia(Request $request) 
    {
        
        $noticia = Noticia::find($request->id);
        $noticia->delete();

        return redirect()->route('pagina.noticia')->with('mensagem', 'A noticia foi deletada com sucesso!');
    }

    public function noticiaUploadFile($file, $link) {
        
        $file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();
        $file_extension = $file->getClientOriginalExtension();
        $expensions = array('jpeg', 'jpg', 'png', 'gif');

        if(in_array($file_extension, $expensions) === false) {

            return redirect()->route('pagina.noticia')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
        } else if($file_size > 2097152) {

            return redirect()->route('pagina.noticia')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
        } else {

            $file->move('uploads/noticias/' . $link . '/', $file_name);

            return $file_name;
        }
    }
}
