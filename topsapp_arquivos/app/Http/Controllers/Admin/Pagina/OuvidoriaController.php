<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ouvidoria;

class OuvidoriaController extends Controller
{
    public function ouvidoria()
    {
        $ouvidoria = Ouvidoria::find(1);

        $ouvidoria->atualizacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($ouvidoria->updated_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'ouvidoria' => $ouvidoria
        );

        return view('admin.paginas.ouvidoria', $data);
    }

    public function atualizarDados(Request $request)
    {
        $ouvidoria = Ouvidoria::find(1);

        if ($ouvidoria->titulo === $request->input('titulo') &&
            $ouvidoria->descricao === $request->input('descricao')
        ) {
            
            return redirect()->route('pagina.ouvidoria')->with('mensagem', 'Os dados da página já estão atualizados!');
        } else {

            $ouvidoria->titulo = $request->input('titulo');
            $ouvidoria->descricao = $request->input('descricao');
            $ouvidoria->save();

            return redirect()->route('pagina.ouvidoria')->with('mensagem', 'Os dados da página foram atualizados com sucesso!');
        }
    }
}
