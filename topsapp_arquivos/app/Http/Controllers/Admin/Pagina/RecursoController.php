<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Info_recurso;
use App\Models\Filtro_recurso;
use App\Models\Recursos_sistema;

class RecursoController extends Controller
{
    // pagina descricao recurso
    public function descricao()
    {
        $infoRecurso = Info_recurso::find(1);
        $infoRecurso->atualizacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($infoRecurso->updated_at)));
        
        $data = array(  'usuario' => $_SESSION['usuario'],
                        'info_recurso' => $infoRecurso
        );

        return view('admin.paginas.recursos.recursos_descricao', $data);
    }

    // atualizar dados da descrição de recurso
    public function atualizarDados(Request $request)
    {
        $textoDescricao = $request->input('texto');
        $infoRecurso = Info_recurso::find(1);

        if($infoRecurso->texto === $textoDescricao) {

            return redirect()->route('pagina.recurso.descricao')->with('mensagem', 'A descrição da página já está atualizada!');
        } else {

            $infoRecurso->texto = $textoDescricao;
            $infoRecurso->save();

            return redirect()->route('pagina.recurso.descricao')->with('mensagem', 'A descrição da página recursos foi atualizada com sucesso!');
        }
    }

    // pagina listar recursos e filtros do sistema
    public function sistema()
    {
        $filtroRecurso = Filtro_recurso::all();

        $recursosSistema = Recursos_sistema::all();
        
        foreach($recursosSistema as $key => $value) {

            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'filtros_recurso' => $filtroRecurso,
                        'recursos_sistema' => $recursosSistema
        );

        return view('admin.paginas.recursos.recursos_sistema', $data);
    }

    // Inicio cadastro de filtros
    public function cadastrarFiltro(Request $request) 
    {

        $inputFiltro = $request->input('nome');

        $filtros = Filtro_recurso::all();

        foreach ($filtros as $key => $value) {
            
            if($value->nome === $inputFiltro) {
            
                return redirect()->route('pagina.recurso.sistema')->with('invalido', 'O filtro já está cadastrado!');
            }
        }

        $filtro = new Filtro_recurso;
        $filtro->nome = $inputFiltro;
        $filtro->save();

        return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O filtro foi cadastrado com sucesso!');
    }
    // Fim cadastro de filtro

    // Inicio deletar filtro
    public function deletarFiltro(Request $request) 
    {

        $filtroRecurso = Filtro_recurso::find($request->id);
        $filtroRecurso->delete();

        return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O filtro foi deletado com sucesso! ');
    }
    // Fim deletar filtro

    // Inicio cadastro de recurso
    public function viewCadastrarRecurso() 
    {
        
        $filtroRecurso = Filtro_recurso::all();

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'filtros_recurso' => $filtroRecurso
        );

        return view('admin.paginas.recursos.cadastrar_recurso', $data);
    }

    public function cadastrarRecurso(Request $request) 
    {
        if ($request->hasFile('file')) {
            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.recurso.sistema')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.recurso.sistema')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/recurso', $file_name);

                $recurso = new Recursos_sistema;
                $recurso->filtro_id = $request->input('filtro');
                $recurso->titulo = $request->input('titulo');
                $recurso->descricao = $request->input('descricao');
                $recurso->imagem = $file_name;
                $recurso->save();

                return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O recurso foi cadastrado com sucesso!');
            }
        }
    }
    // Fim cadastro de recurso

    // Inicio editar recurso
    public function viewEditarRecurso(Request $request) 
    {
        
        $filtroRecurso = Filtro_recurso::all();
        $recurso = Recursos_sistema::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'filtros_recurso' => $filtroRecurso,
                        'recurso' => $recurso
        );

        return view('admin.paginas.recursos.editar_recurso', $data);
    }

    public function editarRecurso(Request $request) 
    {

        $recurso = Recursos_sistema::find($request->input('id'));

        if( $recurso->filtro_id == $request->input('filtro') && 
            $recurso->titulo === $request->input('titulo') && 
            $recurso->descricao === $request->input('descricao') &&
            $recurso->imagem === $request->input('file-name')
        ) {

            return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O recurso já está atualizado!');
        } else if($recurso->imagem === $request->input('file-name')) {

            $recurso->filtro_id = $request->input('filtro');
            $recurso->titulo = $request->input('titulo');
            $recurso->descricao = $request->input('descricao');
            $recurso->save();

            return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O recurso foi atualizado com sucesso!');
        } else {
            
            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.recurso.sistema')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.recurso.sistema')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/recurso', $file_name);

                $recurso->filtro_id = $request->input('filtro');
                $recurso->titulo = $request->input('titulo');
                $recurso->descricao = $request->input('descricao');
                $recurso->imagem = $file_name;
                $recurso->save();

                return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O recurso foi atualizado com sucesso!');
            }
        }
    }
    // Fim editar recurso

    // inicio deletar recurso
    public function deletarRecurso(Request $request) 
    {
        
        $recurso = Recursos_sistema::find($request->id);
        $recurso->delete();

        return redirect()->route('pagina.recurso.sistema')->with('mensagem', 'O recurso foi deletado com sucesso!');
    }
    // fim deletar recurso
}
