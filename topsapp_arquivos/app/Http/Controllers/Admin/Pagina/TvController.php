<?php

namespace App\Http\Controllers\Admin\Pagina;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tv;

class TvController extends Controller
{
    public function tv()
    {
        $videos = Tv::all();
        
        foreach ($videos as $key => $value) {
            
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'videos' => $videos
        );

        return view('admin.paginas.tv.index', $data);
    }

    public function viewCadastrarVideo() 
    {
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.paginas.tv.cadastrar_video', $data);
    }

    public function cadastrarVideo(Request $request)
    {
        $video = new Tv;

        if ($request->hasFile('file')) {
            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif', 'PNG');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.tv')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.tv')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/tv', $file_name);

                $video->titulo = $request->input('titulo');
                $video->video = $request->input('video');
                $video->imagem = $file_name;
                $video->save();

                return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo foi cadastrado com sucesso!');
            }
        } else {

            $video->titulo = $request->input('titulo');
            $video->video = $request->input('video');
            $video->save();

            return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo foi cadastrado com sucesso!');
        }
    }

    public function viewEditarVideo(Request $request)
    {
        $video = Tv::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'video' => $video
        );

        return view('admin.paginas.tv.editar_video', $data);
    }

    public function editarVideo(Request $request)
    {
        $video = Tv::find($request->input('id'));

        if( $video->titulo === $request->input('titulo') && 
            $video->video === $request->input('video') &&
            $video->imagem === $request->input('file-name')
        ) {
            
            return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo já está atualizado!');
        } else if($video->imagem === $request->input('file-name')) {

            $video->titulo = $request->input('titulo');
            $video->video = $request->input('video');
            $video->save();

            return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo foi atualizado com sucesso!');
        } else {

            $file_name = $request->file->getClientOriginalName();
            $file_size = $request->file->getClientSize();
            $file_extension = $request->file->getClientOriginalExtension();
            $expensions = array('jpeg', 'jpg', 'png', 'gif', 'PNG');

            if(in_array($file_extension, $expensions) === false) {

                return redirect()->route('pagina.tv')->with('invalido', 'Extensão não permitida, por favor escolha um arquivo JPEG ou PNG.');
            } else if($file_size > 2097152) {

                return redirect()->route('pagina.tv')->with('invalido', 'O tamanho do arquivo não pode exceder 2 MB');
            } else {

                $request->file->move('uploads/tv', $file_name);

                $video->titulo = $request->input('titulo');
                $video->video = $request->input('video');
                $video->imagem = $file_name;
                $video->save();

                return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo foi atualizado com sucesso!');
            }
        }
    }

    public function deletarVideo(Request $request)
    {
        $video = Tv::find($request->id);
        $video->delete();

        return redirect()->route('pagina.tv')->with('mensagem', 'O vídeo foi deletado com sucesso!');
    }
}
