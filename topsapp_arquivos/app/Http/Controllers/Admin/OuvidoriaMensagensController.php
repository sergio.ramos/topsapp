<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ouvidoria_mensagens;

class OuvidoriaMensagensController extends Controller
{
    public function ouvidorias()
    {
        $mensagens = Ouvidoria_mensagens::all();

        foreach ($mensagens as $key => $value) {
            $value->data_mensagem = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }
        $data = array(  'usuario' => $_SESSION['usuario'],
                        'mensagens' => $mensagens
        );

        return view('admin.ouvidoria.index', $data);
    }

    public function visualizarMensagem(Request $request)
    {
        $mensagem = Ouvidoria_mensagens::find($request->id);

        $mensagem->data_mensagem = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($mensagem->created_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'mensagem' => $mensagem
        );

        return view('admin.ouvidoria.visualizar_mensagem', $data);
    }
}
