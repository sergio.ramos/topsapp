<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dados_site;

class DadosController extends Controller
{
    public function dados()
    {
        $site = Dados_site::find(1);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'site' => $site
        );

        return view('admin.dados.index', $data);
    }

    public function atualizarDados(Request $request)
    {
        $site = Dados_site::find(1);

        if( $site->telefones === $request->input('telefones') &&
            $site->email === $request->input('email') &&
            $site->endereco === $request->input('endereco') &&
            $site->facebook === $request->input('facebook') &&
            $site->twitter === $request->input('twitter') &&
            $site->youtube === $request->input('youtube') &&
            $site->receber_info === $request->input('receber_info')
        ) {

            return redirect()->route('dados.site')->with('mensagem', 'Os dados já estão atualizados');
        } else {

            $site->telefones = $request->input('telefones');
            $site->email = $request->input('email');
            $site->endereco = $request->input('endereco');
            $site->facebook = $request->input('facebook');
            $site->twitter = $request->input('twitter');
            $site->youtube = $request->input('youtube');
            $site->receber_info = $request->input('receber_info');

            $site->save();
            return redirect()->route('dados.site')->with('mensagem', 'Os dados foram atualizados com sucesso!');
        }
    }
}
