<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use App\User;

class UsuarioController extends Controller
{
    public function usuarios()
    {
        $users = User::all();

        foreach ($users as $key => $value) {
            // alterando formato de data e hora da visita
            $value->visita = $this->convertDate('%d de %B de %Y ás ', $value->visita);

            // alterando formato de data e hora do cadastro
            $value->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'users' => $users
        );

        return view('admin.usuario.index', $data);
    }

    public function visualizarUsuario(Request $request)
    {
        $user = User::find($request->id);

        $user->visita = $this->convertDate('%d de %B de %Y ás ', $user->visita);

        $user->data_cadastro = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($user->created_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'user' => $user
        );

        return view('admin.usuario.visualizar_usuario', $data);

    }

    public function viewCadastrarUsuario()
    {
        $data = array(  'usuario' => $_SESSION['usuario']);

        return view('admin.usuario.cadastrar_usuario', $data);
    }

    public function cadastrarUsuario(Request $request)
    {
        try {
            $user = new User;

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->visita = date('Y-m-d H:i:s');
            $user->pusuarios = ($request->input('pusuarios') === 'on') ? 1 : 0;
            $user->psobre = ($request->input('psobre') === 'on') ? 1 : 0;
            $user->pouvidoria = ($request->input('pouvidoria') === 'on') ? 1 : 0;
            $user->pnoticias = ($request->input('pnoticias') === 'on') ? 1 : 0;
            $user->pmensagens = ($request->input('pmensagens') === 'on') ? 1 : 0;
            $user->ptv = ($request->input('ptv') === 'on') ? 1 : 0;
            $user->precursos = ($request->input('precursos') === 'on') ? 1 : 0;
            $user->pslide = ($request->input('pusuarios') === 'on') ? 1 : 0;
            $user->save();

            return redirect()->route('usuarios')->with('mensagem', 'O usuário foi cadastrado com sucesso!');
        } catch (QueryException $ex) {
            
            if($ex->getCode() === "23000") {
                return redirect()->route('usuarios')->with('invalido', 'O usuário já está cadastrado no sistema!');
            } else {
                return redirect()->route('usuarios')->with('invalido', 'Erro SQL ao cadastrar usuário!');
            }
        }
    }

    public function viewEditarUsuario(Request $request)
    {
        $user = User::find($request->id);

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'user' => $user
        );

        return view('admin.usuario.editar_usuario', $data);
    }

    public function editarUsuario(Request $request)
    {
        $user = User::find($request->id);

        if( $user->name === $request->input('name') && 
            $user->email === $request->input('email') && 
            $user->password === $request->input('password') && 
            $user->pusuarios === (($request->input('pusuarios') === 'on') ? 1 : 0) && 
            $user->psobre === (($request->input('psobre') === 'on') ? 1 : 0) && 
            $user->pouvidoria === (($request->input('pouvidoria') === 'on') ? 1 : 0) && 
            $user->pnoticias === (($request->input('pnoticias') === 'on') ? 1 : 0) && 
            $user->pmensagens === (($request->input('pmensagens') === 'on') ? 1 : 0) && 
            $user->ptv === (($request->input('ptv') === 'on') ? 1 : 0) && 
            $user->precursos === (($request->input('precursos') === 'on') ? 1 : 0) && 
            $user->pslide === (($request->input('pusuarios') === 'on') ? 1 : 0)
        ) {

            return redirect()->route('usuarios')->with('mensagem', 'O usuário já está atualizado.');
        } else {

            $user->name = $request->input('name');
            $user->email = $request->input('email');

            if($user->password != $request->input('password')) {

                $user->password = bcrypt($request->input('password'));
            }
            
            $user->visita = date('Y-m-d H:i:s');
            $user->pusuarios = ($request->input('pusuarios') === 'on') ? 1 : 0;
            $user->psobre = ($request->input('psobre') === 'on') ? 1 : 0;
            $user->pouvidoria = ($request->input('pouvidoria') === 'on') ? 1 : 0;
            $user->pnoticias = ($request->input('pnoticias') === 'on') ? 1 : 0;
            $user->pmensagens = ($request->input('pmensagens') === 'on') ? 1 : 0;
            $user->ptv = ($request->input('ptv') === 'on') ? 1 : 0;
            $user->precursos = ($request->input('precursos') === 'on') ? 1 : 0;
            $user->pslide = ($request->input('pusuarios') === 'on') ? 1 : 0;
            $user->save();

            return redirect()->route('usuarios')->with('mensagem', 'O usuário foi atualizdo com sucesso!');
        }
    }

    public function deletarUsuario(Request $request)
    {
        if($_SESSION['usuario']['id'] == $request->id) {

            return redirect()->route('usuarios')->with('invalido', 'Não é possível deletar seu usuário');
        } else {

            $user = User::find($request->id);
            $user->delete();

            return redirect()->route('usuarios')->with('mensagem', 'O usuário foi deletado com sucesso!');
        }
    }
}
