<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Demo;

class DemoController extends Controller
{
    public function demo()
    {
        $demos = Demo::all();

        foreach ($demos as $key => $value) {

            $value->data_solicitacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($value->created_at)));
        }

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'demos' => $demos
        );

        return view('admin.demo.index', $data);
    }

    public function visualizarDemo(Request $request)
    {
        $demo = Demo::find($request->id);

        $demo->data_solicitacao = $this->convertDate('%d de %B de %Y ás ', date('Y-m-d H:i:s', strtotime($demo->created_at)));

        $data = array(  'usuario' => $_SESSION['usuario'],
                        'demo' => $demo
        );

        return view('admin.demo.visualizar_demo', $data);
    }

}
