<?php

namespace App\Http\Controllers\Topapp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TopappController extends Controller
{
    public function index()
    {
        $data = array(  
        );

        return view('topappTecnico.index', $data);
    }

    public function indexCentral()
    {
        $data = array(  
        );

        return view('topappCentral.index', $data);
    }
}
