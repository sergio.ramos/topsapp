<?php

namespace App\Http\Controllers\Topmaps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// config email use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Mail;
use App\Mail\TopvozMail;

class TopmapsController extends Controller
{
    public function index()
    {
        $data = array(  
        );

        return view('topmaps.index', $data);
    }
}
