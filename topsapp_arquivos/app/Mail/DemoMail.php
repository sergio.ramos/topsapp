<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DemoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $telefone_comercial;
    public $telefone_celular;
    public $email;
    public $estado;
    public $cidade;
    public $endereco;
    public $numero;
    public $cep;
    public $site;
    public $responsavel;
    public $conheceu;
    public $descricao;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nome, $telefone_comercial, $telefone_celular, $email, $estado, $cidade, $endereco, $numero, $cep, $site, $responsavel, $conheceu, $descricao)
    {
        $this->nome = $nome;
        $this->telefone_comercial = $telefone_comercial;
        $this->telefone_celular = $telefone_celular;
        $this->email = $email;
        $this->estado = $estado;
        $this->cidade = $cidade;
        $this->endereco = $endereco;
        $this->numero = $numero;
        $this->cep = $cep;
        $this->site = $site;
        $this->responsavel = $responsavel;
        $this->conheceu = $conheceu;
        $this->descricao = $descricao;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email)->subject('Demo - TOPSAPP')->view('emails.demo');
    }
}
