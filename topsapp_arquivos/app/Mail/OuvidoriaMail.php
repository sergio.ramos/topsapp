<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OuvidoriaMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $email;
    public $estado;
    public $cidade;
    public $cep;
    public $telefone;
    public $prioridade;
    public $tipo_pessoa;
    public $mensagem;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nome, $email, $estado, $cidade, $cep, $telefone, $prioridade, $tipo_pessoa, $mensagem)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->estado = $estado;
        $this->cidade = $cidade;
        $this->cep = $cep;
        $this->telefone = $telefone;
        $this->prioridade = $prioridade;
        $this->tipo_pessoa = $tipo_pessoa;
        $this->mensagem = $mensagem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email)->subject('Ouvidoria - TOPSAPP')->view('emails.ouvidoria');
    }
}
