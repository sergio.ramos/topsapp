<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContatoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $email;
    public $site;
    public $telefone;
    public $departamento;
    public $descricao;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nome, $email, $site, $telefone, $departamento, $descricao)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->site = $site;
        $this->telefone = $telefone;
        $this->departamento = $departamento;
        $this->descricao = $descricao;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email)->subject('Contato - TOPSAPP')->view('emails.contato');
    }
}
