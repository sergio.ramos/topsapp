@extends(('template.modulocobranca'))
@section('title')
    
    @parent
@stop

@section('css_pagina')

    {{-- <link rel="stylesheet" href="{{asset('modulocobrancaArquivos/css/home.css')}}"> --}}
    <link type="text/css" rel="stylesheet" href="{{asset('modulocobrancaArquivos/css/style.css')}}">

@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            <div id="particles-js" style="width: 100%;">

            </div>
            
            <div class="preencher">
            
            </div>
            
            <div class="conteudo">
                
                <div class="cinza">
                
                    <img src="{{asset('modulocobrancaArquivos/img/monitor.png')}}">
                    
                    <div class="texto-topo">
                    
                        <p>
                            DIFERENCIAIS
                            <span></span>
                        </p>
                        
                        <span>
                            O MÓDULO DE COBRANÇA foi pensado e desenvolvido para ser uma importante ferramenta que ajudará a diminuir o número de inadimplentes de maneira fácil e rápida. Disponibilizando diversas funcionalidades, o MÓDULO DE COBRANÇA será seu aliado na saúde financeira de sua empresa, provendo renegociações, cobranças e consultas, tudo visando manter a boa relação com o cliente.
                        </span>
                    
                    </div>
                
                </div>
                
                <div class="azul">
                
                    <div class="azul-aln">
                        
                        <p>
                            3 MOTIVOS PARA VOCÊ CONTRATAR O MÓDULO DE COBRANÇA
                            <span></span>
                        </p>
                        
                        <div class="itens-azul">
                        
                            <div class="item-azul">
                            
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_08.png')}}">
                                <span>Página completa e intuitiva para controle dos clientes inadimplentes.</span>
                            
                            </div>
                            
                            <div class="item-azul">
                            
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_10.png')}}">
                                <span>Conte com a já conhecida eficiência e confiabilidade de um produto TopSapp.</span>
                            
                            </div>
                            
                            <div class="item-azul">
                            
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_12.png')}}">
                                <span>Esqueça a dor de cabeça e gerencie facilmente a cobrança em seu provedor!</span>
                            
                            </div>
                         
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="recursos">
                
                    <div class="recursos-aln">
                    
                        <p>
                            RECURSOS
                            <span></span>
                        </p>
                        
                        <div class="recursos-div">
                        
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_18.png')}}">
                            </div>
                                <span>Consulta ao SPC</span>
                            
                            </div>
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_20.png')}}">
                            </div>
                                <span>Parcelamento de títulos</span>
                            
                            </div>
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_22.png')}}">
                            </div>
                                <span>Recebimento agendado</span>
                            
                            </div>
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_25.png')}}">
                            </div>
                                <span>Acompanhamento da negociação</span>
                            
                            </div>
                        
                        </div>
                        
                        <div class="recursos-div tres">
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_38.png')}}">
                            </div>
                                <span>Controle de inadimplentes<br/> por período</span>
                            
                            </div>
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_32.png')}}">
                            </div>
                                <span>Setorização por tempo<br/> em atraso</span>
                            
                            </div>
                            
                            <div class="recurso">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_35.png')}}">
                            </div>
                                <span>Relatório de envio de SMS<br/> e e-mails</span>
                            
                            </div>
                        
                        </div>
                        
                        <div class="solicitar">
                            {{-- <div class="banner"> --}}
                                <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                            {{-- </div> --}}
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="bordas-div">
                    
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">
                    
                        <a href="tel:{{$dados_site->telefones}}">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('modulocobrancaArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>{{$dados_site->telefones}}</span>    
                            </div>
                        </a>
                        
                        <a href="mailto:{{$dados_site->email}}">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('modulocobrancaArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>{{$dados_site->email}}</span>
                            </div>
                        </a>

                        <a target="_blank" href="{{$dados_site->facebook}}" class="contato">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>
                        
                        <a target="_blank" href="{{$dados_site->instagram}}" class="contato">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>{{$dados_site->instagram}}</span>
                        </a>
                        
                        <a target="_blank" href="{{$dados_site->twitter}}" class="contato">
                            <div>
                                <img src="{{asset('modulocobrancaArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                    
                    </div>
                
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script src="{{asset('modulocobrancaArquivos/js/particles.js')}}"></script>
    <script src="{{asset('modulocobrancaArquivos/js/app.js')}}"></script>

    <script>

        if($(window).width() >= 1024) {
            $('div.cinza img').hide()
            $('div.cinza div.texto-topo').hide()
        }

        $(window).scroll(function() {

            if( $(window).scrollTop() < 50 ) {
                $('div.cinza img').hide()
                $('div.cinza div.texto-topo').hide()
            } else {
                $('div.cinza img').show()
                $('div.cinza div.texto-topo').show()
            }

            if ( $(window).scrollTop() >= 650 ) {
                $('#particles-js').css('display', 'flex');
            } else if($(window).scrollTop() >= 177 && janelaWidth < 576) {
                $('#particles-js').css('display', 'flex');
            } else {
                $('#particles-js').attr('style', '');

            }
        })

    </script>

@endsection