@extends('layouts.app')


@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/redefinir.css')}}">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center align-items-center">
                    <img src="{{asset('assets/img/logo_tip.svg')}}" alt="logo topsapp">
                </div>

                <div class="card-body d-flex justify-content-center align-items-center">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group input-group">
                            <label for="email" class="email">{{ __('E-Mail') }}</label>
                            <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </span>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group input-group">
                            <label for="password" class="password">{{ __('Senha') }}</label>
                            {{-- <span class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </span> --}}

                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group input-group">
                            <label for="password-confirm" class="password-confirm">{{ __('Confirme a Senha') }}</label>
                            {{-- <span class="input-group-addon">
                                <i class="fa fa-key"></i>
                            </span> --}}

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Redefinir') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
