@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div aria-labelledby="recuperarSenhaLabel" class="modal fade show" id="recuperarSenha" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="recuperarSenhaLabel">Recuperar senha</h5>
                            <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form id="form-recuperarSenha" method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="form-group input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    {{-- <input class="form-control" id="email_senha" name="email_senha" type="email" placeholder="Insira o e-mail..."> --}}

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button id="btn-form-recuperarSenha" style="display: none" type="submit"></button>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Enviar') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            {{-- <button class="btn btn-primary btn-block" id="recuperarSenha">Enviar</button> --}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header d-flex justify-content-center align-items-center">
                    <img src="{{asset('assets/img/logo_tip.svg')}}" alt="logo topsapp">
                </div>

                <div class="card-body d-flex justify-content-center align-items-center">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group input-group" @error('email') style="margin: 0;"@enderror>
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>

                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="kt-checkbox kt-checkbox--brand">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Lembrar') }}
                                <span></span>
                            </label>
                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Entrar') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link lembrar" {{--data-target="#recuperarSenha" data-toggle="modal"--}} href="{{ route('password.request') }}">
                                    {{ __('Esqueceu a senha?') }}
                                </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection

@section('scripts_pagina')

    <script>
    
        // $('button#recuperarSenha').click(function() {
        //     $('form#form-recuperarSenha').submit()
        // })
        
    </script>

@endsection
