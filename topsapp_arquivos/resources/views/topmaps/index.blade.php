@extends(('template.topmaps'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('topmapsArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('topmapsArquivos/css/style.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            
            <div class="inicio">
        
                <div class="topo">
                
                
                </div>
                
            </div>
            
            <div class="fim">
                
                <div class="azul">
                   
                   <p>Com o TOPMAPS é possível projetar, documentar e gerenciar custos de toda a infraestrutura da sua rede. Toda essa facilidade utilizando elementos que ilustram os equipamentos que compõem a sua rede.</p>
               
                </div>
               
               <div class="ponta"></div>
            
                <div class="background">
                    
                    <div class="itens">

                        <h1>Características do TOPMAPS</h1>
                    
                        <div class="item-esquerda">
                        
                            <img src="{{asset('topmapsArquivos/img/praticidade.png')}}">
                            <p>
                                <span>Praticidade<font style="visibility:hidden;" class="borda-esquerda-span"></font></span>
                                <font>Com o TOPMAPS, o usuário tem controle da sua rede de forma dinâmica, podendo acessar o sistema de qualquer local com disponibilidade de conexão, com isso o TOPMAPS estará sempre disponível para você e sua equipe.</font>
                            </p>
                        
                        </div>
                        
                        <div class="item-direita">
                        
                            <p>
                                <span><font class="borda-direita-font"></font>Eficiência</span>
                                <font>Desenvolvido para uma maior eficácia e de fácil manuseio, o usuário utiliza o sistema na localização de problemas, consultas ou atualizações de registros. Mantendo o controle da rede e antecipando a resolução de possíveis problemas.</font>
                            </p>
                            
                            <img src="{{asset('topmapsArquivos/img/eficiencia.png')}}">
                        
                        </div>

                        <div class="item-esquerda">

                            <img src="{{asset('topmapsArquivos/img/seguranca.png')}}">
                            <p>
                                <span>Segurança<font class="borda-esquerda-span"></font></span>
                                <font>Mantenha as informações de sua rede de forma confiável, garantindo a integridade dos dados que serão utilizados para dar suporte às manutenções e expansões. Através de um sistema que utiliza ferramentas que controlam e facilitam, com segurança, os serviços realizados em sua rede.</font>
                            </p>
                        
                        </div>

                        <h1>Opções do TOPMAPS</h1>

                        <div class="item-direita">
                        
                            <p>
                                <span><font class="borda-direita-font"></font>Cabos</span>
                                <font>Gerencie tubos e fibras, estime gastos com cabos em seu projeto e ainda administre as reservas que podem ser vinculadas aos postes.</font>
                            </p>
                            
                            <img src="{{asset('topmapsArquivos/img/cabos.png')}}">
                        
                        </div>

                        <div class="item-esquerda">
                        
                            <img src="{{asset('topmapsArquivos/img/Marcadores.png')}}">
                            <p>
                                <span>Marcadores<font class="borda-esquerda-span"></font></span>
                                <font>Adicione um ícone personalizado, altere a cor, crie uma identificação e adicione uma descrição.  Você pode usar os marcadores de lugar para indicar informações no mapa, como residencias, empresas ou qualquer localização relevante para sua rede.</font>
                            </p>
                        
                        </div>

                        <div class="item-direita">
                        
                            <p>
                                <span><font class="borda-direita-font"></font>Distâncias</span>
                                <font>Defina pontos, rotas, preímetros no mapa e calcule a distância entre eles.</font>
                            </p>

                            <img src="{{asset('topmapsArquivos/img/distancias.png')}}">
                        
                        </div>

                        <div class="item-esquerda">

                            <img src="{{asset('topmapsArquivos/img/postes.png')}}">

                            <p>
                                <span>Postes<font class="borda-esquerda-span"></font></span>
                                <font>Adicione os postes que fazem parte de seu projeto. Configure valores para os postes, identifique, informe a composição do poste, reservas de cabos e adicione equipamentos.</font>
                            </p>

                        </div>
                    
                        <div class="item-direita">

                            <p>
                                <span><font class="borda-direita-font"></font>Splitters</span>
                                <font>Quando necessário a utilização de splitters para representação na rede, você pode adicioná-lo nos equipamentos em que serão inseridos, configurando a sua representação no projeto e a quantidade de saídas.</font>
                            </p>

                            <img src="{{asset('topmapsArquivos/img/splitters.png')}}">
                        
                        </div>
                        
                        <div class="item-esquerda">
                        
                            <img src="{{asset('topmapsArquivos/img/cto.png')}}">

                            <p>
                                <span>CTO<font class="borda-esquerda-span"></font></span>
                                <font>Adicione os equipamentos de atendimentos para os clientes, como CTO, em seu projeto. Tenha sempre à disposição a representação das ligações e das portas, livres e utilizadas, para cada CTO. É possível adicionar splitters também nas CTO e imprimir a representação das ligações que estão sendo feitas.</font>
                            </p>
                        
                        </div>

                        <div class="item-direita">
                        
                            <p>
                                <span><font class="borda-direita-font"></font>Relatórios</span>
                                <font>Tenha todas as informações sobre sua rede organizadas em uma planilha disponíveis para impressão, relatórios sobre custos, histórico de alterações, portas disponíveis e informações gerais.</font>
                            </p>
                            
                            <img src="{{asset('topmapsArquivos/img/relatorios.png')}}">
                        
                        </div>
                       
                    </div>
                    
                    
                     <div class="bitens">

                        
                    
                    </div>

                    <div class="solicitar">
                        {{-- <div class="banner"> --}}
                            <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                        {{-- </div> --}}
                    </div>
                    
                </div>

                <div class="bordas-div">
            
                        <div class="borda-esquerda"></div>
                        <div class="borda-direita"></div>
                    
                    </div>
                    
                    <div class="final">
                    
                        <p>
                            CONTATOS
                            <span></span>
                        </p>
                        
                        <div class="contatos">

                            <a href="tel:(66) 3211-0010">
                                <div class="contato">
                                    <div>
                                        <img src="{{asset('topmapsArquivos/img/Layout_44.png')}}">
                                    </div>
                                    <span>(66) 3211-0010</span>
                                </div>
                            </a>

                            <a href="mailto:comercial@topsapp.com.br">
                                <div class="contato">
                                    <div>
                                        <img src="{{asset('topmapsArquivos/img/Layout_47.png')}}">
                                    </div>
                                    <span>comercial@topsapp.com.br</span>
                                </div>
                            </a>

                            <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                                <div>
                                    <img src="{{asset('topmapsArquivos/img/fb.png')}}">
                                </div>
                                <span>Facebook</span>
                            </a>

                            <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                                <div>
                                    <img src="{{asset('topmapsArquivos/img/instagram.PNG')}}">
                                </div>
                                <span>Instagram</span>
                            </a>
                            
                            <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                                <div>
                                    <img src="{{asset('topmapsArquivos/img/Layout_50.png')}}">
                                </div>
                                <span>Twitter</span>
                            </a>
                        
                        </div>
                    
                    </div>
                    
                    <div class="bottom">
                    
                        <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                    
                    </div>
            </div>
            
        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script>
            
        

    </script>
    
@endsection