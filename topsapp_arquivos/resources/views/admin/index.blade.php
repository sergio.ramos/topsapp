@extends(('template.admin'))
@section('title')
    Principal
    @parent
@stop
@section('css_pagina')
	<link rel="stylesheet" href="{{ asset('css/principal.css') }}">
@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Principal</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="fa fa-home kt-font-brand"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Principal
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-2">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Título do Site
						</h3>
						<span class="kt-widget14__desc">
							Defina o titulo para ser exibido no site. Última atualização: {{ $titulo_site->atualizacao }}
						</span>
					</div>
					<form id="titulo-principal" class="kt-form" method="POST" action="{{ route('atualizar.titulo') }}">
						@csrf
						<div class="form-group">
							<input class="form-control kt-margin-r-10" type="text" id="titulo_site" name="titulo_site" value="{{ $titulo_site->titulo_site }}">
							<button type="submit" class="btn btn-brand btn-elevate btn-icon-sm">Atualizar</button>
						</div>
					</form>
					@if(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
							</div>
						</div>
					@endif
				</div>
				<div class="kt-portlet__body pt-3">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Administradores <small>Informações dos administradores do site</small>
								</h3>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
<script>
	let data = @JSON($users)

	// console.log(data)
	
	jQuery(document).ready(function () {

		let datatable = $('.kt-datatable').KTDatatable({
			// datasource definition
			data: {
				type: 'local',
				source: data,
				pageSize: 6,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				height: 450, // datatable's body's fixed height
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			// search: {
			// 	input: $('#generalSearch')
			// },

			// columns definition
			columns: [	
				{
					field: 'name',
					title: 'Nome',
				}, {
					field: 'email',
					title: 'E-mail',
				}, {
					field: 'visita',
					title: 'Última visita',
					type: 'date'
				}, {
					field: 'data_cadastro',
					title: 'Data de cadastro:',
					type: 'date'
				}
			]
		})

		$('form#titulo-principal').validate({
			rules: {
				titulo_site: {
					required: true
				}
			}
		})	

		$('form#titulo-principal').submit(function() {
			if($('input#titulo_site').val() === ""){

				$('div#titulo_site-error').appendTo('#titulo-principal')
				$('div#titulo_site-error').css('display', 'block')
			}
		})

	})
</script>
@stop
