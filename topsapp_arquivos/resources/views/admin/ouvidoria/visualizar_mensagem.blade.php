@extends(('template.admin'))
@section('title')
    Mensagem
    @parent
@stop

@section('css_pagina')

	<link rel="stylesheet" href="{{ asset('css/mensagem.css') }}">

@endsection

@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Ouvidoria</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('ouvidorias') }}" class="kt-subheader__breadcrumbs-link">Ouvidoria</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('visualizar.ouvidoria', $mensagem->id ) }}" class="kt-subheader__breadcrumbs-link">Solicitação</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-headset"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Solicitação
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Solicitação de: {{ $mensagem->nome }}
						</h3>
						<span class="kt-widget14__desc">
							Informações da solicitação.
						</span>
					</div>
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="mensagem">
						<ul>
                            <li><span class="kt-font-boldest">Data:</span > {{ $mensagem->data_mensagem }}</li>
							<li><span class="kt-font-boldest">Nome:</span > {{ $mensagem->nome }}</li>
							<li><span class="kt-font-boldest">E-mail:</span > {{ $mensagem->email }}</li>
							<li><span class="kt-font-boldest">Estado:</span > {{ $mensagem->estado }}</li>
							<li><span class="kt-font-boldest">Cidade:</span > {{ $mensagem->cidade }}</li>
                            <li><span class="kt-font-boldest">CEP:</span > {{ $mensagem->cep }}</li>
							<li><span class="kt-font-boldest">Telefone:</span > {{ $mensagem->telefone }}</li>
							<li><span class="kt-font-boldest">Prioridade:</span > {{ $mensagem->prioridade }}</li>
							<li><span class="kt-font-boldest">Tipo de pessoa:</span > {{ $mensagem->tipo_pessoa }}</li>
							<li><span class="kt-font-boldest">Mensagem:</span > <div class="kt-section__content">{{ $mensagem->mensagem }}</div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop
