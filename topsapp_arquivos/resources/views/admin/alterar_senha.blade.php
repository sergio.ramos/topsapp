@extends(('template.admin'))
@section('title')
    Alterar senha
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Alterar Senha</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('alterar.senha') }}" class="kt-subheader__breadcrumbs-link">Alterar Senha</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-user-lock"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Alterar Senha
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<!--begin: Search Form -->
					@if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@elseif(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@endif
					<form id="senha" action="{{ route('alterar.senha') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="kt-form kt-form--label-right">
							<div class="kt-form__body">
								<div class="kt-section kt-section--first">
									<div class="kt-section__body">
										<div class="row">
											<label class="col-xl-3"></label>
											<div class="col-lg-9 col-xl-6">
												<h3 class="kt-section__title kt-section__title-sm mt-3">Alterar senha:</h3>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label">Senha Atual</label>
											<div class="col-lg-9 col-xl-6">
												<input type="password" class="form-control" name="senha_atual" placeholder="Senha atual">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label">Nova Senha</label>
											<div class="col-lg-9 col-xl-6">
												<input type="password" id="nova_senha" class="form-control" name="nova_senha" placeholder="Nova senha">
											</div>
										</div>
										<div class="form-group form-group-last row">
											<label class="col-xl-3 col-lg-3 col-form-label">Verificar Senha</label>
											<div class="col-lg-9 col-xl-6">
												<input type="password" class="form-control" name="verificar_senha" placeholder="Verificar senha">
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
								
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-xl-3"></div>
									<div class="col-lg-9 col-xl-6">
										<button type="submit" class="btn btn-label-brand btn-bold">Salvar</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<!--end: Search Form -->
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
	
	<script>
		
		$('form#senha').validate({
			rules: {
				senha_atual: {
					required: true
				},
				nova_senha: {
					required: true
				},
				verificar_senha: {
					required: true,
					equalTo: "#nova_senha"
				}
			}
		})
		
	</script>

@endsection