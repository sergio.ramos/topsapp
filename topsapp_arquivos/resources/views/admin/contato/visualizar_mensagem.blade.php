@extends(('template.admin'))
@section('title')
    Mensagem
    @parent
@stop

@section('css_pagina')

	<link rel="stylesheet" href="{{ asset('css/mensagem.css') }}">

@endsection

@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Contato</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('contato') }}" class="kt-subheader__breadcrumbs-link">Contato</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('visualizar.mensagem', $mensagem->id ) }}" class="kt-subheader__breadcrumbs-link">Mensagem</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon-chat"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Mensagem
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Mensagem de: {{ $mensagem->nome_mensagem }}
						</h3>
						<span class="kt-widget14__desc">
							Informações da mensagem.
						</span>
					</div>
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="mensagem">
						<ul>
							<li><span class="kt-font-boldest">Nome:</span > {{ $mensagem->nome_mensagem }}</li>
							<li><span class="kt-font-boldest">E-mail:</span > {{ $mensagem->email_mensagem }}</li>
							<li><span class="kt-font-boldest">Departamento:</span > {{ $mensagem->departamento_mensagem }}</li>
							<li><span class="kt-font-boldest">Data:</span > {{ $mensagem->data_mensagem }}</li>
							<li><span class="kt-font-boldest">Telefone:</span > {{ $mensagem->telefone_mensagem }}</li>
							<li><span class="kt-font-boldest">Site:</span > {{ $mensagem->site_mensagem }}</li>
                            <li><span class="kt-font-boldest">Mensagem:</span > <div class="kt-section__content">{{ $mensagem->mensagem_mensagem }}</div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop
