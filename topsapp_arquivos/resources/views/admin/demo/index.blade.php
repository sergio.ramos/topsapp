@extends(('template.admin'))
@section('title')
    Demo
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Demo</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('demo') }}" class="kt-subheader__breadcrumbs-link">Demo</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-desktop"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Demo
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Pedidos
						</h3>
						<span class="kt-widget14__desc">
							Veja as pessoas que solicitaram a demo.
						</span>
					</div>
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Pedidos <small>Lista de solicitações</small>
								</h3>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
	
	<script>
		
		let data = @JSON($demos)

		$(document).ready(function() {

			let datatable = $('.kt-datatable').KTDatatable({
				// definição
				data: {
					type: 'local',
					source: data,
					pagesize: 5
				},

				layout: {
					scroll: false,
					footer: false
				},

				pagination: true,

				columns: [
					{
						field: 'id',
						title: '#',
						sortable: 'desc',
						width: 40,
						type: 'number',
						selector: false,
						textAlign: 'center'
					}, {
						field: 'nome',
						title: 'Nome'
					}, {
						field: 'data_solicitacao',
						title: 'Data'
					}, {
						field: 'Actions',
						title: 'Ação',
						sortable: false,
						width: 130,
						overflow: 'visible',
						textAlign: 'center',
						template: function(row) {
													return `	<div style="display: flex; margin: 0; padding: 0; justify-content: center">
																	<a href="{{ route('visualizar.demo') }}/` + row.id + `" class="btn btn-hover-brand btn-icon btn-pill" title="Visualizar">
																		<i class="fa fa-search"></i>
																	</a>
																</div>`
						}
					}
				]

			})

		})

	</script>

@endsection