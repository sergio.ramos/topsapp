@extends(('template.admin'))
@section('title')
    Mensagem
    @parent
@stop

@section('css_pagina')

	<link rel="stylesheet" href="{{ asset('css/mensagem.css') }}">

@endsection

@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Demo</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="{{ route('demo') }}" class="kt-subheader__breadcrumbs-link">Demo</a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('visualizar.demo', $demo->id ) }}" class="kt-subheader__breadcrumbs-link">Pedido</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-chalkboard-teacher"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Pedido
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Pedido de: {{ $demo->nome }}
						</h3>
						<span class="kt-widget14__desc">
							Informações do pedido.
						</span>
					</div>
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="mensagem">
						<ul>
                            <li><span class="kt-font-boldest">Data:</span > {{ $demo->data_solicitacao }}</li>
							<li><span class="kt-font-boldest">Nome:</span > {{ $demo->nome }}</li>
							<li><span class="kt-font-boldest">Telefone comercial:</span > {{ $demo->telefone_comercial }}</li>
							<li><span class="kt-font-boldest">Telefone celular:</span > {{ $demo->telefone_celular }}</li>
							<li><span class="kt-font-boldest">E-mail:</span > {{ $demo->email }}</li>
							<li><span class="kt-font-boldest">Estado:</span > {{ $demo->estado }}</li>
							<li><span class="kt-font-boldest">Cidade:</span > {{ $demo->cidade }}</li>
							<li><span class="kt-font-boldest">Endereço:</span > {{ $demo->endereco }}</li>
							<li><span class="kt-font-boldest">Número:</span > {{ $demo->numero }}</li>
							<li><span class="kt-font-boldest">CEP:</span > {{ $demo->cep }}</li>
							<li><span class="kt-font-boldest">Site:</span > {{ $demo->site }}</li>
							<li><span class="kt-font-boldest">Responsável:</span > {{ $demo->responsavel }}</li>
							<li><span class="kt-font-boldest">Conheceu:</span > {{ $demo->conheceu }}</li>
							<li><span class="kt-font-boldest">Mensagem:</span > <div class="kt-section__content">{{ $demo->mensagem }}</div></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop
