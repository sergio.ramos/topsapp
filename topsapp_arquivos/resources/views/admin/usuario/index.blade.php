@extends(('template.admin'))
@section('title')
    Usuários
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Usuários</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('usuarios') }}" class="kt-subheader__breadcrumbs-link">Usuários</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-user"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Usuários
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Usuários Cadastrados
						</h3>
						<span class="kt-widget14__desc">
							Edite as informações dos usuários que administram o seu site.
						</span>
					</div>
					@if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@elseif(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@endif
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Usuáiros <small>Lista de usuários</small>
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<div class="kt-portlet__head-actions">
										<a href="{{ route('cadastrar.usuario') }}" class="btn btn-brand btn-elevate btn-icon-sm">
											<i class="fa fa-plus"></i> Novo Usuário
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
<script>

	let data = @JSON($users)

	let datatable = $('.kt-datatable').KTDatatable({
		// datasource definition
		data: {
			type: 'local',
			source: data,
			pageSize: 6,
		},

		// layout definition
		layout: {
			scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
			footer: false // display/hide footer
		},

		pagination: true,

		// search: {
		// 	input: $('#generalSearch')
		// },

		// columns definition
		columns: [	
			{
				field: 'name',
				title: 'Nome',
				sortable: 'desc'
			}, {
				field: 'data_cadastro',
				title: 'Data de cadastro',
			}, {
				field: 'visita',
				title: 'Última visita'
			}, {
				field: 'email',
				title: 'E-mail'
			}, {
				field: 'Actions',
				title: 'Ação',
				sortable: false,
				width: 130,
				overflow: 'visible',
				textAlign: 'center',
				template: function(row) {
											return `	<div style="display: flex; margin: 0; padding: 0; justify-content: center">
															<a href="{{ route('visualizar.usuario') }}/` + row.id + `" class="btn btn-hover-brand btn-icon btn-pill" title="Visualizar">
																<i class="fa fa-search"></i>
															</a>
															<a href="{{ route('editar.usuario') }}/` + row.id + `" class="btn btn-hover-brand btn-icon btn-pill" title="Editar">
																<i class="la la-edit"></i>
															</a>
															<form id="deletar-usuario" action="{{ route('deletar.usuario') }}/` + row.id + `" method="POST">
																@csrf
																@method('DELETE')
																<button id="kt_sweetalert_delete_usuario" type="button" class="btn btn-hover-danger btn-icon btn-pill" title="Excluir"><i class="la la-trash"></i></button>
															</form>
														</div>`
				}
			}
		]
	})

	$('form#titulo-principal').validate({
		rules: {
			titulo_site: {
				required: true
			}
		}
	})

	$('form#titulo-principal').submit(function() {
		if($('input#titulo_site').val() === ""){

			$('div#titulo_site-error').appendTo('#titulo-principal')
			$('div#titulo_site-error').css('display', 'block')
		}
	})
	
	jQuery(document).ready(function () {
		
		// evento deletar slide, mostra um alert confirmando o delete
		$('button#kt_sweetalert_delete_usuario').click(function(e) {
			swal.fire({
				title: 'Você tem certeza?',
				text: "Você não poderá desfazer essa opção!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Sim, excluir!',
				cancelButtonText: 'Cancelar'
			}).then(function(result) {
				if (result.value) {
					
					$('form#deletar-usuario').submit()
					
					swal.fire(
						'Excluído!',
						'O slide foi exluído.',
						'success'
					)
				}
			})
		})
	})
</script>
@endsection