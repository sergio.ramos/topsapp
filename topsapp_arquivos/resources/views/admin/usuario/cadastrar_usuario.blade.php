@extends(('template.admin'))
@section('title')
    Usuario
    @parent
@stop

@section('css_pagina')

	<link rel="stylesheet" href="{{ asset('css/cadastrar_recurso.css') }}">

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Usuários</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('usuarios') }}" class="kt-subheader__breadcrumbs-link">Usuários</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('cadastrar.usuario') }}" class="kt-subheader__breadcrumbs-link">Novo Usuário</a>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-user-plus"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Cadastrar Usuário
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Novo Usuário
						</h3>
						<span class="kt-widget14__desc">
							Cadastre um novo usuário.
						</span>
					</div>
					<form id="usuario" class="kt-form kt-margin-t-10" method="POST" action="{{ route('cadastrar.usuario') }}" enctype="multipart/form-data">
						@csrf
						<div class="form-group mb-4">
							<label for="name">NOME:</label>
							<input type="text" class="form-control" placeholder="Dígite o nome do usuário" name="name" id="name">
						</div>
						<div class="form-group mb-4">
							<label for="email">E-MAIL:</label>
							<input type="email" class="form-control" placeholder="Dígite o email do usuário" name="email" id="email">
                        </div>
                        <div class="form-group mb-4">
							<label for="password">SENHA:</label>
							<input type="password" class="form-control" placeholder="Dígite a senha do usuário" name="password" id="password">
                        </div>
                        <div class="form-group form-group-last mb-4">
                            <label>PERMISSÕES:</label>
							<div class="kt-checkbox-inline">
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="pusuarios" name="pusuarios"> Usuários 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="psobre" name="psobre"> Sobre 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="pouvidoria" name="pouvidoria"> Ouvidoria 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="pnoticias" name="pnoticias"> Notícias 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="pmensagens" name="pmensagens"> Mensagens 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="ptv" name="ptv"> TopSapp TV 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="precursos" name="precursos"> Recursos 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox" id="pslide" name="pslide"> Slide 
                                    <span></span>
                                </label>
                            </div>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Cadastrar Usuário</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$('form#usuario').validate({
			rules: {
				name: {
					required: true
				},
				email: {
					required: true
				},
				password: {
					required: true
				}
			}
		})		

		$(document).ready(function() {
			
		})
		
	</script>
	
@endsection