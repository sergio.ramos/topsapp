@extends(('template.admin'))
@section('title')
    Tv
    @parent
@stop

@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Topsapp Tv</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.tv') }}" class="kt-subheader__breadcrumbs-link">Topsapp Tv</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('editar.video', $video->id) }}" class="kt-subheader__breadcrumbs-link">Editar Vídeo</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#tv" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-edit"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Editar Vídeo
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Vídeo: {{ $video->titulo }}
						</h3>
						<span class="kt-widget14__desc">
							Edite o vídeo.
						</span>
					</div>
					<form id="video" class="kt-form kt-margin-t-10" method="POST" action="{{ route('editar.video') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="number" name="id" value="{{ $video->id }}" style="display: none;">
						<div class="form-group mb-4">
							<label for="titulo">TÍTULO</label>
                            <input type="text" class="form-control" placeholder="Dígite o título do vídeo" name="titulo" id="titulo" value="{{ $video->titulo }}">
						</div>
						<div class="form-group mb-4">
							<label for="video">LINK DO VÍDEO</label>
                            <input type="text" class="form-control" placeholder="Dígite o link do vídeo" name="video" id="video" value="{{ $video->video }}">
                        </div>
                        <div class="form-group form-group-last mb-4">
                            <label for="imagem" style="width: 100%; margin: 0;">IMAGEM</label>
                            <span class="kt-widget14__desc">Dimensões recomendadas: largura: 450px / altura: 300px</span>
							<div id="imagem" style="display: flex">
								<input type="file" id="input-file" name="file" capture style="display: none">
                                <input id="file-name" class="form-control mr-4" name="file-name" value="{{ $video->imagem }}" readonly>
								<button type="button" class="btn btn-brand" id="btn-arquivo" style="height: 3rem; width: 15rem;">Selecionar</button>
							</div>
							<div id="imagem-error" class="error invalid-feedback">Este campo é obrigatório.</div>
							@if ($video->imagem)
								<img src="{{ asset('uploads/tv/' . $video->imagem) }}" alt="imagem do video" style="max-width: 15rem;">
							@endif
                        </div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Atualizar Vídeo</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>
        
        let selecionarArquivo = () => {

			// Selecionar Arquivo
			let btn = $("button#btn-arquivo")
			let input = document.getElementById("input-file")

			btn.on("click", () => {
				
				input.click()
			})

			$("#file-name").click(function() {

				input.click()
			})

			input.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(input.files.length > 0) nome = input.files[0].name
				$("input#file-name").val(nome)
			})
			// Selecionar Arquivo
		}

		$(document).ready(function() {

            selecionarArquivo()

			$('form#video').validate({
				rules: {
					titulo: {
						required: true
					},
					video: {
						required: true
					}

				}
			})
		})

		

	</script>
@endsection