@extends(('template.admin'))
@section('title')
    Tv
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Topsapp Tv</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.tv') }}" class="kt-subheader__breadcrumbs-link">Topsapp Tv</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#tv" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-tv"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Topsapp Tv
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Vídeos
						</h3>
						<span class="kt-widget14__desc">
							Gerencie a página Topsapp Tv
						</span>
					</div>
					@if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@elseif(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@endif
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Vídeos Cadastrados <small>Lista de vídeos cadastrados</small>
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<div class="kt-portlet__head-actions">
										<a href="{{ route('cadastrar.video') }}" class="btn btn-brand btn-elevate btn-icon-sm ml-2">
											<i class="fa fa-plus"></i> Cadastrar Video
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>
		
		let data = @JSON($videos)

		if($(window).width() <= 500) {
			$('div.kt-portlet__head-actions a').html('<i class="fa fa-plus"></i>')
		}

		// datable listar videos
		let datatable = $('.kt-datatable').KTDatatable({
			// definição
			data: {
				type: 'local',
				source: data,
				pageSize: 5
			},

			layout: {
				scroll: false,
				footer: false
			},

			pagination: true,

			columns: [
				{
					field: 'id',
					title: '#',
					sortable: 'desc',
					width: 40,
					type: 'number',
					selector: false,
					textAlign: 'center'
				}, {
					field: 'titulo',
					title: 'Título'
				}, {
					field: 'data_cadastro',
					title: 'Data de Cadastro'
				}, {
					field: 'Actions',
					title: 'Ação',
					sortable: false,
					width: 130,
					overflow: 'visible',
					textAlign: 'center',
					template: function(row) {
												return `	<div style="display: flex; margin: 0; padding: 0; justify-content: center">
																		<a href="{{ route('editar.video') }}/` + row.id + `" class="btn btn-hover-brand btn-icon btn-pill" title="Editar">
																			<i class="la la-edit"></i>
																		</a>
																		<form id="deletar-video" action="{{ route('deletar.video') }}/` + row.id + `" method="POST">
																			@csrf
																			@method('DELETE')
																			<button id="kt_sweetalert_delete_video" type="button" class="btn btn-hover-danger btn-icon btn-pill" title="Excluir"><i class="la la-trash"></i></button>
																		</form>
																	</div>`
					}
				}
			]
		})

		$(document).ready(function() {

			// evento deletar video, mostra um alert confirmando o delete
			$('button#kt_sweetalert_delete_video').click(function(e) {
				swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá desfazer essa opção!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Sim, excluir!',
					cancelButtonText: 'Cancelar'
				}).then(function(result) {
					if (result.value) {
						
						$('form#deletar-video').submit()
						
						swal.fire(
							'Excluído!',
							'O vídeo foi exluído.',
							'success'
						)
					}
				})
			})
		})

	</script>

@endsection