@extends(('template.admin'))
@section('title')
    Sobre
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Sobre</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.sobre') }}" class="kt-subheader__breadcrumbs-link">Sobre</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#sobre" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand socicon-draugiem"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Sobre
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Sobre a empresa
						</h3>
						<span class="kt-widget14__desc">
							Edite a página do site. Última atualização: {{ $dados->atualizacao }}
						</span>
					</div>
					@if(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
								<div class="alert-icon"><i class="la la-check-circle"></i></div>
								<div class="alert-text">{{ session('mensagem') }}</div>
								<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
								</div>
						</div>
					@endif
					<form id="sobre" class="kt-form kt-margin-t-10" method="POST" action="{{ route('atualizar.dados.sobre') }}">
						@csrf
						<div class="form-group mb-4">
							<label for="titulo">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título da página" name="titulo" id="titulo" value="{{ $dados->titulo }}">
						</div>
						<div class="form-group mb-4">
							<label for="subtitulo">SUBTÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o subtítulo da página" name="subtitulo" id="subtitulo" value="{{ $dados->subtitulo }}">
						</div>
						<div class="form-group form-group-last mb-3">
							<label for="descricao">TEXTO</label>
							<textarea id="descricao" name="descricao">{{ $dados->descricao }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Atualizar Dados</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
	<script>

		$(document).ready(function() {
			$('#descricao').summernote({
				placeholder: 'Digite aqui seu texto',
				tabsize: 2,
				minHeight: 200 
			})

			$('form#sobre').validate({
				rules: {
					titulo: {
						required: true
					},
					subtitulo: {
						required: true
					},
					descricao: {
						required: true
					}
				}
			})
		})
	</script>
@endsection