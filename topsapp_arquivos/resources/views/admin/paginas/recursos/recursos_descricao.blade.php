@extends(('template.admin'))
@section('title')
    Recursos
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Recursos</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.recurso.descricao') }}" class="kt-subheader__breadcrumbs-link">Descrição</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#recursos" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-text-width"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Descrição
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Recursos - Texto Principal
						</h3>
						<span class="kt-widget14__desc">
							Edite o texto da página recursos. Última Atualização: {{ $info_recurso->atualizacao }}
						</span>
					</div>
					@if(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
								<div class="alert-icon"><i class="la la-check-circle"></i></div>
								<div class="alert-text">{{ session('mensagem') }}</div>
								<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
								</div>
						</div>
					@endif
					<form id="descricao" class="kt-form kt-margin-t-10" method="POST" action="{{ route('atualizar.dados.recurso.descricao') }}">
						@csrf
						<div class="form-group form-group-last mb-3">
							<label for="texto-sobre">TEXTO</label>
							<textarea id="texto" name="texto">{{ $info_recurso->texto }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Atualizar Dados</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
	<script>

		$(document).ready(function() {
			$('#texto').summernote({
				placeholder: 'Digite aqui seu texto',
				tabsize: 2,
				minHeight: 200
			})

			$('form#descricao').validate({
				rules: {
					texto: {
						required: true
					}
				}
			})
		})
	</script>
@endsection