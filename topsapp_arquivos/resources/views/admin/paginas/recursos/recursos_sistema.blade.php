@extends(('template.admin'))
@section('title')
    Recursos
    @parent
@stop
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Recursos</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.recurso.sistema') }}" class="kt-subheader__breadcrumbs-link">Sistema</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#recursos" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-network-wired"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Recursos do Sistema
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body pb-1">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Recursos do Sistema Cadastrados
						</h3>
						<span class="kt-widget14__desc">
							Lista de recursos cadastrados
						</span>
					</div>
					@if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@elseif(session('mensagem'))
						<div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-check-circle"></i></div>
							<div class="alert-text">{{ session('mensagem') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
					@endif
				</div>
				<div class="kt-portlet__body pt-1 pb-1">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Filtro de Recursos Cadastrados <small>Quando um filtro é deletado, os recursos ligados a ele também são deletados</small>
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<div class="kt-portlet__head-actions">
										<button type="button" id="recurso" class="btn btn-brand btn-elevate btn-icon-sm ml-2" data-toggle="modal" data-target="#kt_modal_filtro">
											<i class="fa fa-plus"></i> Novo Filtro
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable-filtro" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
				<div class="kt-portlet__body pt-1">
					<div class="kt-portlet kt-portlet--bordered">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">
									Recursos do Sistema Cadastrados <small>Lista de recursos cadastrado</small>
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-wrapper">
									<div class="kt-portlet__head-actions">
										<a href="{{ route('cadastrar.recurso.sistema') }}" class="btn btn-brand btn-elevate btn-icon-sm ml-2">
											<i class="fa fa-plus"></i> Novo Recurso
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body kt-portlet__body--fit">
							<!--begin: Datatable -->
							<div class="kt-datatable-recurso" id="json_data"></div>
							<!--end: Datatable -->
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="kt_modal_filtro" tabindex="-1" role="dialog" aria-labelledby="modal_titulo" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="modal_titulo">Cadastrar Novo Filtro</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>
						<div class="modal-body">
							<form id="filtro" class="kt-form" method="POST" action="{{ route('cadastrar.filtro.recurso.sistema') }}">
								@csrf
								<div class="form-group">
									<label for="nome" class="form-control-label">Nome do Filtro:</label>
									<input type="text" class="form-control" id="nome" name="nome">
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
							<button type="button" id="modal-enviar" class="btn btn-brand">Cadastrar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')
<script>
	let dataFiltro = @JSON($filtros_recurso)

	let dataRecurso = @JSON($recursos_sistema)

	if($(window).width() <= 500) {
		$('div.kt-portlet__head-actions a').html('<i class="fa fa-plus"></i>')
		$('div.kt-portlet__head-actions button').html('<i class="fa fa-plus"></i>')
	}

	// evento botão cadastrar filtro
	$('button#modal-enviar').click(function() {
		$('form#filtro').submit()
	})

	// datatable de filtros
	let datatableFiltro = $('.kt-datatable-filtro').KTDatatable({
		// definição
		data: {
			type: 'local',
			source: dataFiltro,
			pageSize: 2
		},

		layout: {
			scroll: false,
			footer: false
		},

		// sortable: true,

		pagination: true,

		columns: [
			{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center'
			}, {
				field: 'nome',
				title: 'Nome'
			}, {
				field: 'Actions',
				title: 'Ação',
				sortable: false,
				width: 130,
				overflow: 'visible',
				textAlign: 'center',
				template: function(row) {
											return `	<form id="deletar-filtro" action="{{ route('deletar.filtro.recurso.sistema') }}/` + row.id + `" method="POST">
															@csrf
															@method('DELETE')
															<button id="kt_sweetalert_delete_filtro" type="button" class="btn btn-hover-danger btn-icon btn-pill kt_sweetalert_delete" title="Excluir"><i class="la la-trash"></i></button>
														</form>`
				}
			}
		]
	})

	// datatable recursos
	let datatableRecurso = $('.kt-datatable-recurso').KTDatatable({
		// definição
		data: {
			type: 'local',
			source: dataRecurso,
			pageSize: 4
		},

		layout: {
			scroll: false,
			footer: false
		},

		// sortable: true,

		pagination: true,

		columns: [
			{
				field: 'id',
				title: '#',
				sortable: 'asc',
				width: 40,
				type: 'number',
				selector: false,
				textAlign: 'center'
			}, {
				field: 'titulo',
				title: 'Titulo'
			}, {
				field: 'data_cadastro',
				title: 'Data de cadastro'
			}, {
				field: 'Actions',
				title: 'Ação',
				sortable: false,
				width: 130,
				overflow: 'visible',
				textAlign: 'center',
				template: function(row) {
											return `	<div style="display: flex; margin: 0; padding: 0; justify-content: center">
															<a href="{{ route('editar.recurso.sistema') }}/` + row.id + `" class="btn btn-hover-brand btn-icon btn-pill" title="Editar">
																<i class="la la-edit"></i>
															</a>
															<form id="deletar-recurso" action="{{ route('deletar.recurso.sistema') }}/` + row.id + `" method="POST">
																@csrf
																@method('DELETE')
																<button id="kt_sweetalert_delete_recurso" type="button" class="btn btn-hover-danger btn-icon btn-pill" title="Excluir"><i class="la la-trash"></i></button>
															</form>
														</div>`
				}
			}
		]
	})

	// validação do formulário de filtro
	$('form#filtro').validate({
		rules: {
			nome: {
				required: true
			}
		}
	})

	// visualizaçõa dos datatable
	$('.kt-datatable__table').css('min-height', '0')

	$(document).ready(function(){

		// evento deletar filtro, mostra um alert confirmando o delete
		$('button#kt_sweetalert_delete_filtro').click(function(e){
			swal.fire({
                title: 'Você tem certeza?',
                text: "Se você excluir um filtro, todos os recursos ligados a ele serão deletados!",
                type: 'warning',
                showCancelButton: true,
				confirmButtonText: 'Sim, excluir!',
				cancelButtonText: 'Cancelar'
            }).then(function(result) {
                if (result.value) {

					$('form#deletar-filtro').submit()

                    swal.fire(
                        'Excluído!',
                        'O filtro foi exluído.',
                        'success'
					)
                }
            })
		})

		// evento deletar recursos, mostra um alert confirmando o delete
		$('button#kt_sweetalert_delete_recurso').click(function(e) {
			swal.fire({
                title: 'Você tem certeza?',
                text: "Você não poderá desfazer essa opção!",
                type: 'warning',
                showCancelButton: true,
				confirmButtonText: 'Sim, excluir!',
				cancelButtonText: 'Cancelar'
            }).then(function(result) {
                if (result.value) {
					
					$('form#deletar-recurso').submit()
					
					swal.fire(
                        'Excluído!',
                        'O recurso foi exluído.',
                        'success'
					)
                }
            })
		})
	})

	

</script>
@endsection