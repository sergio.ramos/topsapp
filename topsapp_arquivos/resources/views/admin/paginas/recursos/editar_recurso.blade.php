@extends(('template.admin'))
@section('title')
    Recursos
    @parent
@stop

@section('css_pagina')

	<link rel="stylesheet" href="{{ asset('css/cadastrar_recurso.css') }}">

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Recursos</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.recurso.sistema') }}" class="kt-subheader__breadcrumbs-link">Sistema</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('editar.recurso.sistema', $recurso->id) }}" class="kt-subheader__breadcrumbs-link">Editar Recurso</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#recursos" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-edit"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Editar Recurso
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Recurso do Sistema
						</h3>
						<span class="kt-widget14__desc">
							Edite o recurso.
						</span>
					</div>
					<form id="recurso" class="kt-form kt-margin-t-10" method="POST" action="{{ route('editar.recurso.sistema') }}" enctype="multipart/form-data">
						@csrf
						<input type="number" name="id" value="{{ $recurso->id }}" style="display: none;">
						<div class="form-group mb-4">
							<select id="filtro" class="form-control kt-selectpicker" name="filtro" type="number">
								@foreach ($filtros_recurso as $item)
									<option value="{{ $item->id }}" @if( $recurso->filtro_id === $item->id ) selected @endif>{{ $item->nome }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group mb-4">
							<label for="titulo">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título da página" name="titulo" id="titulo" value="{{ $recurso->titulo }}">
						</div>
						<div class="form-group mb-4">
							<label for="imagem">IMAGEM</label>
							<div id="imagem">
								<input type="file" id="input-file" name="file" capture style="display: none">
								<input id="file-name" class="form-control mr-4" name="file-name" value="{{ $recurso->imagem }}" readonly>
								<button type="button" class="btn btn-brand" id="btn-arquivo">Selecionar</button>
							</div>
							<div id="imagem-error" class="error invalid-feedback">Este campo é obrigatório.</div>
							<img src="{{ asset('uploads/recurso/' . $recurso->imagem) }}" alt="imagem do recurso" style="max-width: 10rem;">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="descricao">TEXTO</label>
							<textarea id="descricao" name="descricao">{{ $recurso->descricao }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Atualizar Recurso</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>
		let selecionarArquivo = () => {

			// Selecionar Arquivo
			let btn = $("button#btn-arquivo")
			let input = document.getElementById("input-file")

			btn.on("click", () => {
				
				input.click()
			})

			$("#file-name").click(function() {

				input.click()
			})

			input.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(input.files.length > 0) nome = input.files[0].name
				$("input#file-name").val(nome)
			})
			// Selecionar Arquivo
		}

		$(document).ready(function() {

			$('#descricao').summernote({
				placeholder: 'Digite aqui seu texto',
				tabsize: 2,
				minHeight: 200 
			})

			selecionarArquivo()

			$('form#recurso').validate({
				rules: {
					filtro: {
						required: true
					},
					titulo: {
						required: true
					},
					descricao: {
						required: true
					}

				}
			})

			$('form#recurso').submit(function() {

				if($('select#filtro').val() === null) {
					$('div#filtro-error').appendTo('#recurso > div:nth-child(2)')
					
				}
			})
		})

		

	</script>
@endsection