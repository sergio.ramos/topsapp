@extends(('template.admin'))
@section('title')
    Notícias
    @parent
@stop

@section('css_pagina')

	<style>
		
		.dz-image img {
			width: 100%;
		}
	
	</style>

@endsection
@section('conteudo')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Notícias</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('admin.home') }}" class="kt-subheader__breadcrumbs-link">Inicio</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('pagina.noticia') }}" class="kt-subheader__breadcrumbs-link">Notícias</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('cadastrar.noticia') }}" class="kt-subheader__breadcrumbs-link">Editar Notícia</a>
			</div>
		</div>
		<div class="kt-subheader__toolbar">
			<a href="http://topsapp.com.br/#noticias" class="btn btn-brand btn-elevate btn-icon-sm" target="_blank">
				<i class="fa fa-search-plus"></i> Visualizar Página
			</a>
		</div>
	</div>
	<!-- end:: Subheader -->

	<!-- begin:: Content -->
	<div class="kt-content">
		<div class="kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-edit"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Editar Notícia
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget14__header kt-margin-b-10">
						<h4 class="kt-widget14__title">
							Notícia
						</h3>
						<span class="kt-widget14__desc">
							Edite a notícia.
						</span>
					</div>
					<form id="noticia" class="kt-form kt-margin-t-10" method="POST" action="{{ route('editar.noticia') }}" enctype="multipart/form-data">
						@csrf
						<input type="number" name="id" value="{{ $noticia->id }}" style="display: none;">
						<div class="form-group mb-4">
							<label for="titulo_noticia">TÍTULO</label>
							<input type="text" class="form-control" placeholder="Dígite o título da notícia" name="titulo_noticia" id="titulo_noticia" value="{{ $noticia->titulo_noticia }}">
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO DE DESTAQUE</label>
							<div style="display: flex;">
								<input type="file" id="foto_destaque" name="foto_destaque" capture style="display: none">
								<input id="file-foto-destaque" name="foto_destaque_name" class="form-control mr-4" @if($noticia->foto_destaque) value="{{$noticia->foto_destaque}}" @else value="Selecione um arquivo" @endif readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-destaque">Selecionar</button>
							</div>
							@if($noticia->foto_destaque)
								<img src="{{ asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_destaque) }}" alt="foto de destaque" style="max-width: 15rem;">
							@endif
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO DE CAPA</label>
							<div style="display: flex;">
								<input type="file" id="foto_capa" name="foto_capa" capture style="display: none">
								<input id="file-foto-capa" name="foto_capa_name" class="form-control mr-4" @if($noticia->foto_capa) value="{{$noticia->foto_capa}}" @else value="Selecione um arquivo" @endif readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-capa">Selecionar</button>
							</div>
							@if($noticia->foto_capa)
								<img src="{{ asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_capa) }}" alt="foto de capa" style="max-width: 15rem;">
							@endif
						</div>
						<div class="form-group mb-4">
							<label for="imagem">FOTO INTERNA</label>
							<div style="display: flex;">
								<input type="file" id="foto_interna" name="foto_interna" capture style="display: none">
								<input id="file-foto-interna" name="foto_interna_name" class="form-control mr-4" @if($noticia->foto_interna) value="{{$noticia->foto_interna}}" @else value="Selecione um arquivo" @endif readonly>
								<button type="button" class="btn btn-brand" id="btn-foto-interna">Selecionar</button>
							</div>
							@if($noticia->foto_interna)
								<img src="{{ asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_interna) }}" alt="foto interna" style="max-width: 15rem;">
							@endif
						</div>
						<div class="form-group mb-4">
							<label for="dropzone">FOTOS</label>
							<div class="kt-dropzone dropzone m-dropzone--success" id="dropzone">
								<div class="kt-dropzone__msg dz-message needsclick">
									<h3 class="kt-dropzone__msg-title">Arraste ou clique para fazer upload</h3>
									<span class="kt-dropzone__msg-desc">Apenas arquivos de imagem é permitido.</span>
								</div>
							</div>
							<input type="text" class="form-control" name="imagens" id="imagens" style="display: none" value="{{$noticia->imagens}}">
						</div>
						<div class="form-group form-group-last mb-4">
							<label for="descricao_noticia">TEXTO</label>
							<textarea id="descricao_noticia" name="descricao_noticia">{{ $noticia->descricao_noticia }}</textarea>
						</div>
						<div class="kt-form__actions">
							<button type="submit" class="btn btn-brand">Editar Notícia</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Content -->
</div>
@stop

@section('scripts_pagina')

	<script>

		$('#descricao_noticia').summernote({
			placeholder: 'Digite aqui seu texto',
			tabsize: 2,
			minHeight: 200 
		})

		$('form#noticia').validate({
			rules: {
				titulo_noticia: {
					required: true
				},
				descricao_noticia: {
					required: true
				}

			}
		})

		let selecionarArquivo = () => {

			// Selecionar Arquivo foto destaque
			let btnFotoDestaque = $("button#btn-foto-destaque")
			let inputFotoDestaque = document.getElementById("foto_destaque")

			btnFotoDestaque.on("click", () => {
				
				inputFotoDestaque.click()
			})

			$("#file-foto-destaque").click(function() {

				inputFotoDestaque.click()
			})

			inputFotoDestaque.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoDestaque.files.length > 0) nome = inputFotoDestaque.files[0].name
				$("input#file-foto-destaque").val(nome)
			})
			// Selecionar Arquivo foto destaque

			// Selecionar Arquivo foto capa
			let btnFotoCapa = $("button#btn-foto-capa")
			let inputFotoCapa = document.getElementById("foto_capa")

			btnFotoCapa.on("click", () => {
				
				inputFotoCapa.click()
			})

			$("#file-foto-capa").click(function() {

				inputFotoCapa.click()
			})

			inputFotoCapa.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoCapa.files.length > 0) nome = inputFotoCapa.files[0].name
				$("input#file-foto-capa").val(nome)
			})
			// Selecionar Arquivo foto capa

			// Selecionar Arquivo foto Interna
			let btnFotoInterna = $("button#btn-foto-interna")
			let inputFotoInterna = document.getElementById("foto_interna")

			btnFotoInterna.on("click", () => {
				
				inputFotoInterna.click()
			})

			$("#file-foto-interna").click(function() {

				inputFotoInterna.click()
			})

			inputFotoInterna.addEventListener("change", () => {

				let nome = "Não há arquivo selecionado!"
				if(inputFotoInterna.files.length > 0) nome = inputFotoInterna.files[0].name
				$("input#file-foto-interna").val(nome)
			})
			// Selecionar Arquivo foto Interna
		}

		function previewThumbailFromUrl(opts) {
							let imgDropzone = Dropzone.forElement("#" + opts.selector)
							let mockFile = {
								name: opts.fileName,
								size: opts.size,
								accepted: true,
								kind: 'image'
							}
							imgDropzone.emit("addedfile", mockFile)
							imgDropzone.files.push(mockFile)
							imgDropzone.emit("thumbnail", mockFile, opts.imageURL)
							imgDropzone.emit("complete", mockFile)

						}

		let idNoticia = '{{$noticia->id}}'
		let linkNoticia = '{{$noticia->link}}'
	
		Dropzone.options.dropzone = {
			url: "upload-imagens/" + idNoticia,
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
            maxFilesize: 10,
            renameFile: function (file) {
                let data = new Date().toISOString().slice(0, 10)
                return data + "-" + file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 50000,
			init: function () {
				thisDropzone = this

				$.get('view-imagens/' + idNoticia, function(data) {
					let path = "{{asset('uploads/noticias/')}}"
		
					$.each(data, function(key,value){

						previewThumbailFromUrl({
							selector: 'dropzone',
							fileName: value.name,
							imageURL: path + "/" + linkNoticia + "/fotos/" + value.name,
							size: value.size
						})
						
					})
				})

				
			},
            removedfile: function (file  ) {
				let name

				if(typeof file.upload === 'undefined') {
					name = file.name
				} else {
					name = file.upload.filename
				}

                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    type: 'POST',
                    url: 'delete-imagens/' + idNoticia,
                    data: {filename: name},
                    success: function (data) {

						let imagens =  $("input#imagens").val()

						$("input#imagens").val(imagens.replace(data + ";", ""))

						// console.log("O arquivo " + data + " foi deletado com sucesso")
                    },
                    error: function (e) {
                        // console.log(e)
                    }
                });
                let fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0
            },

            success: function (file, response) {

				let imagens = $("input#imagens").val()

				$("input#imagens").val(imagens + response.arquivo + ";")
            },
            error: function (file, response) {
                return false
            }
        }

		$(document).ready(function() {

			selecionarArquivo()

		})

		

	</script>
@endsection