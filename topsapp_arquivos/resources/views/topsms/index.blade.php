@extends(('template.topsms'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('topsmsArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('topsmsArquivos/css/style.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            
            <div class="topo">
            
                <div>
                    <img src="{{asset('topsmsArquivos/img/topsms_04.png')}}">

                    <h2>SMS EM GRANDES <span>QUANTIDADES</span></h2>

                    <p>Divulgue, avise, faça suas campanhas com a eficiência de mensagens objetivas.</p>
                </div>
                
            </div>
            
            <div class="preencher"></div>
            
            <div class="container" style="max-width: 100%; padding: 0;">
            
                <a href="{{route('topsms.home')}}" style="width: 100%;">
                    <div class="header">
                        
                        <img src="{{asset('topsmsArquivos/img/logo.PNG')}}">
                        
                    </div>
                </a>
                
                <div class="tec">

                    <h1>CONHEÇA O TOPSMS</h1>

                    <p>
                        Uma plataforma de envio de mensagem via SMS (sigla de Short Message Service, que em português significa Serviço de Mensagens Curtas). Uma ferramenta desenvolvida para quem quer e precisa de facilidade e praticidade. Melhora, de forma eficiente e dinâmica, a comunicação com seus clientes.
                    </p>
                
                </div>
                
                <div class="azul">
                
                    <div class="azul-aln">
                    
                        <div class="azul-titulo">
                            <h1>TECNOLOGIA DINÂMICA</h1>
                            <p>confiabilidade para suas iniciativas</p>
                        </div>

                        <div class="azul-icon">
                            <div>
                                <img src="{{asset('topsmsArquivos/img/topsms_07.png')}}" alt="">
                                <h2>EMPRESA CONTRATANTE</h2>
                            </div>

                            <img src="{{asset('topsmsArquivos/img/topsms_16.png')}}" alt="">
                            
                            <div>
                                <img src="{{asset('topsmsArquivos/img/topsms_09.png')}}" alt="">
                                <h2>TOPSMS</h2>
                            </div>

                            <img src="{{asset('topsmsArquivos/img/topsms_16.png')}}" alt="">

                            <div>
                                <img src="{{asset('topsmsArquivos/img/topsms_11.png')}}" alt="">
                                <h2>OPERADORAS</h2>
                            </div>

                            <img src="{{asset('topsmsArquivos/img/topsms_16.png')}}" alt="">

                            <div>
                                <img src="{{asset('topsmsArquivos/img/topsms_13.png')}}" alt="">
                                <h2>USUÁRIO FINAL</h2>
                            </div>
                        </div>
                    </div>
                
                </div>
                
                <div class="funcionalidades">

                    <div class="conteudo">
                    
                        <div class="funcionalidades-icon">
                            
                            <img src="{{asset('topsmsArquivos/img/topsms_34.png')}}">

                        </div>

                        <div class="funcionalidades-texto">
                            <h1><span>SMS</span> PERSONALIZADO</h1>

                            <P>Adicione o nome do destinatário e outras informações pessoais a cada SMS: seu nome ou nome da empresa, data de nascimento, código de desconto e outros.</P>
                        </div>
                    
                    </div>
                    
                    <div class="solicitar">
                        
                        <div class="banner">
                                <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                        </div>
                    </div>
                </div>
                
                <div class="bordas-div">
            
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">

                        <a href="tel:(66) 3211-0010">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topsmsArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>(66) 3211-0010</span>
                            </div>
                        </a>
                        
                        <a href="mailto:comercial@topsapp.com.br">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topsmsArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>comercial@topsapp.com.br</span>
                            </div>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                            <div>
                                <img src="{{asset('topsmsArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                            <div>
                                <img src="{{asset('topsmsArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>Instagram</span>
                        </a>
                        
                        <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                            <div>
                                <img src="{{asset('topsmsArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                    
                    </div>
                
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
                
                
            
            </div>

        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script type="text/javascript">

        if($(window).width() >= 1024) {
            $('div.tec p.tec-title').hide()
            $('div.tec div.tec-aln').hide()
    
            $(window).scroll(function() {

                if( $(window).scrollTop() < 50 ) {
                    $('div.tec p.tec-title').hide()
                    $('div.tec div.tec-aln').hide()
                } else {
                    $('div.tec p.tec-title').show()
                    $('div.tec div.tec-aln').show()
                }

                if ( $(window).scrollTop() >= 650 ) {
                    $('#particles-js').css('display', 'flex');
                } else if($(window).scrollTop() >= 177 && janelaWidth < 576) {
                    $('#particles-js').css('display', 'flex');
                } else {
                    $('#particles-js').attr('style', '');

                }
            })
        }
            
        $(window).scroll(function() {

            if ( $(window).scrollTop() >= 640 ) {
                $('.header').css('display', 'flex')
            } else {
                $('.header').attr('style', '')
            }
        })

    </script>
    
@endsection