@extends(('template.top_wifi'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('top_wifiArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('top_wifiArquivos/css/style.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            
            <div class="topo">
        
                <img src="{{asset('top_wifiArquivos/img/novo_topvoz_03.png')}}">

                <h1>PORTAL PARA GESTÃO DE HOTSPOTS</h1>

                <p>O TOPWIFI cria e gerencia hotsposts transformando sua rede Wi-Fi em uma ferramenta de marketing com segurança e comodidade</p>
            
            </div>
            
            <div class="conteudo" style="max-width: 100%; padding: 0;">
                
                <a href="{{route('topwifi.home')}}" style="width: 100%;" id="home">
                    <div id="particles-js">
                
                        <img src="{{asset('top_wifiArquivos/img/novo_topvoz_03.png')}}">
                
                    </div>
                </a>

                <div class="azul">
                   
                   <p>O TOPWIFI é uma poderosa ferramenta de HotSpot com possibilidade de coleta de cadastros e campanhas de marketing.</p>
               
                </div>

                <div class="ponta"></div>
                
                <div class="funciona">
                    
                    <p>COMO FUNCIONA</p>
                
                    <div class="funciona-img">
                    
                    </div>
                    
                    <div class="funciona-txts">
                        <div class="container">
                            <div class="row">
                                <div class="funciona-txt col-12 col-lg-4">
                                
                                    <p>CADASTRO RÁPIDO E SEGURO</p>
                                    <span>O cliente se conecta à sua rede Wi-Fi sem senha e faz um cadastro usando informações pessoais por meio de um portal de captura. Você pode utilizar campanhas publicitárias para serem exibidas durante o preenchimento dos dados.</span>
                                
                                </div>
                                
                                <div class="funciona-txt col-12 col-lg-4">
                                
                                    <p>VALIDAÇÃO DO CADASTRO</p>
                                    <span>Para conceder o acesso à internet, o TOPWIFI registra e valida as informações fornecidas. Se o visitante já possui cadastro, o dispositivo é reconhecido automaticamente pelo sistema, facilitando a conexão.</span>
                                
                                </div>
                                
                                <div class="funciona-txt col-12 col-lg-4">
                                
                                    <p>ACESSO À REDE LIBERADO</p>
                                    <span>Pronto! Conexão liberada. Seu cliente está livre para navegar na internet. Agora você terá um registro com as informações do cadastro, disponíveis para relacionamento e marketing com o cliente.</span>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                    
                    <div class="torta">
                    
                        <div class="torta-aln">
                        
                            <p>LAYOUT PERSONALIZÁVEL</p>
                            <img>
                            <h2>Impressione seus clientes com um layout com sua identidade visual</h2>
                        
                        </div>
                    
                    </div>
                
                <div class="beneficios">
                    
                    <div class="beneficios-aln">
                
                        <p>BENEFÍCIOS TOP WIFI</p>
                        
                        <div class="beneficios-itens">
                            <div class="container">
                                <div class="row">
                                    <div class="beneficios-item col-12 col-lg-3">
                                        <div class="img">
                                            <img src="{{asset('top_wifiArquivos/img/novo_topvoz_18.png')}}">
                                        </div>
                                        <p>MARKETING</p>
                                        <span>Ofereça login social (Check-in e Curtir), exiba banners promocionais. Desenvolva campanhas próprias ou patrocinadas, anuncie parceiros.</span>
                                    
                                    </div>
                                    
                                    <div class="beneficios-item col-12 col-lg-3">
                                        <div class="img">
                                            <img src="{{asset('top_wifiArquivos/img/novo_topvoz_20.png')}}">
                                        </div>
                                        <p>CONTROLE DA REDE</p>
                                        <span>Tenha o controle da sua rede Wi-Fi, gerencie o tempo de conectividade, limite o tráfego de acesso e bloqueie usuários indesejados.</span>
                                    
                                    </div>
                                    
                                    <div class="beneficios-item col-12 col-lg-3">
                                        <div class="img">
                                            <img src="{{asset('top_wifiArquivos/img/novo_topvoz_22.png')}}">
                                        </div>
                                        <p>SEGURANÇA</p>
                                        <span>Ofereça uma conexão segura para você e seus visitantes. Atenda ao <a href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2014/lei/l12965.htm" target="_blank">Marco Civil</a> da internet e proteja seu negócio.</span>
                                    
                                    </div>
                                    
                                    <div class="beneficios-item col-12 col-lg-3">
                                        <div class="img">
                                            <img src="{{asset('top_wifiArquivos/img/novo_topvoz_15.png')}}">
                                        </div>
                                        <p>BAIXO CUSTO</p>
                                        <span>Solução de baixo custo para diversos tipos e portes de empresas. Compatível com diversos equipamentos de rede.</span>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="solicitar">
                            <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                        </div>
                    </div>
                
                </div>

                <div class="bordas-div">
            
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">
                        
                        <a href="tel:(66) 3211-0010">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('top_wifiArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>(66) 3211-0010</span>
                            </div>
                        </a>
                        
                        <a href="mailto:comercial@topsapp.com.br">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('top_wifiArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>comercial@topsapp.com.br</span>
                            </div>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                            <div>
                                <img src="{{asset('top_wifiArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                            <div>
                                <img src="{{asset('top_wifiArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>Instagram</span>
                        </a>
                        
                        <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                            <div>
                                <img src="{{asset('top_wifiArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                    
                    </div>
                
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
               
            </div>

        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script type="text/javascript">

        let desktop = 2
        // executar no inicio
		if(janelaWidth >= 1024) {
            configDesktop()
		}

		if(janelaWidth < 1024) {
            configMobile()
		}

        $(window).on('resize', function() {
			janelaWidth = $(this).width()

			if(janelaWidth >= 1024) {
                configDesktop()
            }

			if(janelaWidth < 1024) {
                configMobile()
			}
		})

        function configMobile() {
            if(desktop != 0) {
                $('div.funciona-img img').remove()
                $('div.funciona-txt:nth-child(1)').prepend('<img src="{{asset('top_wifiArquivos/img/novo_topvoz_07_01.png')}}">')
                $('div.funciona-txt:nth-child(2)').prepend('<img src="{{asset('top_wifiArquivos/img/novo_topvoz_07_03.png')}}">')
                $('div.funciona-txt:nth-child(3)').prepend('<img src="{{asset('top_wifiArquivos/img/novo_topvoz_07_05.png')}}">')

                $('div.torta-aln img').attr('src', '{{asset('top_wifiArquivos/img/novo_topvoz_12.png')}}')

                desktop = 0
            }
        }

        function configDesktop() {
            if(desktop != 1) {
                $('div.funciona-img').prepend('<img src="{{asset('top_wifiArquivos/img/novo_topvoz_07.png')}}">')
			    $('div.funciona-txt:nth-child(1) img').remove()
                $('div.funciona-txt:nth-child(2) img').remove()
                $('div.funciona-txt:nth-child(3) img').remove()

                $('div.torta-aln img').attr('src', '{{asset('top_wifiArquivos/img/novo_topvoz_11.png')}}')

                desktop = 1
            }
        }
            
        $(window).scroll(function() {
            if($(window).width() >= 1024) {
                if ( $(window).scrollTop() >= 700 ) {
                    $('#particles-js').css('display', 'flex');
                } else {
                    $('#particles-js').attr('style', '');
                }
            }

            if($(window).width() >= 576 && $(window).width() < 1024) {
                if ( $(window).scrollTop() >= 385 ) {
                    $('#particles-js').css('display', 'flex');
                } else {
                    $('#particles-js').attr('style', '');
                }
            }

            if($(window).width() < 576) {
                $('#particles-js').css('display', 'none');
            }
        })

        if($(window).width() >= 1024) {
            $('div.azul').slideUp(800)
            $('div.ponta').slideUp(800)
            $('div.azul p').fadeOut()
            $('div.funciona').css('margin-top', '10rem')
    
            $(window).scroll(function() {

                if( $(window).scrollTop() < 50 ) {
                    $('div.azul').slideUp(800)
                    $('div.ponta').slideUp(800)
                    $('div.azul p').fadeOut()
                    $('div.funciona').css('margin-top', '10rem')
                } else {
                    $('div.azul').slideDown(800)
                    $('div.ponta').slideDown(800)
                    $('div.azul p').fadeIn()
                    $('div.funciona').css('margin-top', '0')
                }
            })
        }

    </script>
    
@endsection