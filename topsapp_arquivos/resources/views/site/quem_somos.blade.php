@extends(('template.site'))
@section('title')
    Quem Somos -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/quem_somos.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
			<section class="topo_sobre" style="background: url({{asset('img/rct_28.jpg')}})">
				<div class="container">
                    <div class="row">
						<div class="texto col-12 col-lg-6">
							<h1>{{$sobre->titulo}}</h1>
							<p>
								{{$sobre->subtitulo}}
							</p>
						</div>

						<div class="img col-12 col-lg-6">
							<img src="{{asset('img/rct_29.png')}}" alt="logo do topsapp">
						</div>
					</div>
				</div>
			</section>

			<section class="cont_sobre">
				<div class="container">
                    <div class="row">
						{!! $sobre->descricao !!}
					</div>
				</div>
			</section>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
		if(janelaWidth >= 1024) {
			width = "11.8rem"
			position = "0"
		} else {
			width = "10.8rem"
			position = "0"
		}

		// menu style
		$('div.menu-line').css("width", width)
		$('div.menu-line').css("left", position)

		$('section.cont_sobre div').removeAttr('style')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
			janelaWidth = $(this).width()

			if(janelaWidth >= 1024) {
				width = "11.8rem"
				position = "0"
			} else {
				width = "10.8rem"
				position = "0"
			}

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
		// fim configurações do menu

    </script>

@endsection