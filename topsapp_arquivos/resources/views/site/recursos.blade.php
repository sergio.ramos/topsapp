@extends(('template.site'))
@section('title')
    Recursos -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/recursos.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    <section class="topo_recursos col-12">
                        <h2>RECURSOS DO SISTEMA</h2>
                        <div class="texto">
                            {!! $info_recurso->texto !!}
                        </div>
                    </section>

                    <section class="cont_recuros col-12">
                        <div id="recurso">
                            <ul>
                            </ul>
                        </div>
                        {{-- buttons next e previous --}}
                        <div class="d-flex justify-content-center mb-4">
                            <button class="btn btn-brand previous mr-2"><<</button>
                            <button class="btn btn-brand next ml-2">>></button>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
        if(janelaWidth >= 1024) {
            width = "8.6rem"
            position = "11.8rem"
		} else {
            width = "7.8rem"
            position = "10.8rem"
        }

        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(2) a').css('color', '#1b6faa')

		$('section.cont_sobre div').removeAttr('style')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
        }

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "8.6rem"
                position = "11.8rem"
            } else {
                width = "7.8rem"
                position = "10.8rem"
            }

			if(janelaWidth >= 1024) {
				configMenuDesktop(width, position)
            }
            
            if(janelaWidth >= 576 && janelaWidth < 1024) {
                configMenuDesktop(width, position)
            }

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu

        let recursos = @JSON($recursos)

        let index = 0, qtd = 8
        let listRecursos = ""
        listRecuros = exibirRecursos(index, qtd)
        $('div#recurso ul').hide()
        $('div#recurso ul').append(listRecursos)
        $('div#recurso ul').fadeIn()

        $('button.next').click(function() {
            if(qtd < recursos.length) {
                index = qtd
                qtd += 8

                listRecursos = ''
                listRecursos = exibirRecursos(index, qtd)
                $('div#recurso ul li').remove()
                $('div#recurso ul').hide()
                $('div#recurso ul').append(listRecursos)
                $('div#recurso ul').fadeIn()
            }
        })

        $('button.previous').click(function() {
            if(qtd > 8) {
                index = 0
                qtd = 8

                listRecursos = ''
                listRecursos = exibirRecursos(index, qtd)
                $('div#recurso ul li').remove()
                $('div#recurso ul').hide()
                $('div#recurso ul').append(listRecursos)
                $('div#recurso ul').fadeIn()
            }
        })

        function exibirRecursos(index, qtd) {

            for (index; index < qtd; index++) {

                listRecursos += `
                                            <li class="item_recurso">
                                                <div class="head">
                                                    <img src="uploads/recurso/${recursos[index].imagem}" alt="${recursos[index].titulo}">
                                                </div>
                                                <div class="body">
                                                    <h5>${recursos[index].titulo}</h5>
                                                    ${recursos[index].descricao}
                                                </div>
                                            </li>
                `
            }

            return listRecursos
        }
        
    </script>

@endsection