@extends(('template.site'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/home.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content">
            {{-- INICIO SLIDES --}}
            <div class="slides">
                <div id="carousel-slide" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-slide" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-slide" data-slide-to="1"></li>
                        <li data-target="#carousel-slide" data-slide-to="2"></li>
                        <li data-target="#carousel-slide" data-slide-to="3"></li>
                        <li data-target="#carousel-slide" data-slide-to="4"></li>
                        <li data-target="#carousel-slide" data-slide-to="5"></li>
                        <li data-target="#carousel-slide" data-slide-to="6"></li>
                        <li data-target="#carousel-slide" data-slide-to="7"></li>
                        <li data-target="#carousel-slide" data-slide-to="8"></li>
                    </ol>
                    
                    <div class="carousel-inner">
                        
                        <div class="carousel-item active">
                            <img src="{{asset('uploads/slide/' . $slides[0]->imagem)}}" class="d-block w-100" alt="{{ $slides[0]->titulo }}" href="">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{--{{ $slides[0]->titulo }}--}}</h5>
                                {{-- <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/topvoz" target="_blank"><img src="{{asset('uploads/slide/' . $slides[1]->imagem)}}" class="d-block w-100" alt="{{ $slides[1]->titulo }}"></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[1]->titulo }}</h5>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/topmaps" target="_blank"><img src="{{asset('uploads/slide/' . $slides[2]->imagem)}}" class="d-block w-100" alt="{{ $slides[2]->titulo }}" href=""></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[2]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/topapp" target="_blank"><img src="{{asset('uploads/slide/' . $slides[3]->imagem)}}" class="d-block w-100" alt="{{ $slides[3]->titulo }}" href=""></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[3]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/modulocobranca" target="_blank"><img src="{{asset('uploads/slide/' . $slides[4]->imagem)}}" class="d-block w-100" alt="{{ $slides[4]->titulo }}"></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[4]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="http://" target="_blank"><img src="{{asset('uploads/slide/' . $slides[5]->imagem)}}" class="d-block w-100" alt="{{ $slides[5]->titulo }}"></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[5]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/topwifi" target="_blank"><img src="{{asset('uploads/slide/' . $slides[6]->imagem)}}" class="d-block w-100" alt="{{ $slides[6]->titulo }}" href=""></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[6]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.topsapp.com.br/topsms" target="_blank"><img src="{{asset('uploads/slide/' . $slides[7]->imagem)}}" class="d-block w-100" alt="{{ $slides[7]->titulo }}" href=""></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[7]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="carousel-item">
                           <a href="http://" target="_blank"><img src="{{asset('uploads/slide/' . $slides[8]->imagem)}}" class="d-block w-100" alt="{{ $slides[8]->titulo }}"></a>
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $slides[8]->titulo }}</h5>
                                {{-- <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> --}}
                            </div>
                        </div>
                        <div class="container alinhamento">
                            <ul class="lista_segmentos">
                                <li style="display: none;">{{--{{$slides[0]->titulo}}--}}</li>
                                <a href="http://topsapp/topvoz" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="1">{{$slides[1]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/topmaps" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="2">{{$slides[2]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/topapp" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="3">{{$slides[3]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/modulocobranca" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="4">{{$slides[4]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/moduloweb" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="5">{{$slides[5]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/topwifi" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="6">{{$slides[6]->titulo}}</li></a>
                                <a href="https://topsapp.com.br/topsms" style="color: inherit" target="_blank"><li data-target="#carousel-slide" data-slide-to="7">{{$slides[7]->titulo}}</li></a>
                                <li data-target="#carousel-slide" data-slide-to="8">{{$slides[8]->titulo}}</li>
                            </ul>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-slide" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-slide" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            {{-- FIM SLIDES --}}

            {{-- INICIO CONHEÇA O SISTEMA --}}
            <div class="kt-portlet kt-portlet--mobile conheca_sistema">
                <div class="container ">
                    <div class="row">
                        <div class="conheca_logo col-12 col-md-12 col-lg-5">
                            <img src="{{asset('img/rct_12.png')}}" alt="logo do topsapp">
                        </div>
                        <div class="accordion col-12 col-md-12 col-lg-7 p-0" id="accordion">
                            <div class="card">
                                <div class="card-header" id="conheca">
                                    <div class="card-title">
                                        {{--<i class="flaticon-pie-chart-1"></i>--}} CONHEÇA O SISTEMA TOPSAPP
                                    </div>
                                </div>
                                <div id="conheca_texto" class="collapse" aria-labelledby="conheca" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        O Sistema TopSapp é o melhor e mais completo Sistema para gerenciamento de provedores de internet. Acompanhando o desenvolvimento do mercado de tecnologia e pensando sempre em atender melhor os seus clientes, a equipe TopSapp desenvolve constantemente novos recursos para promover facilidade e rapidez no desenvolvimento das atividades dos usuários do sistema.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- FIM CONHECÇA O SISTEMA --}}

            {{-- INICIO RECURSOS DO SISTEMA --}}
            <div class="kt-portlet kt-portlet--mobile recursos_sistema">
                <div class="container">
                    <div class="kt-portlet__head row">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                NOSSOS RECURSOS
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body row">
                        <div class="home_recursos">
                            <div class="recursos row pr-3 pl-3">
                                <div class="item-recurso col-12 col-md-6 col-lg-3">
                                    <img src="{{asset('uploads/recurso/' . $recursos[0]->imagem)}}" alt="{{$recursos[0]->titulo}}">
                                    <h5>{{$recursos[0]->titulo}}</h5>
                                    <p>{{$recursos[0]->descricao}}</p>
                                </div>

                                <div class="item-recurso col-12 col-md-6 col-lg-3">
                                    <img src="{{asset('uploads/recurso/' . $recursos[1]->imagem)}}" alt="{{$recursos[1]->titulo}}">
                                    <h5>{{$recursos[1]->titulo}}</h5>
                                    <p>{{$recursos[1]->descricao}}</p>
                                </div>

                                <div class="item-recurso col-12 col-md-6 col-lg-3">
                                    <img src="{{asset('uploads/recurso/' . $recursos[2]->imagem)}}" alt="{{$recursos[2]->titulo}}">
                                    <h5>{{$recursos[2]->titulo}}</h5>
                                    <p>{{$recursos[2]->descricao}}</p>
                                </div>

                                <div class="item-recurso col-12 col-md-6 col-lg-3">
                                    <img src="{{asset('uploads/recurso/' . $recursos[3]->imagem)}}" alt="{{$recursos[3]->titulo}}">
                                    <h5>{{$recursos[3]->titulo}}</h5>
                                    <p>{{$recursos[3]->descricao}}</p>
                                </div>

                                <div class="col-12 mt-3">
                                    <button class="btn btn-brand btn-elevate btn-icon-sm btn-top" onclick="location.href = '{{route('site.recursos')}}'">VER OUTROS</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- FIM RECURSOS DO SISTEMA --}}

            {{-- INICIO SOLUÇÕES DO SISTEMA --}}
            <div class="kt-portlet kt-portlet--mobile solucoes_sistema">
                <div class="container">
                    <div class="kt-portlet__head row">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                A CADA NECESSIDADE UMA SOLUÇÃO
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body row">
                        <div class="home_solucoes">
                            <div class="solucoes row pl-3 pr-3 justify-content-center">
                                <div class="item-solucao col-12 col-md-8 col-lg-4">
                                    <h4>COMUNICAÇÃO</h4>
                                    <img src="{{asset('img/rct_23.png')}}" alt="icone comunicação">
                                    <p>
                                        Ganhe em agilidade com o novo atendimento online e avisos financeiros
                                    </p>
                                </div>

                                <div class="item-solucao col-12 col-md-8 col-lg-4">
                                    <h4>FACILIDADE</h4>
                                    <img src="{{asset('img/rct_21.png')}}" alt="icone facilidade">
                                    <p>
                                        Interface intuitiva e um completo gerenciamento de seus clientes.
                                    </p>
                                </div>

                                <div class="item-solucao col-12 col-md-8 col-lg-4">
                                    <h4>SEGURANÇA</h4>
                                    <img src="{{asset('img/rct_22.png')}}" alt="icone comunicação">
                                    <p>
                                        Tenha tranquilidade em seu dia a dia com o TopSapp, sabendo que está em boas mãos com o que há de melhor em segurança virtual.
                                    </p>
                                </div>

                                <div class="col-12 mt-3">
                                    <button class="btn btn-brand btn-elevate btn-icon-sm btn-top" onclick="location.href = '{{route('site.contato')}}'">SOLICITE UMA PROPOSTA</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- FIM SOLUÇÕES DO SISTEMA --}}
                    
            {{-- INICIO NOTICIAS E TV TOPSAPP --}}
            <div class="kt-portlet kt-portlet--mobile noticias_e_tv mb-0 p-3">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-7 p-0">
                            <div class="kt-portlet__head justify-content-between">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        NOTÍCIAS
                                    </h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <a href="{{route('site.noticias')}}">VER TODAS</a>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-widget3">
                                    @for($i = $noticias->count(), $e = $i-2; $i > $e; $i--)
                                        <div class="kt-widget3__item">
                                            <div class="kt-widget3__header">
                                                <div class="kt-widget3__user-img">							 
                                                    {{-- <img class="kt-widget3__img" src="./assets/media/users/user1.jpg" alt="">   --}}
                                                </div>
                                                <div class="kt-widget3__info">
                                                    <a href="{{route('site.abrir.noticia', ['link' => $noticias[$i-1]->link, 'id' => $noticias[$i-1]->id])}}" class="kt-widget3__username">
                                                        {{$noticias[$i-1]->titulo_noticia}}
                                                    </a>
                                                    <br> 
                                                    <span class="kt-widget3__time">
                                                        {{$noticias[$i-1]->data_cadastro}}
                                                    </span>		 
                                                </div>
                                                <span class="kt-widget3__status kt-font-info">
                                                {{-- Pending --}}
                                                </span>	
                                            </div>
                                            <div class="kt-widget3__body">
                                                <a href="{{route('site.abrir.noticia', ['link' => $noticias[$i-1]->link, 'id' => $noticias[$i-1]->id])}}" class="text-link">
                                                    <p class="kt-widget3__text"> 
                                                        {{$noticias[$i-1]->descricao_noticia}}
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5 p-0">
                            <div class="kt-portlet__head justify-content-between">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        TOPSAPP TV
                                    </h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <a href="{{route('site.tv')}}">VER TODOS</a>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="home_tv" link='<iframe class="video" src="{{$video->video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'>
                                    <img class="img_video" onclick="linkvideo = $('.home_tv').attr('link');$('.home_tv').html(linkvideo)" src="{{asset('uploads/tv/' . $video->imagem)}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- FIM NOTICIAS E TV TOPSAPP --}}
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>
    
    <script>
        
        let slides = @JSON($slides)

        let noticias = @JSON($noticias)

		width = "0"
        position = "0"
        
        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
            configMenuMobile()
            configHomeMobile()
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
			janelaWidth = $(this).width()

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
            }

			if(janelaWidth < 576) {
                configMenuMobile()
                configHomeMobile()
			}
		})
		// executar quando for redimensionado
		// fim configurações do menu

        // MENU SEGMENTOS TRASIÇÕES DOS SLIDES
        $('#carousel-slide').on('slide.bs.carousel', function (slide) {
            let nextPosition = slide.to + 1
            let backPosition = slide.from + 1

            $('ul.lista_segmentos li:nth-child(' + backPosition +')').removeClass('ativado')
            $('ul.lista_segmentos li:nth-child(' + nextPosition +')').addClass('ativado')
        })
        // MENU SEGMENTOS TRASIÇÕES DOS SLIDES

        // expand conheça o topsapp
        $('div#conheca_texto').collapse()

        function configHomeMobile() {

            let altura = 225

            $('div#conheca').click(function(){
                if(altura === 0) {
                    altura = 225
                } else {
                    altura = 0
                }
            })

            // EVENTO DE SCROLL NO MOBILE
            $(document).scroll(function() {
                // inicio evento scroll para visualização dos recursos
                if($(this).scrollTop() >= (0 + altura) && $(this).scrollTop() < (259.99 + altura) ) {
                    $('div.home_recursos div.item-recurso:nth-child(1)').addClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) img').addClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) h5').addClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) p').addClass('p-hover')
                } else {
                    $('div.home_recursos div.item-recurso:nth-child(1)').removeClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) img').removeClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) h5').removeClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(1) p').removeClass('p-hover')
                }

                if($(this).scrollTop() > (260 + altura) && $(this).scrollTop() < (519.99 + altura) ) {
                    $('div.home_recursos div.item-recurso:nth-child(2)').addClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) img').addClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) h5').addClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) p').addClass('p-hover')
                } else {
                    $('div.home_recursos div.item-recurso:nth-child(2)').removeClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) img').removeClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) h5').removeClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(2) p').removeClass('p-hover')
                }

                if($(this).scrollTop() > (520 + altura) && $(this).scrollTop() < (779.99 + altura) ) {
                    $('div.home_recursos div.item-recurso:nth-child(3)').addClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) img').addClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) h5').addClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) p').addClass('p-hover')
                } else {
                    $('div.home_recursos div.item-recurso:nth-child(3)').removeClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) img').removeClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) h5').removeClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(3) p').removeClass('p-hover')
                }

                if($(this).scrollTop() > (780 + altura) && $(this).scrollTop() < (1059.99 + altura) ) {
                    $('div.home_recursos div.item-recurso:nth-child(4)').addClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) img').addClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) h5').addClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) p').addClass('p-hover')
                } else {
                    $('div.home_recursos div.item-recurso:nth-child(4)').removeClass('recurso-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) img').removeClass('img-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) h5').removeClass('h5-hover')
                    $('div.home_recursos div.item-recurso:nth-child(4) p').removeClass('p-hover')
                }
                // fim evento scroll para visualização dos recursos

                // inicio evento scroll para visualização das soluções
                if($(this).scrollTop() > (1130 + altura) && $(this).scrollTop() < (1439.99 + altura) ) {
                    $('div.home_solucoes div.item-solucao:nth-child(1)').addClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) img').addClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) h4').addClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) p').addClass('p-hover')
                } else {
                    $('div.home_solucoes div.item-solucao:nth-child(1)').removeClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) img').removeClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) h4').removeClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(1) p').removeClass('p-hover')
                }

                if($(this).scrollTop() > (1440 + altura) && $(this).scrollTop() < (1729.99 + altura) ) {
                    $('div.home_solucoes div.item-solucao:nth-child(2)').addClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) img').addClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) h4').addClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) p').addClass('p-hover')
                } else {
                    $('div.home_solucoes div.item-solucao:nth-child(2)').removeClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) img').removeClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) h4').removeClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(2) p').removeClass('p-hover')
                }

                if($(this).scrollTop() > (1730 + altura) && $(this).scrollTop() < (2199.99 + altura) ) {
                    $('div.home_solucoes div.item-solucao:nth-child(3)').addClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) img').addClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) h4').addClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) p').addClass('p-hover')
                } else {
                    $('div.home_solucoes div.item-solucao:nth-child(3)').removeClass('solucao-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) img').removeClass('img-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) h4').removeClass('h4-hover')
                    $('div.home_solucoes div.item-solucao:nth-child(3) p').removeClass('p-hover')
                }
                // fim evento scroll para visualização das soluções
            })
        }

        

        function configHoverCarousel() {

            // transição dos slides efeito hover
            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(2) > li').hover(function() {
                $('#carousel-slide').carousel(1)
            }).click(function() {
                window.open('https://topsapp.com.br/topvoz', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(3) > li').hover(function() {
                $('#carousel-slide').carousel(2)
            }).click(function() {
                window.open('https://topsapp.com.br/topmaps', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(4) > li').hover(function() {
                $('#carousel-slide').carousel(3)
            }).click(function() {
                window.open('https://topsapp.com.br/topapp', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(5) > li').mouseover(function() {
                $('#carousel-slide').carousel(4)
            }).click(function() {
                window.open('https://topsapp.com.br/modulocobranca', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(6) > li').mouseover(function() {
                $('#carousel-slide').carousel(5)
            }).click(function() {
                window.open('https://topsapp.com.br/topvoz', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(7) > li').mouseover(function() {
                $('#carousel-slide').carousel(6)
            }).click(function() {
                window.open('https://topsapp.com.br/topwifi', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > a:nth-child(8) > li').mouseover(function() {
                $('#carousel-slide').carousel(7)
            }).click(function() {
                window.open('https://topsapp.com.br/topsms', '_blank')
            })

            $('#carousel-slide > div > div.container.alinhamento > ul > li:nth-child(9)').mouseover(function() {
                $('#carousel-slide').carousel(8)
            })
            // transição dos slides efeito hover
        }

        configHoverCarousel()

    </script>

@endsection