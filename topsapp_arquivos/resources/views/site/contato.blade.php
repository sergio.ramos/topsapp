@extends(('template.site'))
@section('title')
    Contato -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/contato.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    <section class="contato col-12 pb-4">
                        <h2>CONTATO</h2>
                        <div class="texto">
                            <p>Por e-mail: <a href="mailto:{{$dados_site->email}}" target="_top">{{$dados_site->email}}</a></p>
                            <br>
                            <p>Por telefone: <a href="tel:{{$dados_site->telefones}}">{{$dados_site->telefones}}</a></p>
                            <p>Whatsapp comercial: <a href="tel:(66) 9 9638-4467">(66) 9 9638-4467</a></p>
                            <br>
                            <p>
                                Plantão Topsapp: <a href="tel:(66) 9 9638-4467">(66) 9 9685-2854</a> (horário de Brasília)
                                <br>
                                Segunda à Sexta: 12:30hr às 14:30hr / 18:30hr às 23:00hr
                                <br>
                                Sábado: 12:30hr às 23:00hr / Domingo: 08:00hr às 23:00hr
                            </p>
                            <br>
                            <p>{{$dados_site->endereco}}</p>
                        </div>
                    </section>

                    <section class="contato_form col-12">
                        <p>Preencha os campos abaixo corretamente.</p>
                        <div class="form_border"></div>
                        @if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
                        @elseif(session('mensagem'))
                        <div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1 col-12" role="alert">
                            <div class="alert-icon"><i class="la la-check-circle"></i></div>
                            <div class="alert-text">{{ session('mensagem') }}</div>
                            <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                            </div>
                        </div>
                        @endif
                        <form id="contato" class="kt-form kt-margin-t-10" method="POST" action="{{route('site.contato.email')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="nome">NOME</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu nome..." name="nome" id="nome">
                                </div>
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="email">E-MAIL</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu email..." name="email" id="email">
                                </div>
                                <div class="col-lg-6 p-0">
                                    <div class="row m-0">
                                    <div class="form-group mb-4 col-lg-12">
                                        <label for="site">SITE</label>
                                        <input type="text" class="form-control contato_form_input" placeholder="" name="site" id="site">
                                    </div>
                                    <div class="form-group form-group-last mb-4 col-lg-6">
                                        <label for="telefone">TELEFONE DE CONTATO</label>
                                        <input type="text" class="form-control contato_form_input" placeholder="Dígite seu telefone..." name="telefone" id="telefone">
                                    </div>
                                    <div class="form-group mb-4 col-lg-6">
                                        <label for="departamento">DEPARTAMENTO</label>
                                        <select name="departamento" class="kt-selectpicker" id="departamento">
                                            <option value="" disabled selected style="display: none">Selecione</option>
                                            <option value="administrativo">Administrativo</option>
                                            <option value="comercial">Comercial</option>xx
                                            <option value="financeiro">Financeiro</option>
                                            <option value="suporte">Suporte</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-6">
                                    <label for="descricao">SOBRE O QUE DESEJA CONVERSAR?</label>
                                    <textarea id="descricao" class="form-control contato_form_input" name="descricao"></textarea>
                                </div>
                                <div class="validation col-12 d-flex justify-content-start">
                                    <div class="g-recaptcha" data-sitekey="6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr"></div>
                                </div>
                                <div class="kt-form__actions text-center mb-4 col-12">
                                    <button type="submit" class="btn btn-brand btn-top">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
        if(janelaWidth >= 1024) {
            width = "8.2rem"
            position = "38.45rem"
		} else {
            width = "7.2rem"
            position = "34.8rem"
        }

        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(5) a').css('color', '#1b6faa')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "8.2rem"
                position = "38.45rem"
            } else {
                width = "7.2rem"
                position = "34.8rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu

        // configurações do formulario
        $('form#contato').validate({
			rules: {
				nome: {
					required: true
				},
				email: {
					required: true
				},
				site: {
					required: true
				},
                telefone: {
                    required: true
                },
                departamento: {
                    required: true
                },
                descricao: {
                    required: true
                }
			}
		})

        $('input#telefone').inputmask("(99) 9 9999-9999")
        $("input#email").inputmask({mask:"*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(t,a){return(t=t.toLowerCase()).replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}})

    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

@endsection