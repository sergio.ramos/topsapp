@extends(('template.site'))
@section('title')
    Noticias -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/listarNoticias.css')}}">
@endsection

@php
    $indice = count($noticias) - 1
@endphp

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    <div class="superior col-12">
                        <div class="row">
                            <div class="imagem_grande col-12 col-md-12 col-lg-8">
                                <a href="{{route('site.abrir.noticia', ['link' => $noticias[$indice]->link, 'id' => $noticias[$indice]->id])}}">
                                    <div class="hoverzoom">
                                        <img src="{{asset('uploads/noticias/' . $noticias[$indice]->link . '/' . $noticias[$indice]->foto_destaque)}}" alt="foto de destaque" class="img-fluid">
                                        <div class="mask"></div>
                                        <div class="retina"></div>
                                    </div>
                                    <div class="borda_noticia_imagem">
                                        <div class="titulo_noticias_grande">
                                            <div>{{$noticias[$indice]->data_cadastro}}</div>
                                            <span>{{$noticias[$indice]->titulo_noticia}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="imagens_lateral col-12 col-md-12 col-lg-4">
                                <li>
                                    <div class="imagem_laranja">
                                        <a href="{{route('site.abrir.noticia', ['link' => $noticias[$indice - 1]->link, 'id' => $noticias[$indice - 1]->id])}}">
                                            <div class="hoverzoom">
                                                <img src="{{asset('uploads/noticias/' . $noticias[$indice - 1]->link . '/' . $noticias[$indice - 1]->foto_destaque)}}" alt="foto de destaque" class="img-fluid">
                                                <div class="mask"></div>
                                                <div class="retina"></div>
                                            </div>
                                            <div class="borda_lateral_imagem">
                                                <h6>{{$noticias[$indice - 1]->data_cadastro}}</h6>
                                                <p>{{$noticias[$indice - 1]->titulo_noticia}}</p>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <div class="imagem_laranja">
                                        <a href="{{route('site.abrir.noticia', ['link' => $noticias[$indice - 2]->link, 'id' => $noticias[$indice - 2]->id])}}">
                                            <div class="hoverzoom">
                                                <img src="{{asset('uploads/noticias/' . $noticias[$indice - 2]->link . '/' . $noticias[$indice - 2]->foto_destaque)}}" alt="foto de destaque" class="img-fluid">
                                                <div class="mask"></div>
                                                <div class="retina"></div>
                                            </div>
                                            <div class="borda_lateral_imagem">
                                                <h6>{{$noticias[$indice - 2]->data_cadastro}}</h6>
                                                <p>{{$noticias[$indice - 2]->titulo_noticia}}</p>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="inferior col-12">
                        <div class="row">
                            {{-- buttons next e previous --}}
                            <div class="d-flex justify-content-center mb-4 col-12">
                                <button class="btn btn-brand previous mr-2"><<</button>
                                <button class="btn btn-brand next ml-2">>></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

        if(janelaWidth >= 1024) {
            width = "7.95rem"
            position = "52.9rem"
		} else {
            width = "6.8rem"
            position = "46.7rem"
        }

        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(7) a').css('color', '#1b6faa')

		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}

		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "7.95rem"
                position = "52.9rem"
            } else {
                width = "6.8rem"
                position = "46.7rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})

        let noticias = @JSON($noticias)

        let index = noticias.length - 4
        let posFinal = noticias.length - 7
        let listaNoticias = ""
        listaNoticias = listarNoticia(index, posFinal)
        $('div.inferior div.row').hide()
        $('div.inferior div.row').prepend(listaNoticias)
        $('div.inferior div.row').fadeIn()

        $('button.next').click(function() {
            if( posFinal != 0 ) {
                index = posFinal - 1
                posFinal -= 4

                if(posFinal < 0) {
                    posFinal = 0
                }

                listaNoticias = ''
                listaNoticias = listarNoticia(index, posFinal)
                $('div.inferior div.row div.noticia_link').remove()
                $('div.inferior div.row').hide()
                $('div.inferior div.row').prepend(listaNoticias)
                $('div.inferior div.row').fadeIn()
            }
        })

        $('button.previous').click(function() {
            if(index < (noticias.length - 4)) {
                
                if(posFinal == 0) {
                    posFinal = index + 1
                } else {
                    posFinal += 4
                }

                index += 4

                listaNoticias = ''
                listaNoticias = listarNoticia(index, posFinal)
                $('div.inferior div.row div.noticia_link').remove()
                $('div.inferior div.row').hide()
                $('div.inferior div.row').prepend(listaNoticias)
                $('div.inferior div.row').fadeIn()
            }
        })


        function listarNoticia(index, posFinal) {

            for (index; index >= posFinal; index--) {

                listaNoticias += `   <div class="noticia_link col-12 col-md-12 col-lg-3">
                                        <a href="noticias/${noticias[index].link}/${noticias[index].id}">
                                            <div class="img_inferior">
                                                <img src="uploads/noticias/${noticias[index].link}/${noticias[index].foto_destaque}" alt="${noticias[index].titulo_noticia}" alt="foto de destaque" class="img-fluid">
                                            </div>
                                            <div class="data">${noticias[index].data_cadastro}</div>
                                            <div class="texto">${noticias[index].titulo_noticia}</div>
                                        </a>
                                    </div>`
            }

            return listaNoticias
        }
        
    </script>

@endsection