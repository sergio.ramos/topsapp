@extends(('template.site'))
@section('title')
    Demo -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/demo.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    <section class="demo col-12 pb-4">
                        <h2>VERSÃO DEMONSTRATIVA DO TOPSAPP</h2>
                        <div class="texto">
                            <p>
                                Gostaria de receber uma versão DEMO para testar e conhecer o programa?
                                É fácil, basta preencher o formulário abaixo corretamente com os seus dados e receberá o arquivo para download do nosso sistema para os devidos testes.
                            </p>
                        </div>
                    </section>

                    <section class="demo_form col-12">
                        <p>Preencha os campos abaixo corretamente.</p>
                        <div class="form_border"></div>
                        @if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
                        @elseif(session('mensagem'))
                        <div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1 col-12" role="alert">
                            <div class="alert-icon"><i class="la la-check-circle"></i></div>
                            <div class="alert-text" style="text-align: center">{{ session('mensagem') }}, <a href="http://topsapp.com.br/Executaveis/InstallTopSappDemonstrativoDL3.zip" style="color: #fff; text-decoration: underline;">clique aqui</a> para o inicio do download. <div>Usuário: dtopsapp</div><div>Senha: dtop12345</div></div>
                            <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                            </div>
                        </div>
                        @endif
                        <form id="demo" class="kt-form kt-margin-t-10" method="POST" action="{{route('site.demo.email')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="nome">NOME</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu nome..." name="nome" id="nome">
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-3">
                                    <label for="telefone_comercial">TELEFONE COMERCIAL</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu telefone..." name="telefone_comercial" id="telefone_comercial">
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-3">
                                    <label for="telefone_celular">TELEFONE CELULAR</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu telefone..." name="telefone_celular" id="telefone_celular">
                                </div>
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="email">E-MAIL</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu email..." name="email" id="email">
                                </div>
                                <div class="form-group mb-4 col-lg-3">
                                    <label for="estados_brasil">ESTADO</label>
                                    <select name="estados_brasil" class="kt-selectpicker" id="estados_brasil">
                                        <option value="" disabled selected style="display: none">Selecione um Estado</option>
                                        <option value="Acre">Acre</option>
                                        <option value="Alagoas">Alagoas</option>
                                        <option value="Amapá">Amapá</option>
                                        <option value="Amazonas">Amazonas</option>
                                        <option value="Bahia">Bahia</option>
                                        <option value="Ceará">Ceará</option>
                                        <option value="Distrito Federal">Distrito Federal</option>
                                        <option value="Espírito Santo">Espírito Santo</option>
                                        <option value="Goiás">Goiás</option>
                                        <option value="Maranhão">Maranhão</option>
                                        <option value="Mato Grosso">Mato Grosso</option>
                                        <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option value="Minas Gerais">Minas Gerais</option>
                                        <option value="Pará">Pará</option>
                                        <option value="Paraíba">Paraíba</option>
                                        <option value="Paraná">Paraná</option>
                                        <option value="Pernambuco">Pernambuco</option>
                                        <option value="Piauí">Piauí</option>
                                        <option value="Rio de Janeiro">Rio de Janeiro</option>
                                        <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option value="Rondônia">Rondônia</option>
                                        <option value="Roraima">Roraima</option>
                                        <option value="Santa Catarina">Santa Catarina</option>
                                        <option value="São Paulo">São Paulo</option>
                                        <option value="Sergipe">Sergipe</option>
                                        <option value="Tocantins">Tocantins</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-3">
                                    <label for="cidade">CIDADE</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite sua cidade..." name="cidade" id="cidade">
                                </div>
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="endereco">ENDEREÇO</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="" name="endereco" id="endereco">
                                </div>
                                <div class="form-group mb-4 col-lg-3">
                                    <label for="numero">NÚMERO</label>
                                    <input type="number" class="form-control contato_form_input" placeholder="" name="numero" id="numero">
                                </div>
                                <div class="form-group mb-4 col-lg-3">
                                    <label for="cep">CEP</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="" name="cep" id="cep">
                                </div>
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="site">SEU SITE</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="" name="site" id="site">
                                </div>
                                <div class="form-group mb-4 col-lg-3">
                                    <label for="responsavel">RESPONSÁVEL</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="" name="responsavel" id="responsavel">
                                </div>
                                <div class="form-group mb-4 col-lg-3">
                                    <label for="conheceu">COMO CONHECEU O TOPSAPP?</label>
                                    <select name="conheceu" class="kt-selectpicker" id="conheceu">
                                        <option value="" disabled selected style="display: none">Selecione</option>
                                        <option value="Evento Local">Evento Local</option>
                                        <option value="Feira">Feira</option>
                                        <option value="Indicação">Indicação</option>
                                        <option value="Internet">Internet</option>
                                        <option value="Jornal">Jornal</option>
                                        <option value="Mala Direta">Mala Direta</option>
                                        <option value="Revista">Revista</option>
                                        <option value="Seminário">Seminário</option>
                                        <option value="Redes Sociais">Redes Sociais</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-6">
                                    <label for="descricao">DESCREVA BREVEMENTE ALGUMA OBSERVAÇÃO QUE VOCÊ ACHA INTERESSANTE PARA NOSSOS ANALISTAS?</label>
                                    <textarea id="descricao" class="form-control contato_form_input" name="descricao"></textarea>
                                </div>
                                <div class="validation col-12 col-lg-6">
                                    <div class="g-recaptcha" data-sitekey="6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr"></div>
                                </div>
                                <div class="kt-form__actions text-center mb-4 col-12">
                                    <button type="submit" class="btn btn-brand btn-top">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
        if(janelaWidth >= 1024) {
            width = "6.2rem"
            position = "46.65rem"
		} else {
            width = "4.8rem"
            position = "41.9rem"
        }

        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(6) a').css('color', '#1b6faa')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "6.2rem"
                position = "46.65rem"
            } else {
                width = "4.8rem"
                position = "41.9rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu

        // configurações do formulario
        $('form#demo').validate({
			rules: {
				nome: {
					required: true
				},
                telefone_comercial: {
                    required: false
                },
                telefone_celular: {
                    required: true
                },
				email: {
					required: true
				},
				estados_brasil: {
					required: true
				},
                cidade: {
                    required: true
                },
                endereco: {
                    required: true
                },
                numero: {
                    required: true
                },
                cep: {
                    required: true
                },
                site: {
                    required: true
                },
                responsavel: {
                    required: true
                },
                conheceu: {
                    required: true
                },
                descricao: {
                    required: true
                }
			}
		})

        $('input#telefone_comercial').inputmask("(99) 9999-9999")
        $('input#telefone_celular').inputmask("(99) 9 9999-9999")
        $("input#email").inputmask({mask:"*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(t,a){return(t=t.toLowerCase()).replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}})
        $('input#cep').inputmask("99999-999")

    </script>

@endsection