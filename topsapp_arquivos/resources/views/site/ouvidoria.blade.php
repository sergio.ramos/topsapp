@extends(('template.site'))
@section('title')
    Ouvidoria -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/ouvidoria.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    <section class="ouvidoria col-12 pb-4">
                        <h2>{{$ouvidoria->titulo}}</h2>
                        <div class="texto">
                            {!! $ouvidoria->descricao !!}
                        </div>
                    </section>

                    <section class="ouvidoria_form col-12">
                        <p>Preencha os campos abaixo corretamente.</p>
                        <div class="form_border"></div>
                        @if(session('invalido'))
						<div class="alert alert-danger fade show mb-0 pl-3 pr-3 pt-1 pb-1" role="alert">
							<div class="alert-icon"><i class="la la-times-circle"></i></div>
							<div class="alert-text">{{ session('invalido') }}</div>
							<div class="alert-close">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
								</button>
							</div>
						</div>
                        @elseif(session('mensagem'))
                        <div class="alert alert-success fade show mb-0 pl-3 pr-3 pt-1 pb-1 col-12" role="alert">
                            <div class="alert-icon"><i class="la la-check-circle"></i></div>
                            <div class="alert-text">{{ session('mensagem') }}</div>
                            <div class="alert-close">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                            </div>
                        </div>
                        @endif
                        <form id="ouvidoria" class="kt-form kt-margin-t-10" method="POST" action="{{route('site.ouvidoria.email')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="nome">NOME</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu nome..." name="nome" id="nome">
                                </div>
                                <div class="form-group mb-4 col-lg-6">
                                    <label for="email">E-MAIL</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu email..." name="email" id="email">
                                </div>
                                <div class="form-group mb-4 col-md-6 col-lg-3">
                                    <label for="estados_brasil">ESTADO</label>
                                    <select name="estados_brasil" class="kt-selectpicker" id="estados_brasil">
                                        <option value="" disabled selected style="display: none">Selecione um Estado</option>
                                        <option value="Acre">Acre</option>
                                        <option value="Alagoas">Alagoas</option>
                                        <option value="Amapá">Amapá</option>
                                        <option value="Amazonas">Amazonas</option>
                                        <option value="Bahia">Bahia</option>
                                        <option value="Ceará">Ceará</option>
                                        <option value="Distrito Federal">Distrito Federal</option>
                                        <option value="Espírito Santo">Espírito Santo</option>
                                        <option value="Goiás">Goiás</option>
                                        <option value="Maranhão">Maranhão</option>
                                        <option value="Mato Grosso">Mato Grosso</option>
                                        <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                        <option value="Minas Gerais">Minas Gerais</option>
                                        <option value="Pará">Pará</option>
                                        <option value="Paraíba">Paraíba</option>
                                        <option value="Paraná">Paraná</option>
                                        <option value="Pernambuco">Pernambuco</option>
                                        <option value="Piauí">Piauí</option>
                                        <option value="Rio de Janeiro">Rio de Janeiro</option>
                                        <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                                        <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                        <option value="Rondônia">Rondônia</option>
                                        <option value="Roraima">Roraima</option>
                                        <option value="Santa Catarina">Santa Catarina</option>
                                        <option value="São Paulo">São Paulo</option>
                                        <option value="Sergipe">Sergipe</option>
                                        <option value="Tocantins">Tocantins</option>
                                    </select>
                                </div>
                                <div class="form-group form-group-last mb-4 col-md-6 col-lg-3">
                                    <label for="cidade">CIDADE</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite sua cidade..." name="cidade" id="cidade">
                                </div>
                                <div class="form-group form-group-last mb-4 col-md-6 col-lg-3">
                                    <label for="cep">CEP</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu cep..." name="cep" id="cep">
                                </div>
                                <div class="form-group form-group-last mb-4 col-md-6 col-lg-3">
                                    <label for="telefone">TELEFONE</label>
                                    <input type="text" class="form-control contato_form_input" placeholder="Dígite seu telefone..." name="telefone" id="telefone">
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="row p-0 m-0">
                                        <div class="pl-0 mb-4 col-md-6 col-lg-6">
                                            <label for="prioridade">PRIORIDADE</label>
                                            <select name="prioridade" class="kt-selectpicker" id="prioridade">
                                                <option value="" disabled selected style="display: none">Prioridade</option>
                                                <option value="urgente">Urgente</option>
                                                <option value="normal">Normal</option>
                                            </select>
                                        </div>
                                        <div class="pr-0 col-md-6 col-lg-6">
                                            <label for="tipo_pessoa">TIPO DE PESSOA</label>
                                            <select name="tipo_pessoa" class="kt-selectpicker" id="tipo_pessoa">
                                                <option value="" disabled selected style="display: none">Tipo Pessoa</option>
                                                <option value="fisica">Física</option>
                                                <option value="juridica">Jurídica</option>
                                            </select>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="6LeMm7EUAAAAAALcwY1OA9PDvER41KWbj9Uoisqr"></div>
                                    </div>
                                </div>
                                <div class="form-group form-group-last mb-4 col-lg-6">
                                    <label for="descricao">TEXTO</label>
                                    <textarea id="descricao" class="form-control contato_form_input" name="descricao"></textarea>
                                </div>
                                <div class="validation col-12 d-flex justify-content-start">

                                </div>
                                <div class="kt-form__actions text-center mb-4 col-12">
                                    <button type="submit" class="btn btn-brand btn-top">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
        if(janelaWidth >= 1024) {
            width = "8.95rem"
            position = "29.57rem"
		} else {
            width = "7.8rem"
            position = "27rem"
        }

        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(4) a').css('color', '#1b6faa')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
        }
        
        if(janelaWidth < 1024) {
            $("div.g-recaptcha").appendTo("div.validation")
        }

        if(janelaWidth >= 1024) {
            $("div.g-recaptcha").appendTo("div.form-group:nth-child(7)")
        }
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "8.95rem"
                position = "29.57rem"
            } else {
                width = "7.8rem"
                position = "27rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
            }
            
            if(janelaWidth < 1024) {
                $("div.g-recaptcha").appendTo("div.validation")
            }

            if(janelaWidth >= 1024) {
                $("div.g-recaptcha").appendTo("div.form-group:nth-child(7)")
            }
		})
		// executar quando for redimensionado
        // fim configurações do menu

        // configurações do formulario
        $('form#ouvidoria').validate({
			rules: {
				nome: {
					required: true
				},
				email: {
					required: true
				},
				estados_brasil: {
					required: true
				},
                cidade: {
                    required: true
                },
                cep: {
                    required: true
                },
                telefone: {
                    required: true
                },
                prioridade: {
                    required: true
                },
                tipo_pessoa: {
                    required: true
                },
                descricao: {
                    required: true
                }
			}
		})

        $('input#cep').inputmask("99999-999")
        $('input#telefone').inputmask("(99) 9 9999-9999")
        $("input#email").inputmask({mask:"*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(t,a){return(t=t.toLowerCase()).replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}})

    </script>

@endsection