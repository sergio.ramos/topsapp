@extends(('template.site'))
@section('title')
    Topsapp TV -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/tv.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <section class="topo_tv" style="background: url({{asset('img/rct_28.jpg')}})">
                <img src="{{asset('img/rct_33.png')}}" alt="">
            </section>

            <section class="cont_tv">
                <div class="container">
                    <div class="row">
                        <div class="tv col-12">
                            <ul>

                            </ul>
                        </div>

                        <div class="col-12 mt-3 text-center">
                            <button class="btn btn-brand btn-elevate btn-icon-sm btn-top">VER MAIS</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu 
        if(janelaWidth >= 1024) {
            width = "9.25rem"
            position = "20.3rem"
		} else {
            width = "8.3rem"
            position = "18.7rem"
        }

        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(3) a').css('color', '#1b6faa')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "9.25rem"
                position = "20.3rem"
            } else {
                width = "8.3rem"
                position = "18.7rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu

        // inicio configurações para exibição de vídeos
        let videos = @JSON($videos)

        let listVideos = ""
        let quantidade = 4

        exibirVideos(quantidade)
        $('div.tv ul').hide()
        $('div.tv ul').append(listVideos)
        $('div.tv ul').fadeIn()

        $('button.btn-top').click(function() {
            listVideos = ""
            quantidade += 4
            let cont  = (videos.length - quantidade) <= 0 ? 0 : videos.length - quantidade
            if(cont > 0) {

                exibirVideos(cont)
                $('div.tv ul li').remove()
                $('div.tv ul').hide()
                $('div.tv ul').append(listVideos)
                $('div.tv ul').fadeIn()
            } else {

                exibirVideos(cont)
                $('div.tv ul li').remove()
                $('div.tv ul').hide()
                $('div.tv ul').append(listVideos)
                $('div.tv ul').fadeIn()
                $('button.btn-top').remove()
            }
        })

        function exibirVideos(quantidade) {

            let index = 0
            if(quantidade == 4) {
                index = (videos.length - quantidade)
            } else {
                index = quantidade
            }

            for (let cont = (videos.length -1); index <= cont; cont--) {
                console.log(cont)
                // inserir videos
                listVideos += `
                                <li class="item_video">
                                    <div class="head">
                                        <h5>${videos[cont].titulo}</h5>
                                    </div>
                                    <div class="body">
                                        <div class="home_tv${cont}" link='<iframe class="video" src="${videos[cont].video}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'>
                                            <img class="img_video" onclick="linkvideo = $('.home_tv${cont}').attr('link');$('.home_tv${cont}').html(linkvideo)" src="uploads/tv/${videos[cont].imagem}">
                                        </div>
                                    </div>
                                </li>
                `
                // // inserir videos
            }
        }
        // fim configurações para exibição de vídeos

    </script>

@endsection