@extends(('template.site'))
@section('title')
    Notícia -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/magnific_popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/site/noticias.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
            <div class="container">
                <div class="row">
                    {{-- INICIO NOTICIA --}}
                    <div class="kt-portlet kt-portlet--mobile solucoes_sistema">
                        <div class="container">
                            <div class="kt-portlet__head row">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        {{$noticia->titulo_noticia}}
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-12">
                                        <span class="noticia_data_ler">{{$noticia->data_cadastro}}</span>

                                        @isset($noticia->foto_interna)
                                            <div class="noticia-banner">
                        
                                                <div class="banner-noticia">
                                                        {{-- uploads/noticias/' . $noticias[$indice]->link . '/' . $noticias[$indice]->foto_destaque --}}
                                                    {{-- <div class="nt-img" style="background:url('{{asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_interna)}}') center center;background-size:cover;"></div> --}}
                                                    <a href="{{asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_interna)}}" class="image-link">
                                                        <img class="nt-img" src="{{asset('uploads/noticias/' . $noticia->link . '/' . $noticia->foto_interna)}}" alt="">
                                                    </a>
                                                    
                                                </div>
                                                
                                            </div>
                                        @endisset

                                        {!! $noticia->descricao_noticia !!}

                                        @isset($noticia->imagens)
                                            <div class="galeria">
                                                @foreach ($noticia->imagens as $item)
                                                    <a class="" href="{{asset('uploads/noticias/' . $noticia->link . '/fotos' . '/' . $item)}}">
                                                        <img src="{{asset('uploads/noticias/' . $noticia->link . '/fotos' . '/' . $item)}}" alt="">
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endisset
                                    </div>
                                    <div class="col-12 mt-3">
                                        <button class="btn btn-brand btn-elevate btn-icon-sm btn-top" onclick="location.href = '{{route('site.noticias')}}'">VOLTAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- FIM NOTICIA --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script src="{{asset('js/magnific_popup.js')}}"></script>

    <script>

        // MAGINIFIC POPUP

        $(document).ready(function() {
            $('.image-link').magnificPopup({type:'image'})
            $('div.galeria').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery:{enabled:true}
                // other options
            });
        })

		// inicio configurações do menu 
        if(janelaWidth >= 1024) {
            width = "7.95rem"
            position = "52.9rem"
		} else {
            width = "6.8rem"
            position = "46.7rem"
        }

        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
        $('li.nav-item:nth-child(7) a').css('color', '#1b6faa')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
		}

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "7.95rem"
                position = "52.9rem"
            } else {
                width = "6.8rem"
                position = "46.7rem"
            }

			if(janelaWidth >= 576) {
				configMenuDesktop(width, position)
			}

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu
        
    </script>

@endsection