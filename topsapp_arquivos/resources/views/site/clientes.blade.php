@extends(('template.site'))
@section('title')
    Clientes -
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('css/site/clientes.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0 m-0 ">
			<section class="topo_clientes" style="background: url({{asset('img/rct_28.jpg')}})">
				<div class="container">
                    <div class="row">
						<div class="col-12">
                            <h1>CLIENTES TOPSAPP</h1>
                            <button id="brasil" class="selecionado">EM TODO O BRASIL</button>
                            <button id="paises">EM OUTROS PAÍSES</button>
						</div>
					</div>
				</div>
			</section>

			<section class="cont_clientes">
				<div class="container">
                    <div class="row">
                        <img class="brasil" src="{{asset('img/mapa_brasil.png')}}" alt="Mapa topsapp no Brasil" style="transition: all 0.4s ease-in-out; opacity: 1;">
                        <img class="paises" src="{{asset('img/mapa_paises.png')}}" alt="Mapa Países" style="transition: all 0.4s ease-in-out; opacity: 0;">
					</div>
				</div>
			</section>
        </div>
    </div>
@endsection

@section('scripts_pagina')
    <script src="{{asset('js/menu_site.js')}}"></script>

    <script>

		// inicio configurações do menu
		if(janelaWidth >= 1024) {
			width = "11.8rem"
		} else {
			width = "10.8rem"
        }
        
		position = "0"

        // menu style
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)

		$('section.cont_sobre div').removeAttr('style')

        // executar no inicio
		if(janelaWidth >= 576) {
			configMenuDesktop(width, position)
        }

		if(janelaWidth < 576) {
			configMenuMobile(width, position)
		}
        // executar no inicio
        
        // executar quando for redimensionado
		$(window).on('resize', function() {
            janelaWidth = $(this).width()
            
            if(janelaWidth >= 1024) {
                width = "11.8rem"
            } else {
                width = "10.8rem"
            }

			if(janelaWidth >= 1024) {
				configMenuDesktop(width, position)
            }
            
            if(janelaWidth >= 576 && janelaWidth < 1024) {
                configMenuDesktop(width, position)
            }

			if(janelaWidth < 576) {
				configMenuMobile()
			}
		})
		// executar quando for redimensionado
        // fim configurações do menu
        
        // botão alterar mapa
        $('button#brasil').click(function() {
            $('button#brasil').addClass('selecionado')
            $('button#paises').removeClass('selecionado')

            $('img.brasil').attr('style', 'opacity: 1')
            $('img.paises').attr('style', 'opacity: 0')
        })

        $('button#paises').click(function() {
            $('button#paises').addClass('selecionado')
            $('button#brasil').removeClass('selecionado')

            $('img.paises').attr('style', 'opacity: 1')
            $('img.brasil').attr('style', 'opacity: 0')
        })

    </script>

@endsection