<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
	{{-- <div class="kt-container  kt-container--fluid"> --}}
    <div class="container">
        <div class="kt-footer__menu row pb-2">
            {{-- d-flex flex-column align-items-center --}}
            <div class="contato col-12 col-md-5 col-lg-5 p-3">
                <h5>CONTATO</h5>
                <a href="tel:{{$dados_site->telefones}}"><span class="kt-font-boldest">{{$dados_site->telefones}}</span></a>
                <a href="mailto:{{$dados_site->email}}" target="_top">{{$dados_site->email}}</a>
            </div>
            {{-- d-flex flex-column align-items-center --}}
            <div class="menu_footer col-12 col-md-4 col-lg-4 p-3">
                <h5>O TOPSAPP</h5>
                <div class="flex-row col-10 col-md-12 p-0">
                    <ul class="col-6 col-lg-6">
                        <li><a href="{{route('site.home')}}" class="kt-footer__menu-link kt-link">Home</a></li>
                        <li><a href="{{route('site.ouvidoria')}}" class="kt-footer__menu-link kt-link">Ouvidoria</a></li>
                        <li><a href="{{route('site.contato')}}" class="kt-footer__menu-link kt-link">Contato</a></li>
                    </ul>
                    <ul class="col-6 col-lg-6">
                        <li><a href="{{route('site.recursos')}}" class="kt-footer__menu-link kt-link">Recursos</a></li>
                        <li><a href="{{route('site.tv')}}" class="kt-footer__menu-link kt-link">Topsapp Tv</a></li>
                        <li><a href="{{route('site.demo')}}" class="kt-footer__menu-link kt-link">Demo</a></li>
                    </ul>
                </div>
            </div>
            {{-- d-flex flex-column align-items-center --}}
            <div class="redes_sociais_footer col-12 col-md-3 col-lg-3 p-3">
                <h5>REDES SOCIAIS</h5>
                <ul class="d-flex">
                    <li class=""><a href="{{$dados_site->facebook}}" target="_blank"><i class="fab fa-facebook"></i></a></li>
                    <li class="ml-3 mr-3"><a href="{{$dados_site->twitter}}" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
                    <li class=""><a href="{{$dados_site->youtube}}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="kt-footer__copyright row justify-content-center pt-3">
            2019&nbsp;©&nbsp;<a href="http://multiwaretecnologia.com.br/" target="_blank" class="kt-link">Multiware Tecnologia</a>
        </div>
    </div>
</div>	