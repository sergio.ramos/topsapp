<!DOCTYPE html>
<html lang="pt-br">
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			@section('title')
			- Topsapp - Tip
			@show			
		</title>
		<meta name="description" content="Tip topsapp">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Fonts -->

		<style>

			#kt_header_mobile > div.kt-header-mobile__logo > a > img {
				width: 10rem;
			}

			@media (min-width: 768px) and (max-width: 1023.98px) {
				#kt_header_mobile > div.kt-header-mobile__logo > a > img {
					width: 11rem;
				}
			}

			@media (min-width: 1024px) {
				#kt_aside_brand {
					padding-left: 4rem;
				}

				#kt_aside_brand > div.kt-aside__brand-logo > a > img {
					width: 8.5rem;
				}
			}

		</style>

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->		
		<link href="{{asset('assets/default/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/brand/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/aside/light.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Layout Skins -->

		<!--begin::Page Custom Styles(used by this page) -->
		@yield('css_pagina')
		<!--end::Page Custom Styles -->

		<link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.png')}}"/>
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-footer--fixed kt-page--loading">

		@auth
			<!-- begin::Page loader -->
			<div class="kt-page-loader kt-page-loader--logo">
				<img alt="Logo" src="{{asset('assets/media/logos/tip_carregando.svg')}}"/>
				<div class="kt-spinner kt-spinner--brand"></div>
			</div>
			<!-- end::Page Loader -->  

			<!-- begin:: Page -->
			<!-- begin:: Header Mobile -->
			<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
				<div class="kt-header-mobile__logo">
					<a href="{{ route('admin.home') }}">
						<img alt="Logo" src="{{asset('assets/media/logos/sistema_tip.svg')}}" />
					</a>
				</div>
				<div class="kt-header-mobile__toolbar">
					<button class="kt-header-mobile__toggler" id="kt_aside_mobile_toggler"><span></span></button>
					{{-- <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button> --}}
					<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
				</div>
			</div>
			<!-- end:: Header Mobile -->
		@endauth
		
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				@auth
					<!-- begin:: Aside -->
					@include('template.menu')
					<!-- end:: Aside -->
				@endauth

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
					@auth
						<!-- begin:: Header -->
						@include('template.header_shortcut')
						<!-- end:: Header -->
					@endauth

					<!-- BEGIN: Conteudo Página -->
					@yield('conteudo')
					<!-- END: Conteudo Página -->
					
					@auth
						<!-- begin:: Footer -->
						@include('template.footer')
						<!-- end:: Footer -->
					@endauth
				</div>
			</div>
		</div>
		<!-- end:: Page -->

		@auth
		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->
		@endauth

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		@yield('scripts_pagina_vendors')
		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script>

			if($(window).width() <= 500) {
				$('div.kt-subheader__toolbar a').html('<i class="fa fa-search-plus"></i>')
				$('h3.kt-subheader__title').html('')
			}

			// Class definition
			var KTBootstrapSelectModal = function () {
				
				// Private functions
				var funcBootstrapSelectModal = function () {
					// minimum setup
					$('.kt-selectpicker').selectpicker()
				}

				return {
					// public functions
					init: function() {
						funcBootstrapSelectModal()
					}
				}
			}()
		
				
			// Initialization
			jQuery(document).ready(function() {
				KTBootstrapSelectModal.init()
			})
		</script>
		@yield('scripts_pagina')
		<!--end::Page Scripts -->

		<!--begin::Global App Bundle(used by all pages) -->
		<script src="{{asset('assets/app/bundle/app.bundle.js')}}" type="text/javascript"></script>
		@yield('scripts_pagina_app_bundle')
		<!--end::Global App Bundle -->

		<script>

			$(document).ready(function() {
				$('.kt-header-mobile--fixed').css('z-index', '600')
				$('.kt-header__topbar').css('z-index', '599')
			})

		</script>
	</body>
	<!-- end::Body -->
</html>