<div style="background:#29aae2">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-sm navbar-dark --white col-12">
                <!-- logo -->
                <a href="{{route($pageAtual['rota'])}}" class="navbar-brand">{{$pageAtual['nome']}}</a>

                <!-- Menu Hamburguer -->
                <button class="navbar-toggler collapsed" data-toggle="collapse" data-target="#nav-target" aria-expanded="true">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- navegação  d-flex flex-column align-items-start -->
                <div class="navbar-collapse collapse col-12 col-lg-9" id="nav-target" style="">
                    <div class="menu-line"></div>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a href="" class="nav-link dropdown-toggle" data-toggle="dropdown">INSTITUCIONAL</a>

                            <div class="dropdown-menu">
                                <a href="{{route('site.quemSomos')}}" class="dropdown-item">Quem somos</a>
                                <a href="{{route('site.clientes')}}" class="dropdown-item">Clientes</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.recursos')}}" class="nav-link">RECURSOS</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.tv')}}" class="nav-link">TOPSAPP TV</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.ouvidoria')}}" class="nav-link">OUVIDORIA</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.contato')}}" class="nav-link">CONTATO</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.demo')}}" class="nav-link">DEMO</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('site.noticias')}}" class="nav-link">NOTÍCIAS</a>
                        </li>
                    </ul>
                    
                </div>
                <div class="col-0 col-lg-3">
                    <div class="segmentos">SEGMENTOS</div>
                </div>
            </nav>
        </div>
    </div>
</div>