<!DOCTYPE html>
<html lang="pt-br">
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			@section('title')
				TOPMAPS
			@show			
		</title>
		<meta name="description" content="Sistema para gerenciamento de infraestrutura de redes.">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<!--end::Fonts -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->		
		<link href="{{asset('assets/default/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/brand/light.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/default/skins/aside/light.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Layout Skins -->

		{{-- <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.png')}}"/> --}}
		<link rel="icon" type="image/ico" href="{{asset('topmapsArquivos/img/favicon.png')}}">

		{{-- <link rel="stylesheet" href="{{asset('css/site/header.css')}}"> --}}
		{{-- <link rel="stylesheet" href="{{asset('css/site/footer.css')}}"> --}}
		{{-- <link rel="stylesheet" href="{{asset('css/site/tema.css')}}"> --}}

		<!--begin::Page Custom Styles(used by this page) -->
		@yield('css_pagina')
		<!--end::Page Custom Styles -->
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-page--loading">
		
		<!-- begin::Page loader -->
		{{-- <div class="kt-page-loader kt-page-loader--logo">
			<img alt="Logo" src="{{asset('assets/media/logos/logo_topsapp_carregando.png')}}"/>
			<div class="kt-spinner kt-spinner--brand"></div>
		</div> --}}
		<!-- end::Page Loader -->

		<div class="kt-grid kt-grid--hor kt-grid--root">
			{{-- <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page"> --}}
				{{-- <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper"> --}}
						{{-- kt-header--fixed --}}
					{{-- <div id="kt_header" class="kt-header kt-grid__item"> --}}
                        <!-- begin:: Header -->
                        
						<!-- end:: Header -->

                        <!-- begin:: Menu -->
                        
						<!-- end:: Menu -->
					{{-- </div> --}}

					<!-- BEGIN: Conteudo Página -->
					@yield('conteudo')
					<!-- END: Conteudo Página -->
					
                    <!-- begin:: Footer -->
                    
					<!-- end:: Footer -->
				{{-- </div> --}}
			{{-- </div> --}}
		</div>
		<!-- end:: Page -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		
		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		@yield('scripts_pagina_vendors')
		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script>

			// Class definition
			var KTBootstrapSelectModal = function () {
				
				// Private functions
				var funcBootstrapSelectModal = function () {
					// minimum setup
					$('.kt-selectpicker').selectpicker()
				}

				return {
					// public functions
					init: function() {
						funcBootstrapSelectModal()
					}
				}
			}()
		
				
			// Initialization
			jQuery(document).ready(function() {
				KTBootstrapSelectModal.init()
			})

		</script>
		<!--end::Page Scripts -->

		<!--begin::Page Scripts(used by this page) -->
		{{-- <script src="{{asset('js/menu_site.js')}}"></script> --}}

		<script>

			let janelaWidth = $(window).width()

		</script>

		@yield('scripts_pagina')
		<!--end::Page Scripts -->

		<!--begin::Global App Bundle(used by all pages) -->
		<script src="{{asset('assets/app/bundle/app.bundle.js')}}" type="text/javascript"></script>
		@yield('scripts_pagina_app_bundle')
		<!--end::Global App Bundle -->
	</body>
	<!-- end::Body -->
</html>