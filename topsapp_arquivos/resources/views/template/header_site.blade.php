<div class="container">
    <header class="row">
        <div class="logo col-12 col-lg-6">
            <a href="{{route('site.home')}}">
                <img src="{{asset('img/logo_topsapp.png')}}" alt="logo topsapp">
            </a>
        </div>

        <div class="info col-12 col-md-12 col-lg-6">
            <div class="telefone">
                <i class="fa fa-phone-volume mr-2"></i><a href="tel:{{$dados_site->telefones}}"><span class="kt-font-boldest">{{$dados_site->telefones}}</span></a>
            </div>

            <div class="redes_sociais">
                <span class="kt-font-boldest">REDES SOCIAIS</span>
                <ul>
                    <li><a href="{{$dados_site->facebook}}" target="_blank"><i class="fab fa-facebook"></i></a></li>
                    <li class="ml-2 mr-2"><a href="{{$dados_site->twitter}}" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
                    <li><a href="{{$dados_site->youtube}}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </header>
</div>