@extends(('template.topappCentral'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link href="{{asset('topappCentralArquivos/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('topappCentralArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('topappCentralArquivos/css/style.css')}}">
@endsection

@section('conteudo')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            
            <div class="topo">
                <div class="topo-esquerdo">
                    <img src="{{asset('topappCentralArquivos/img/logo_03.png')}}" alt="">
                </div>
                
                <div class="topo-direito">
                    <h1><span>AGILIZE O DIA A DIA DE SEUS</span> CLIENTES E OTIMIZE O SEU TEMPO</h1>
                </div>
            </div>
            
            <div class="conteudo">
            
                <div class="title">
                
                    <p>CENTRAL APP</p>
                    {{-- <div class="barra"></div> --}}
                    <span>Com o CENTRAL APP, sua empresa poderá otimizar o tempo através das facilidades oferecidas ao cliente, ou seja, soluções na palma da mão através do smartphone ou tablet. Empresas que possuem um aplicativo, estão na vanguarda da tecnologia.</span>
                
                </div>

                <div class="fim-aln">
                    
                    <div class="fim-item">
                        <img src="{{asset('topappCentralArquivos/img/icones_03.png')}}">
                        {{-- <div class="barra"></div> --}}
                        <h2>Notificações</h2>
                        <p>Aprimore o relacionamento entre empresa e cliente. Através do TopSapp é possível enviar notificações personalizadas e relevantes, de acordo com o interesse de cada cliente.</p>
                    
                    </div>
                    
                    <div class="fim-item">
                    
                        <img src="{{asset('topappCentralArquivos/img/icones_05.png')}}">
                        {{-- <div class="barra"></div> --}}
                        <h2>Extrato de Acessos</h2>
                        <p>Acompanhe o histórico detalhado dos acessos, consumo de download e franquias utilizadas. </p>
                    
                    </div>
                    
                    <div class="fim-item">
                    
                        <img src="{{asset('topappCentralArquivos/img/icones_07.png')}}">
                        {{-- <div class="barra"></div> --}}
                        <h2>Serviços Contratados</h2>
                        <p>Informações detalhadas sobre serviços contratados, dados pessoais e de cobranças, contrato do assinante.</p>
                    
                    </div> 

                    <div class="sub-itens">
                        <div class="frase">
                            <img src="{{asset('topappCentralArquivos/img/fundo_12.png')}}" alt="">
                        </div>

                        <div class="itens">
                            <div class="item">
                    
                                <img src="{{asset('topappCentralArquivos/img/icones_13.png')}}">
                                {{-- <div class="barra"></div> --}}
                                <h2>Solicitações de Suporte</h2>
                                <p>Abertura de solicitação de suporte, consulta do status da solicitação, respostas do técnico, histórico dos atendimentos.</p>
                            
                            </div>

                            <div class="item">
                            
                                <img src="{{asset('topappCentralArquivos/img/icones_16.png')}}">
                                {{-- <div class="barra"></div> --}}
                                <h2>Consultas Financeiras</h2>
                                <p>Acesso detalhado a consultas financeiras, tais como faturas pendentes de pagamento e pagas.</p>
                            
                            </div>

                            <div class="item">
                            
                                <img src="{{asset('topappCentralArquivos/img/icones_20.png')}}">
                                {{-- <div class="barra"></div> --}}
                                <h2>Liberação em Confiança</h2>
                                <p>Com essa opção, é possível liberar a conexão e os serviços contratados, até que seja confirmado o pagamento da fatura.</p>
                            
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="meio">
                
                    <div class="meio-aln">
                    
                        <div class="meio-desc">
                        
                            <div class="esquerda">

                                <div class="kt-notes">
                                    <div class="kt-notes__items">
                                        <div class="kt-notes__item"> 
                                            <div class="kt-notes__media">
                                                <span class="kt-notes__icon">
                                                    <i class="fa fa-clipboard-list kt-font-brand"></i>
                                                </span>                               
                                            </div>         
                                            <div class="kt-notes__content"> 
                                                <div class="kt-notes__section">     
                                                    <div class="kt-notes__info">
                                                        <a href="#" onclick="return false;" class="kt-notes__title" style="cursor: default;">
                                                            Diagnóstico
                                                        </a>
                                                    </div>
                                                </div>
                                                <span class="kt-notes__body">                                        
                                                    Com apenas um toque é realizado um diagnóstico do serviço, onde status cadastrais e financeiros são checados.
                                                </span>  
                                            </div>                                             
                                        </div> 
                                        <div class="kt-notes__item"> 
                                            <div class="kt-notes__media">
                                                <span class="kt-notes__icon">
                                                    <i class="fa fa-address-book kt-font-success"></i>
                                                </span>                                  
                                            </div>   
                                            <div class="kt-notes__content"> 
                                                <div class="kt-notes__section">     
                                                    <div class="kt-notes__info">
                                                        <a href="#" onclick="return false;" class="kt-notes__title" style="cursor: default;">
                                                            Contatos
                                                        </a>
                                                    </div>
                                                </div>
                                                <span class="kt-notes__body">                                        
                                                    Disponibilize facilmente a seus clientes os contatos de sua empresa e alavanque suas mídias sociais.
                                                </span>  
                                            </div>                     
                                        </div> 
                                        <div class="kt-notes__item"> 
                                            <div class="kt-notes__media">
                                                <span class="kt-notes__icon">
                                                    <i class="flaticon-safe-shield-protection kt-font-danger"></i>
                                                </span>                               
                                            </div>                             
                                            <div class="kt-notes__content"> 
                                                <div class="kt-notes__section">     
                                                    <div class="kt-notes__info">
                                                        <a href="#" onclick="return false;" class="kt-notes__title" style="cursor: default;">
                                                            Segurança
                                                        </a>
                                                    </div>
                                                </div>
                                                <span class="kt-notes__body">
                                                    Possibilita login via biometria, aceitando tanto digitais quanto FaceID.
                                                </span>  
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="direita">
                                    <img src="{{asset('topappCentralArquivos/img/fundo_26.png')}}" alt="">
                            </div>
                        
                        </div>

                        <div class="loja">

                            <img src="{{asset('topappCentralArquivos/img/fundo_27.PNG')}}">
                                                    
                        </div>

                        <div class="solicitar">
                            {{-- <div class="banner"> --}}
                                <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                            {{-- </div> --}}
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="bordas-div">
            
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">
                        
                        <a href="tel:(66) 3211-0010">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topappCentralArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>(66) 3211-0010</span>
                            </div>
                        </a>

                        <a href="mailto:comercial@topsapp.com.br">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topappCentralArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>comercial@topsapp.com.br</span>
                            </div>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                            <div>
                                <img src="{{asset('topappCentralArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                            <div>
                                <img src="{{asset('topappCentralArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>Instagram</span>
                        </a>

                        <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                            <div>
                                <img src="{{asset('topappCentralArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                    </div>
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
                
            </div>

        </div>
    </div>
@endsection

@section('scripts_pagina')

    
    
@endsection