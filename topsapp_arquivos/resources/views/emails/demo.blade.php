<!DOCTYPE html>
<html>
<head>
    <title>Mensagem enviada pelo site</title>
    <meta charset="utf-8">
    <style>
        body{
            width:510px;
            margin:0;
            padding:0 20px;
            font-family:Verdana, sans-serif;
            font-size:.8rem;
        }
        h1{
            font-size:1.2rem;
        }
        h2{
            font-size:1rem;
        }
    </style>
</head>
<body>
    <h1>Demo - TOPSAPP</h1>
<hr>
    <h2>Dados do solicitante</h2>
    <p>
        <strong>Nome:</strong> {{$nome}}<br>
        <strong>E-mail:</strong> {{$email}}<br>
        <strong>Telefone Comercial:</strong> {{$telefone_comercial}}<br>
        <strong>Telefone Celular:</strong> {{$telefone_celular}}<br>
        <strong>Site:</strong> {{$site}}<br>
    </p>
<hr>
    <h2>Dados de endereço</h2>
    <p>
        <strong>Endereco:</strong> {{$endereco}}<br>
        <strong>Número:</strong> {{$numero}}<br>
        <strong>Cidade:</strong> {{$cidade}}<br>
        <strong>Estado:</strong> {{$estado}}<br>
        <strong>Cep:</strong> {{$cep}}<br>
    </p>
<hr>
    <h2>Solicitação da versão topsapp demo</h2>
    <p>
        <strong>Responsavel:</strong> {{$responsavel}}<br>
        <strong>Como conheceu:</strong> {{$conheceu}}<br>
        <strong>Mensagem:</strong><br> <?php echo nl2br($descricao); ?>
    </p>
<hr>
<div>
    <p>
        <a href="http://topsapp.com.br" title="TOPSAPP">Topsapp gestão de provedores de internet</a>
        <br>
        Rua das cerejeiras - Nº 1987<br>
        Jardim Paraiso – CEP 78556-106<br>
        Sinop – MT – Brasil<br>
        Tel.: +55 66 3511-0010
    </p>
</div>
</body>
</html> 