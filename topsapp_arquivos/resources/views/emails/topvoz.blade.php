<!DOCTYPE html>
<html>
<head>
    <title>Solicitação - TOPVOZ</title>
    <meta charset="utf-8">
    <style>
        body{
            width:510px;
            margin:0;
            padding:0 20px;
            font-family:"Raleway", sans-serif;
            font-size:12px;
        }
        h1{
            font-size:20px;
        }
        h2{
            font-size:16px;
        }
    </style>
</head>
<body style="font-family: Verdana; font-size: .8rem;">
    <h1>Solicitação - TOPVOZ</h1>
<hr>
    <h2>Dados do solicitante</h2>
    <p>
        <strong>Nome:</strong> {{$nome}}<br>
        <strong>E-mail:</strong> {{$email}}<br>
        <strong>Telefone para contato:</strong> {{$telefone}}<br>
    </p>
<hr>
<div>
    <p>
        <a href="http://topsapp.com.br/topvoz" title="TOPSAPP">Topsapp gestão de provedores de internet</a>
        <br>
        Rua das cerejeiras - Nº 1987<br>
        Jardim Paraiso – CEP 78556-106<br>
        Sinop – MT – Brasil<br>
        Tel.: +55 66 3511-0010
    </p>
</div>
</body>
</html> 