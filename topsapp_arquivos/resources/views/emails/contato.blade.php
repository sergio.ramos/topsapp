<!DOCTYPE html>
<html>
<head>
    <title>Mensagem enviada pelo site</title>
    <meta charset="utf-8">
    <style>
        body{
            width:510px;
            margin:0;
            padding:0 20px;
            font-family:Verdana, sans-serif;
            font-size:.8rem;
        }
        h1{
            font-size:1.2rem;
        }
        h2{
            font-size:1rem;
        }
    </style>
</head>
<body>
    <h1>Contato - TOPSAPP</h1>
<hr>
    <h2>Dados do solicitante</h2>
    <p>
        <strong>Nome:</strong> {{$nome}}<br>
        <strong>E-mail:</strong> {{$email}}<br>
        <strong>Telefone para contato:</strong> {{$telefone}}<br>
        <strong>Site:</strong> {{$site}}<br>
    </p>
<hr>
    <h2>Solicitação contato</h2>
    <p>
        <strong>Departamento:</strong> {{$departamento}}<br>
        <strong>Mensagem:</strong><br> <?php echo nl2br($descricao); ?>
    </p>
<hr>
<div>
    <p>
        <a href="http://topsapp.com.br" title="TOPSAPP">Topsapp gestão de provedores de internet</a>
        <br>
        Rua das cerejeiras - Nº 1987<br>
        Jardim Paraiso – CEP 78556-106<br>
        Sinop – MT – Brasil<br>
        Tel.: +55 66 3511-0010
    </p>
</div>
</body>
</html> 