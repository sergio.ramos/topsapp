<!DOCTYPE html>
<html>
<head>
    <title>Ouvidoria - TOPSAPP</title>
    <meta charset="utf-8">
    <style>
        body{
            width:510px;
            margin:0;
            padding:0 20px;
            font-family:"Raleway", sans-serif;
            font-size:12px;
        }
        h1{
            font-size:20px;
        }
        h2{
            font-size:16px;
        }
    </style>
</head>
<body style="font-family: Verdana; font-size: .8rem;">
    <h1>Ouvidoria - TOPSAPP</h1>
<hr>
    <h2>Dados do solicitante</h2>
    <p>
        <strong>Nome:</strong> {{$nome}}<br>
        <strong>E-mail:</strong> {{$email}}<br>
        <strong>Tipo de pessoa:</strong> {{$tipo_pessoa}}<br>
        <strong>Telefone para contato:</strong> {{$telefone}}<br>
    </p>
<hr>
    <h2>Dados de endereço</h2>
    <p>
        <strong>Cidade:</strong> {{$cidade}}<br>
        <strong>Estado:</strong> {{$estado}}<br>
        <strong>Cep:</strong> {{$cep}}<br>
    </p>
<hr>
    <h2>Solicitação ouvidoria</h2>
    <p>
        <strong>Prioridade:</strong> {{$prioridade}} <br>
        <strong>Mensagem:</strong><br> <?php echo nl2br($mensagem); ?>
    </p>
<hr>
<div>
    <p>
        <a href="http://topsapp.com.br" title="TOPSAPP">Topsapp gestão de provedores de internet</a>
        <br>
        Rua das cerejeiras - Nº 1987<br>
        Jardim Paraiso – CEP 78556-106<br>
        Sinop – MT – Brasil<br>
        Tel.: +55 66 3511-0010
    </p>
</div>
</body>
</html> 