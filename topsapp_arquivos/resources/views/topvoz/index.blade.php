@extends(('template.topvoz'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('topvozArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('topvozArquivos/css/style.css')}}">
@endsection

@section('conteudo')
    {{-- <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"> --}}
        <!-- begin:: Content -->
	    {{-- <div class="kt-content p-0"> --}}

            <a href="{{route('topvoz.home')}}" style="width: 100%;" id="home">
                <div class="header">
                
                    <img class="header-img" src="{{asset('topvozArquivos/img/logo.png')}}">
                
                </div>
            </a>

            <div class="banner-azul">
            
            </div>

            <<div class="conteudo">

                <div class="sobre-topvoz">
                    <div class="sobre-topvoz-aln">

                        <P>O TOPVOZ é um produto desenvolvido pela Multiware Tecnologia junto ao TopSapp. Trata-se de um SoftSwitch idealizado para atender todas suas necessidades em telefonia IP.</P>

                    </div>
                </div>
            
                <div class="banner-meio">

                    <div class="banner-meio-aln row">
                    
                        <div class="quadros col-12 col-lg-6">
                        
                            <div class="quadros-aln">
                            
                                <img src="{{asset('topvozArquivos/img/portabilidade.png')}}" alt="">

                                <h2>PORTABILIDADE</h2>
                            
                            </div>

                            <div class="quadros-aln">
                            
                                <img src="{{asset('topvozArquivos/img/novo_topvoz---Alterado_09.png')}}" alt="">

                                <h2>MENSAGEM PROGRAMADA</h2>
                            
                            </div>

                            <div class="quadros-aln">
                            
                                <img src="{{asset('topvozArquivos/img/novo_topvoz---Alterado_14.png')}}" alt="">

                                <h2>FATURAS</h2>
                            
                            </div>

                            <div class="quadros-aln">
                            
                                <img src="{{asset('topvozArquivos/img/novo_topvoz---Alterado_16.png')}}" alt="">

                                <h2>PLANOS</h2>
                            
                            </div>
                        
                        </div>
                        
                        <div class="solucoes col-12 col-lg-6">

                            <h1>SOLUÇÕES</h1>

                            <div class="kt-portlet ">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            OUTRAS
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body p-0">
                                    <!--begin::Accordion-->
                                    <div class="accordion accordion-light accordion-toggle-plus" id="accordion_solucoes">
                                        <div class="card">
                                            <div class="card-header" id="headOne">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    GERENCIADOR DE TRONCOS
                                                </div>
                                            </div>
                                            <div id="collapseOne" class="collapse" aria-labelledby="headOne" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    O sistema TOPVOZ realiza o cadastramento de troncos da tecnologia SIP e E1. Suporta troncos que trabalham por meio de um registro (usuário, senha e linha de registro) e também com tech-prefix (colocado a frente de uma rota de saída). 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headTwo">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    GERENCIAMENTO CENTRALIZADO
                                                </div>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headTwo" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    Toda gestão de faturas, cobranças, inadimplências e contratos dos clientes são gerenciados pelo sistema TopSapp.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headThree">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    OPÇÃO ESPIÃO
                                                </div>
                                            </div>
                                            <div id="collapseThree" class="collapse" aria-labelledby="headThree" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    De maneira discreta e eficaz, monitore cada ramal de seu negócio e ouça tudo em tempo real.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headFour">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    DETALHAMENTO DAS LIGAÇÕES
                                                </div>
                                            </div>
                                            <div id="collapseFour" class="collapse" aria-labelledby="headFour" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    Relatórios e controle das ligações, contendo horários, tempo de chamada, destino, custos.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headFive">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    GRAVAÇÃO DAS CHAMADAS
                                                </div>
                                            </div>
                                            <div id="collapseFive" class="collapse" aria-labelledby="headFive" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    Pode ser configurado a gravação das chamadas, tanto entrantes quanto saíntes de cada terminal cadastrado.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headSix">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    CRIAÇÃO DE URA'S INTELIGENTES
                                                </div>
                                            </div>
                                            <div id="collapseSix" class="collapse" aria-labelledby="headSix" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    Com a URA (Unidade de Resposta Audível) é possível personalizar os atendimentos, direcionando para a opção que o cliente desejar. Tornando o atendimento mais eficiente e rápido. 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headSeven">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    LIBERAÇÃO POR CONFIANÇA
                                                </div>
                                            </div>
                                            <div id="collapseSeven" class="collapse" aria-labelledby="headSeven" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    A Liberação por confiança foi criada com intuito de proporcionar um meio de ajudar seus clientes a agilizar os pagamentos e liberação do serviço.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headEight">
                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight" {{--style="color: rgba(255,255,255,.8);"--}}>
                                                    AVALIAÇÃO DE ATENDIMENTO
                                                </div>
                                            </div>
                                            <div id="collapseEight" class="collapse" aria-labelledby="headEight" data-parent="#accordion_solucoes" style="background-color: rgba(255,255,255,.8); color: rgb(51,77,110);">
                                                <div class="card-body" style="padding: .7rem 2rem 0rem 1.5rem;">
                                                    Tenha um feedback de seus clientes, podendo configurar avaliações de atendimento de seus operadores.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Accordion-->  
                                </div>
                            </div>  

                        </div>
                    
                    </div>

                </div>

                <div class="servicos">
                    <div class="servicos-aln">
                        <h1>O QUE PODEMOS FAZER POR VOCÊ</h1>

                        <div class="container">
                            <div class="row">
                                <div class="servico col-6 col-lg-3">
                                    <img src="{{asset('topvozArquivos/img/novo_topvoz---Alterado_21.png')}}" alt="">
                                    <h2>SEGURANÇA</h2>
                                </div>
                                
                                <div class="servico col-6 col-lg-3">
                                    <img src="{{asset('topvozArquivos/img/novo_topvoz---Alterado_23.png')}}" alt="">
                                    <h2>SUPORTE ESPECIALIZADO</h2>
                                </div>
                                
                                <div class="servico col-6 col-lg-3">
                                    <img src="{{asset('topvozArquivos/img/escalabilidade.PNG')}}" alt="">
                                    <h2>ALTA ESCALABILIDADE</h2>
                                </div>

                                <div class="servico col-6 col-lg-3">
                                    <img src="{{asset('topvozArquivos/img/custo.PNG')}}" alt="">
                                    <h2>BAIXO CUSTO</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="solicitar">
                    <div class="banner">
                        <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                    </div>
                </div>

                <div class="bordas-div">
            
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">

                        <a href="tel:(66) 3211-0010">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topvozArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>(66) 3211-0010</span>
                            </div>
                        </a>

                        <a href="mailto:comercial@topsapp.com.br">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topvozArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>comercial@topsapp.com.br</span>
                            </div>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                            <div>
                                <img src="{{asset('topvozArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                            <div>
                                <img src="{{asset('topvozArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>Instagram</span>
                        </a>
                        
                        <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                            <div>
                                <img src="{{asset('topvozArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                        
                    </div>
                
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
                
            </div>

        {{-- </div>    --}}
    {{-- </div> --}}
@endsection

@section('scripts_pagina')

    

@endsection