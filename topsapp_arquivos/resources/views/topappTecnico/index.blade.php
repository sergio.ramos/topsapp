@extends(('template.topappTecnico'))
@section('title')
    
    @parent
@stop

@section('css_pagina')
    <link rel="stylesheet" href="{{asset('topappTecnicoArquivos/css/home.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('topappTecnicoArquivos/css/style.css')}}">
@endsection

@section('conteudo')

    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
	    <div class="kt-content p-0">
            
            <div class="topo">
                
                <img src="{{asset('topappTecnicoArquivos/img/logo_topapp_white.png')}}">
            
            </div>
            
            <div class="conteudo">

                <a href="{{route('topapp.home')}}" style="width: 100%;">
                    <div id="particles-js">
                    
                        <img src="{{asset('topappTecnicoArquivos/img/logo_topapp.png')}}">
                    
                    </div>
                </a>

                <div class="azul">
                   
                    <p>Aplicativo voltado a gestão do seu provedor. Quando se trata de evoluir seu desempenho, se diferenciar da concorrência e aumentar faturamento, o TOP APP foi desenvolvido para ser uma fundamental ferramenta de inovação tecnológica. Pronto para impulsionar as demandas de sua empresa.</p>
                
                 </div>

                <div class="ponta"></div>
                
                <div class="meio">
                
                    <div class="meio-aln">
                        <div class="title">

                            <div class="barra"></div>
        
                            <h1>Gerencie seu negócio de qualquer lugar, a qualquer momento.</h1>
                        
                        </div>

                        <div class="meio-texto">
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/gestao.svg')}}">
                                </span>
                                <h2>Clientes</h2>
                                <p>Listagem de clientes, gerenciamento de informações cadastrais, status e outros.</p>
                            </div>
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/abertura.svg')}}">
                                </span>
                                <h2>Financeiro</h2>
                                <p>Acesso a lista de faturas, visualização, geração e envio.</p>
                            </div>
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/detalhes.svg')}}">
                                </span>
                                <h2>Suporte</h2>
                                <p>Lista de atendimentos, controle de atendimentos, coleta de assinatura, fotos e outros.</p>
                            </div>
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/atividades.svg')}}">
                                </span>
                                <h2>Viabilidade</h2>
                                <p>Cadastro de Viabilidade para prospecção de clientes.</p>
                            </div>
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/atividades.svg')}}">
                                </span>
                                <h2>Controle</h2>
                                <p>Possibilita o acompanhamento de rotas e atendimentos de seus técnicos.</p>
                            </div>
                            <div class="item-texto">
                                <span>
                                    <img src="{{asset('topappTecnicoArquivos/img/atividades.svg')}}">
                                </span>
                                <h2>Utilidade</h2>
                                <p>Inserção de postes e marcadores na plataforma TopMaps através do TOP APP.</p>
                            </div>
                        </div>
                                            
                        <div class="meio-conteudo">
                            
                            <div class="direita">

                                <img src="{{asset('topappTecnicoArquivos/img/celular.png')}}">
                            
                            </div>
                        
                        </div>

                        <div class="esquerda">

                            <img src="{{asset('topappTecnicoArquivos/img/loja.png')}}">
                                                    
                        </div>

                        <div class="solicitar">
                            <div class="banner">
                                <button class="button" onClick="window.open('https://www.topsapp.com.br/contato','Topsapp - Contato')">SOLICITAR PROPOSTA</button>
                            </div>
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="bordas-div">
            
                    <div class="borda-esquerda"></div>
                    <div class="borda-direita"></div>
                
                </div>
                
                <div class="final">
                
                    <p>
                        CONTATOS
                        <span></span>
                    </p>
                    
                    <div class="contatos">
                        
                        <a href="tel:(66) 3211-0010">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topappTecnicoArquivos/img/Layout_44.png')}}">
                                </div>
                                <span>(66) 3211-0010</span>
                            </div>
                        </a>

                        <a href="mailto:comercial@topsapp.com.br">
                            <div class="contato">
                                <div>
                                    <img src="{{asset('topappTecnicoArquivos/img/Layout_47.png')}}">
                                </div>
                                <span>comercial@topsapp.com.br</span>
                            </div>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/multiwaretecnologia" class="contato">
                            <div>
                                <img src="{{asset('topappTecnicoArquivos/img/fb.png')}}">
                            </div>
                            <span>Facebook</span>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/topsapp10/" class="contato">
                            <div>
                                <img src="{{asset('topappTecnicoArquivos/img/instagram.PNG')}}">
                            </div>
                            <span>Instagram</span>
                        </a>

                        <a target="_blank" href="https://twitter.com/topsapp" class="contato">
                            <div>
                                <img src="{{asset('topappTecnicoArquivos/img/Layout_50.png')}}">
                            </div>
                            <span>Twitter</span>
                        </a>
                    
                    </div>
                
                </div>
                
                <div class="bottom">
                
                    <p>Copyright 2019 - Todos os direitos reservados a Multiware Tecnologia</p>
                
                </div>
                
            </div>

        </div>
    </div>
@endsection

@section('scripts_pagina')

    <script>

        if($(window).width() >= 1024) {
            $('div.title div.barra').hide()
            $('div.title h1').hide()
            $('div.conteudo').css('background', '#fff')
    
            $(window).scroll(function() {

                if( $(window).scrollTop() < 50 ) {
                    $('div.title div.barra').hide()
                    $('div.title h1').hide()
                    $('div.conteudo').css('background', '#fff')
                } else {
                    $('div.title div.barra').show()
                    $('div.title h1').show()
                    $('div.conteudo').css('background', '#f5f5f5')
                }
            })
        }

        $(window).scroll(function() {
            if($(window).width() >= 1024) {
                if ( $(window).scrollTop() >= 660 ) {
                    $('#particles-js').css('display', 'flex');
                } else {
                    $('#particles-js').attr('style', '');
                }
            }

            if($(window).width() >= 576 && $(window).width() < 1024) {
                if ( $(window).scrollTop() >= 200 ) {
                    $('#particles-js').css('display', 'flex');
                } else {
                    $('#particles-js').attr('style', '');
                }
            }

            if($(window).width() < 576) {
                $('#particles-js').css('display', 'none');
            }
        })

        if($(window).width() >= 1024) {
            $('div.azul').slideUp(800)
            $('div.ponta').slideUp(800)
            $('div.azul p').fadeOut()
            $('div.funciona').css('margin-top', '10rem')
    
            $(window).scroll(function() {

                if( $(window).scrollTop() < 50 ) {
                    $('div.azul').slideUp(800)
                    $('div.ponta').slideUp(800)
                    $('div.azul p').fadeOut()
                    $('div.funciona').css('margin-top', '10rem')
                } else {
                    $('div.azul').slideDown(800)
                    $('div.ponta').slideDown(800)
                    $('div.azul p').fadeIn()
                    $('div.funciona').css('margin-top', '0')
                }
            })
        }
    
    </script>
    
@endsection