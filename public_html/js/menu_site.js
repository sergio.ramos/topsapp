function configMenuDesktop(width, position) {

    let url_atual = window.location.href.substr(window.location.href.lastIndexOf("/"))

    // inicio efeito de transição no menu
    $('li.nav-item:nth-child(1)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "11.8rem")
            $('div.menu-line').css("left", "0")
        } else {
            $('div.menu-line').css("width", "10.8rem")
            $('div.menu-line').css("left", "0")
        }
        
    }).mouseout(function() {
        if(url_atual != '/') {
            $('div.menu-line').css("width", width)
            $('div.menu-line').css("left", position)
        } else {
            $('div.menu-line').css("width", "0")
        }
    })

    $('li.nav-item:nth-child(2)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "8.6rem")
            $('div.menu-line').css("left", "11.8rem")
        } else {
            $('div.menu-line').css("width", "7.8rem")
            $('div.menu-line').css("left", "10.8rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })

    $('li.nav-item:nth-child(3)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "9.25rem")
            $('div.menu-line').css("left", "20.3rem")
        } else {
            $('div.menu-line').css("width", "8.3rem")
            $('div.menu-line').css("left", "18.7rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })

    $('li.nav-item:nth-child(4)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "8.95rem")
            $('div.menu-line').css("left", "29.57rem")
        } else {
            $('div.menu-line').css("width", "7.8rem")
            $('div.menu-line').css("left", "27rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })

    $('li.nav-item:nth-child(5)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "8.2rem")
            $('div.menu-line').css("left", "38.45rem")
        } else {
            $('div.menu-line').css("width", "7.2rem")
            $('div.menu-line').css("left", "34.8rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })

    $('li.nav-item:nth-child(6)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "6.2rem")
            $('div.menu-line').css("left", "46.65rem")
        } else {
            $('div.menu-line').css("width", "4.8rem")
            $('div.menu-line').css("left", "41.9rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })

    $('li.nav-item:nth-child(7)').mouseover(function() {
        if(janelaWidth >= 1024) {
            $('div.menu-line').css("width", "7.95rem")
            $('div.menu-line').css("left", "52.9rem")
        } else {
            $('div.menu-line').css("width", "6.8rem")
            $('div.menu-line').css("left", "46.7rem")
        }
    }).mouseout(function() {
        $('div.menu-line').css("width", width)
        $('div.menu-line').css("left", position)
    })
    // fim efeito de transição no menu
}

function configMenuMobile() {

}